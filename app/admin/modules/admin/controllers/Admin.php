<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_admin");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'admin';

            $data['pending_order'] = $this->M_admin->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_admin->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_admin->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function new_admin()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_admin->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_admin->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_admin->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function edit_admin($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_admin->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_admin->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_admin->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_admin->fetch_table("*","a__admin","id_account='$id'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }



    public function ajax_list(){
         $column = '*';
         $column_order = array(null, 'name','username','email','level'); //set column field database for datatable orderable
         $column_search = array('name','username','email','level'); //set column field database for datatable searchable 
         $order = array('id_account' => 'DESC'); // default order 
         $table = "a__admin";
         $where = "";
         $joins = "";

        $list = $this->M_admin->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            if($key->is_active=="1"){ $checked =  "checked"; }else{ $checked = ""; } 
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->name;
            $row[] = $key->username;
            $row[] = $key->email;
            $row[] = $key->level;
            $row[] = "<button data-toggle='tooltip' data-placement='top' title='Edit' onclick='ajax_get_edit_admin(".$key->id_account.")' class='btn btn-success btn-sm'><i class='zmdi zmdi-edit'></i></button>
                     <button data-toggle='tooltip' data-placement='top' title='Delete' onclick='ajax_action_delete_admin(".$key->id_account.")' class='btn btn-danger btn-sm'><i class='zmdi zmdi-delete'></i></button>";
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_admin->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_admin->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_admin(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('level', 'level', 'required');
       

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
            "name" => post("nama"),
            "email" => post("email"),
            "username" => post("username"),
            "password" => md5(post("password")),
            "level" => post("level"),
            "is_active" => post("is_active"),
            "active_at" => date("Y-m-d H:i:s"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
        );
            $add = $this->M_admin->insert_table("a__admin",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/admin'
                );
                print json_encode($json_data);
            }
            
        }
        
    }

    public function ajax_action_delete_admin(){
           $delete = $this->M_admin->delete_table("a__admin","id_account",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/admin'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_action_edit_admin(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('level', 'level', 'required');
        


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
                "name" => post("nama"),
                "email" => post("email"),
                "username" => post("username"),
                "password" => md5(post("password")),
                "level" => post("level"),
                "is_active" => post("is_active"),
                "update_at" => date("Y-m-d H:i:s")
            );

            $edit = $this->M_admin->update_table("a__admin",$data,"id_account",post("id_account"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/admin'
                );
                print json_encode($json_data);
            }
        }

    }

    
}

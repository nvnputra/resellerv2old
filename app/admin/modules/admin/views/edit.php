 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Admin</h4>
 </div>
 <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}

 foreach ($data as $key ) { ?>
<form method="post" onsubmit="return ajax_action_edit_admin();">
        <input type="hidden" name="id" value="<?php echo $key->id_account;  ?>" id="id">
        <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" value="<?php echo $key->name; ?>" name="nama" id="e_nama" class="form-control">
            </td>
            <td>
              <label>Email :</label>
              <input type="text" value="<?php echo $key->email; ?>" name="email" id="e_email" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Username :</label>
              <input type="text" name="username" value="<?php echo $key->username; ?>" id="e_username" class="form-control">
            </td>
            <td>
              <label>Password :</label>
              <input type="password" name="password" id="e_password" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Level :</label>
              <select name="level" class="form-control" id="e_level">
                <option value=""></option>
                <option <?php if ($key->level=="SUPER") { echo "selected"; } ?> value="SUPER">Super Admin</option>
                <option <?php if ($key->level=="ADMIN") { echo "selected"; } ?> value="ADMIN">Admin</option>
              </select>
            </td>
            <td>
              <label>Is Active :</label><br>
              <input <?php if ($key->is_active=="1") { echo "checked"; } ?> type="checkbox" name="isactive" id="e_isactive" value="1" >
            </td>
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/admin"><button type="button" class="btn btn-danger" data-dismiss="modal">Back</button></a>
        <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
      </div>
</form>
<?php } ?>
<script type="text/javascript">
    function ajax_action_edit_admin(){
    var id_account = $("#id").val();
    if ($('#e_isactive').is(':checked')) {
      var isactive = 1;
    }else{
      var isactive = 0;
    }

    var nama = $("#e_nama").val();
    var username = $("#e_username").val();
    var password = $("#e_password").val();
    var email = $("#e_email").val();
    var level = $("#e_level").val();
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/admin/ajax_action_edit_admin/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, username:username,password:password, email:email, level:level,is_active:isactive,id_account:id_account},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
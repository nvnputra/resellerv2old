 <div class="card-header bg-primary">
      <h4 style="color: white;" class="modal-title" id="title-tambah">New Admin</h4>
 </div>
<form method="post" onsubmit="return ajax_action_add_admin();">
      <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </td>
            <td>
              <label>Email :</label>
              <input type="text" name="email" id="email" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Username :</label>
              <input type="text" name="username" id="username" class="form-control">
            </td>
            <td>
              <label>Password :</label>
              <input type="password" name="password" id="password" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Level :</label>
              <select name="level" class="form-control" id="level">
                <option value=""></option>
                <option value="SUPER">Super Admin</option>
                <option value="ADMIN">Admin</option>
              </select>
            </td>
            <td>
              <label>Is Active :</label><br>
              <input type="checkbox" checked="" name="isactive" id="isactive" value="1" >
            </td>
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/admin"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_admin" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function ajax_action_add_admin() {
    if ($('#isactive').is(':checked')) {
      var isactive = 1;
    }else{
      var isactive = 0;
    }

    var nama = $("#nama").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var email = $("#email").val();
    var level = $("#level").val();
   
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/admin/ajax_action_add_admin/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, username:username,password:password, email:email, level:level,is_active:isactive},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
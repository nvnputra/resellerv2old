<style type="text/css">
  #list_admin td{
    text-align: center;
  }
</style>

<a href="<?php echo base_url().$this->config->item('index_page'); ?>/admin/new_admin/"><button class="btn btn-primary btn-sm" ><i class="fas fa-plus"></i> New Admin</button></a>
<br><br>
<table class="table table-borderless table-striped " id="list_admin">
  <thead style="background: #000; color: #FFF;">
    <tr>
      <th><center>No</center></th>
      <th><center>Nama</center></th>
      <th><center>Username</center></th>
      <th><center>Email</center></th>
      <th><center>Level</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
  </tbody> 
</table>



<script type="text/javascript">

  
  function ajax_action_delete_admin(id_account){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/admin/ajax_action_delete_admin/",
              type:'POST',
              dataType: "json",
              data: {id:id_account},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_admin(id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/admin/edit_admin/"+id;
  }



</script>
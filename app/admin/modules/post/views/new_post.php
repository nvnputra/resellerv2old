 <div class="card-header bg-primary">
      <h4 style="color: white;" class="modal-title" id="title-tambah">New Post</h4>
 </div>
<form method="post" onsubmit="return ajax_action_add_post();">
      <table class="table">
          <tr>
            <td>
              <label>Subject :</label>
              <input type="text" name="subject" id="subject" class="form-control">
            </td>
            <td rowspan="2">
              <label>Body :</label>
              <textarea class="form-control" id="body" name="body" rows="5"></textarea>
            </td>
          </tr>
          <tr>
          
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <button type="submit" id="btn_admin" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function ajax_action_add_post() {

    var subject = $("#subject").val();
    var body = $("#body").val();
    var id_category = <?php echo $this->input->get("id"); ?>;
   
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_action_add_post/",
              type:'POST',
              dataType: "json",
              data: {subject:subject, body:body,id_category:id_category},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
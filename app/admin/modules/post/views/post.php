<style type="text/css">
  #list_admin td{
    text-align: center;
  }
</style>
<div class="card-header bg-primary">
<h4 style="color: white;">- <?php echo $title; //echo $this->input->get("data"); ?> -</h4>
</div><br><br>
<a href="<?php echo base_url().$this->config->item('index_page'); ?>/post/new_post?id=<?php echo $this->input->get("id"); ?>"><button class="btn btn-primary btn-sm" ><i class="fas fa-plus"></i> New Post</button></a>
<br><br>
<table class="table table-borderless table-striped " id="list_post">
  <thead style="background: #000; color: #FFF;">
    <tr>
      <th><center>No</center></th>
      <th><center>Category</center></th>
      <th><center>Subject</center></th>
      <th><center>Body</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
  </tbody> 
</table>



<script type="text/javascript">

  
  function ajax_action_delete_post(id){
    var id_category = <?php echo $this->input->get("id"); ?>;
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_action_delete_post/",
              type:'POST',
              dataType: "json",
              data: {id:id,id_category:id_category},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{  
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_post
  (id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/post/edit_post/"+id;
  }



</script>
 <div class="card-header bg-primary">
      <h4 style="color: white;" class="modal-title" id="title-tambah">New Category</h4>
 </div>
<form method="post" onsubmit="return ajax_action_add_category();">
      <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </td>
            <td rowspan="2">
              <label>Description :</label>
              <textarea class="form-control" id="desc" name="desc" rows="5"></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <label>Parent :</label>
              <select name="parent" class="form-control" id="parent">
                <option value=""></option>
                <?php foreach ($data_cat as $key ) { ?>
                   <option value="<?php echo $key->id_category; ?>"><?php echo $key->name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/post/category"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_admin" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function ajax_action_add_category() {

    var nama = $("#nama").val();
    var desc = $("#desc").val();
    var parent = $("#parent").val();
   
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_action_add_category/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, desc:desc,parent:parent},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
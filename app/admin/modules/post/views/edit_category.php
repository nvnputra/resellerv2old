 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Category</h4>
 </div>
 <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}
 foreach ($data as $key ) { ?>
<form method="post" onsubmit="return ajax_action_edit_category();">
        <input type="hidden" name="id" value="<?php echo $key->id_category;  ?>" id="id">
         <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" value="<?php echo $key->name;  ?>" name="nama" id="nama" class="form-control">
            </td>
            <td rowspan="2">
              <label>Description :</label>
              <textarea class="form-control" id="desc" name="desc" rows="5"><?php echo $key->description;  ?></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <label>Parent :</label>
              <select name="parent" class="form-control" id="parent">
                <option value=""></option>
                <?php foreach ($data_cat as $c ) { ?>
                   <option <?php if ($c->id_category==$key->id_parent) { echo "selected"; } ?> value="<?php echo $c->id_category; ?>"><?php echo $c->name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/post/category"><button type="button" class="btn btn-danger" data-dismiss="modal">Back</button></a>
        <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
      </div>
</form>
<?php } ?>
<script type="text/javascript">
    function ajax_action_edit_category(){

    var nama = $("#nama").val();
    var desc = $("#desc").val();
    var parent = $("#parent").val();
    var id = $("#id").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_action_edit_category/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, desc:desc,parent:parent,id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
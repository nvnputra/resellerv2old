 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Post</h4>
 </div>
 <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}
 foreach ($data as $key ) { ?>
<form method="post" onsubmit="return ajax_action_edit_post();">
        <input type="hidden" name="id" value="<?php echo $key->id_post;  ?>" id="id">
        <table class="table">
          <tr>
            <td>
              <label>Subject :</label>
              <input type="text" name="subject" value="<?php echo $key->subject;  ?>" id="subject" class="form-control">
            </td>
            <td rowspan="2">
              <label>Body :</label>
              <textarea class="form-control" id="body" name="body" rows="5"><?php echo $key->body;  ?></textarea>
            </td>
          </tr>
          <tr>
          
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
      </div>
</form>
<?php } ?>
<script type="text/javascript">
    function ajax_action_edit_post(){

    var subject = $("#subject").val();
    var body = $("#body").val();
    var id = $("#id").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_action_edit_post/",
              type:'POST',
              dataType: "json",
              data: {subject:subject, body:body,id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
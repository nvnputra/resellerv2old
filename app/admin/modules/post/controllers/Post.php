<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_post");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'post';
            $id = $this->input->get("id");
            $get_title = $this->M_post->get_row("name","a__post_category","id_category='$id'","","",FALSE);
            $data['title'] = $get_title->name;
            $data['menu_post'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_order'] = $this->M_post->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_post->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function new_post()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'new_post';
            $data['pending_order'] = $this->M_post->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_post->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['menu_post'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_action_add_post(){
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('body', 'body', 'required');
       

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
        $data = array(
            "subject" => post("subject"),
            "body" => post("body"),
            "id_category" => post("id_category"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
        );
            $add = $this->M_post->insert_table("a__post",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/post/?id='.post("id_category")
                );
                print json_encode($json_data);
            }
            
        }
        
    }

    public function ajax_action_delete_post(){
           $delete = $this->M_post->delete_table("a__post","id_post",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/post?id='.post("id_category")
                );
                print json_encode($json_data);
            }
    }

    public function edit_post($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit_post';
            $data['pending_order'] = $this->M_post->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_post->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_post->fetch_table("*","a__post","id_post='$id'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_action_edit_post(){
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('body', 'body', 'required');
       
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
           $data = array(
            "subject" => post("subject"),
            "body" => post("body"),
            "update_at" => date("Y-m-d H:i:s")
        );

            $edit = $this->M_post->update_table("a__post",$data,"id_post",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{

                $get_id_cat = $this->M_post->get_row("id_category","a__post","id_post='".post("id")."'","","",FALSE);
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/post?id='.$get_id_cat->id_category
                );
                print json_encode($json_data);
            }
        }

    }

    public function ajax_list_post(){
         $column = '*';
         $column_order = array(null, 'id_category','subject','body',null); //set column field database for datatable orderable
         $column_search = array('id_category','subject','body'); //set column field database for datatable searchable 
         $order = array('id_post' => 'DESC'); // default order 
         $table = "a__post";
         $get_id = post("id");
         $where = "id_category = '$get_id' ";
         $joins = "";

        $list = $this->M_post->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->id_category;
            $row[] = $key->subject;
            $row[] = $key->body;
            $row[] = "<button data-toggle='tooltip' data-placement='top' title='Edit' onclick='ajax_get_edit_post(".$key->id_post.")' class='btn btn-success btn-sm'><i class='zmdi zmdi-edit'></i></button>
                     <button data-toggle='tooltip' data-placement='top' title='Delete' onclick='ajax_action_delete_post(".$key->id_post.")' class='btn btn-danger btn-sm'><i class='zmdi zmdi-delete'></i></button>";
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_post->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_post->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }


    public function category()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'category';
            $data['menu_post'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_order'] = $this->M_post->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_post->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function new_category()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'new_category';
            $data['pending_order'] = $this->M_post->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_post->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data_cat'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_list_category(){
         $column = '*';
         $column_order = array(null, 'name','description','id_parent',null); //set column field database for datatable orderable
         $column_search = array('name','description','id_parent'); //set column field database for datatable searchable 
         $order = array('id_category' => 'DESC'); // default order 
         $table = "a__post_category";
         $where = "";
         $joins = "";

        $list = $this->M_post->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->name;
            $row[] = $key->description;
            $row[] = $key->id_parent;
            $row[] = "<button data-toggle='tooltip' data-placement='top' title='Edit' onclick='ajax_get_edit_category(".$key->id_category.")' class='btn btn-success btn-sm'><i class='zmdi zmdi-edit'></i></button>
                     <button data-toggle='tooltip' data-placement='top' title='Delete' onclick='ajax_action_delete_category(".$key->id_category.")' class='btn btn-danger btn-sm'><i class='zmdi zmdi-delete'></i></button>";
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_post->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_post->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_category(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('desc', 'desc', 'required');
       

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
        $data = array(
            "name" => post("nama"),
            "description" => post("desc"),
            "id_parent" => post("parent"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
        );
            $add = $this->M_post->insert_table("a__post_category",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/post/category/'
                );
                print json_encode($json_data);
            }
            
        }
        
    }

    public function ajax_action_delete_category(){
           $delete = $this->M_post->delete_table("a__post_category","id_category",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/post/category'
                );
                print json_encode($json_data);
            }
    }

    public function edit_category($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit_category';
            $data['pending_order'] = $this->M_post->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_post->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['menu_post'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $data['data'] = $this->M_post->fetch_table("*","a__post_category","id_category='$id'");
            $data['data_cat'] = $this->M_post->fetch_table("*","a__post_category","id_parent = '0'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_action_edit_category(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('desc', 'desc', 'required');
       


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
           $data = array(
            "name" => post("nama"),
            "description" => post("desc"),
            "id_parent" => post("parent"),
            "update_at" => date("Y-m-d H:i:s")
        );

            $edit = $this->M_post->update_table("a__post_category",$data,"id_category",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/post/category/'
                );
                print json_encode($json_data);
            }
        }

    }
    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_product");
      $this->load->model("product_type/M_product_type","M_product_type");
      $this->load->model("product_api/M_product_api","M_product_api");
      $this->load->model("member/M_member","M_member");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'product';
            $data['pending_order'] = $this->M_product->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['type'] = $this->M_product_type->fetch_table("*","a__product_type","");
            $data['api'] = $this->M_product_api->fetch_table("*","a__product_api","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function new_product()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'new_product';
            $data['pending_order'] = $this->M_product->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['type'] = $this->M_product_type->fetch_table("*","a__product_type","");
            $data['api'] = $this->M_product_api->fetch_table("*","a__product_api","");
            $data['config'] = $this->M_product->get_row("value","sys__config","param='usd-to-rp'","","",FALSE);
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function ajax_action_lookup(){
        $this->load->library('curl'); 

        $search = post('search');
        $api = post('api');
        $get_api = $this->M_product->get_row("*","a__product_api","api_name='$api' ","","",FALSE);
        $params = array(
            "key" => $get_api->api_key,
            "request" => 'service',
            "supplier" => $api
        );
        
        $get_lookup = $this->curl->simple_post($get_api->api_url, $params, array(CURLOPT_BUFFERSIZE => 10));
        $arr_data = json_decode($get_lookup);
        $data_arr = array();
        /*validasi data lookup kosong*/
        if(count($arr_data)=="0"){
            echo json_encode($data_arr);
            die();            
        }
        /*proses pencarian data lookup*/
        foreach ($arr_data->data as $key) {
            if (($key->service == $search) || ($key->category == $search)){
          //if ((false !== stripos($search, $key->service)) || (false !== stripos($search, $key->name)) ){
                 $set_data = array(
                    "service"=> $key->service,
                    "name"=> $key->name,
                    "category"=> $key->category,
                    "min"=> $key->min,
                    "max"=> $key->max,
                    "rate"=> $key->rate
                    
                 );
                array_push($data_arr, $set_data);
            }
        }

       echo json_encode($data_arr);
        
    }

    public function edit_product($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit_product';
            $data['pending_order'] = $this->M_product->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_product->fetch_table("*","a__product","id='$id'");
            $data['type'] = $this->M_product_type->fetch_table("*","a__product_type","");
            $data['api'] = $this->M_product_api->fetch_table("*","a__product_api","");
            $data['config'] = $this->M_product->get_row("value","sys__config","param='usd-to-rp'","","",FALSE);
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_list(){
         $column = 'b.name as type,c.api_name as api,a.*';
         $column_order = array(null, 'a.name','b.name','c.api_name','price_sell','min_order','max_order',null,null,null,null); //set column field database for datatable orderable
         $column_search = array('a.name','b.name','c.api_name','price_sell','min_order','max_order'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "a__product a";
         $where = "";
         $joins = array(
                array(
                    'table' => 'a__product_type b',
                    'condition' => 'a.id_type = b.id',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product_api c',
                    'condition' => 'a.id_api = c.id',
                    'jointype' => 'LEFT'
                )
            );

        $list = $this->M_product->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            if($key->is_private=="1"){ $is_private =  "checked"; $disabled =""; }else{ $is_private = ""; $disabled ="disabled"; } 
            if($key->is_active=="1"){ $is_active =  "checked"; }else{ $is_active = ""; } 
            if($key->is_auto=="1"){ $is_auto =  "checked"; }else{ $is_auto = ""; } 
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->name;
            $row[] = $key->type;
            $row[] = $key->api;
            $row[] = number_format($key->price_sell,0,'.','.');
            $row[] = $key->min_order;
            $row[] = $key->max_order;
            $row[] = '<input type="checkbox" disabled="" '.$is_private.' name="">';
            $row[] = '<input type="checkbox" disabled="" '.$is_active.' name="">';
            $row[] = '<input type="checkbox" disabled="" '.$is_auto.' name="">';
            $row[] = '<button onclick="ajax_get_view_product('.$key->id.')" class="btn btn-info btn-xs">View</button>
          <a href="'.base_url().$this->config->item('index_page').'/product/edit_product/'.$key->id.'"><button class="btn btn-success btn-xs">Edit</button></a>
          <button onclick="ajax_action_delete_product('.$key->id.')" class="btn btn-danger btn-xs">Delete</button>
          <button '.$disabled.' onclick="ajax_get_product_user('.$key->id.')" class="btn btn-warning btn-xs">Product User</button>';
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_product->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_product->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function product_user($id){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                )
            );
            $data['content'] = 'product_user';
            $data['id_product'] = $id;
            $data['product'] = $this->M_product->fetch_table("*","a__product","id='$id'");;
            $data['data'] = $this->M_product->fetch_joins("a__product_user a","b.username,a.*",$joins,"id_product='$id'",FALSE);
            $data['type'] = $this->M_product_type->fetch_table("*","a__product_type","");
            $data['api'] = $this->M_product_api->fetch_table("*","a__product_api","");
            $data['member'] = $this->M_member->fetch_table("*","m__member","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_action_product_user(){
        $this->form_validation->set_rules('member', 'member', 'required');

         if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
             $data = array(
                "id_product" => post("id"),
                "id_member" => post("member"),
            );
            $add = $this->M_product->insert_table("a__product_user",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => 
                    ''.base_url().$this->config->item('index_page').'/product/product_user/'.post("id").''
                );
                print json_encode($json_data);
            }
        }
    }

    public function ajax_action_delete_product_user(){
           $delete = $this->M_product->delete_table("a__product_user","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                     "redirect" => 
                    ''.base_url().$this->config->item('index_page').'/product/product_user/'.post("id").''
                );
                print json_encode($json_data);
            }
    }

    public function ajax_action_add_product(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('price_hpp', 'Price Hpp', 'required');
        $this->form_validation->set_rules('price_hpp_usd', 'Price Hpp Usd', 'required');
        $this->form_validation->set_rules('markup', 'Markup', 'required');
        $this->form_validation->set_rules('markup_rp', 'Markup Rp', 'required');
        $this->form_validation->set_rules('price_sell', 'Price Sell', 'required');
        $this->form_validation->set_rules('markup_rp', 'Markup Rp', 'required');
        $this->form_validation->set_rules('price_sell', 'Price Sell', 'required');
        $this->form_validation->set_rules('min_order', 'Min Order', 'required');
        $this->form_validation->set_rules('max_order', 'Max Order', 'required');
        if(post('is_auto')==1){
            /*$this->form_validation->set_rules('api_info', 'Api info', 'required');*/
            $this->form_validation->set_rules('api', 'Api', 'required');
        }


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
            "name" => post("nama"),
            "id_type" => post("type"),
            "id_api" => post("api"),
            "price_hpp" => str_replace(",","", post("price_hpp")) ,
            "price_hppusd" => str_replace(",","", post("price_hpp_usd")) ,
            "markup" => str_replace(",","", post("markup")) ,
            "markup_rp" => str_replace(",","", post("markup_rp")) ,
            "price_sell" => str_replace(",","", post("price_sell")) ,
            "min_order" => post("min_order"),
            "max_order" => post("max_order"),
            "is_auto" => post("is_auto"),
            "api_service" => post("service_id"),
            "is_private" => post("is_private"),
            "is_active" => post("is_active"),
            "is_validate_ig" => post("is_validate_ig"),
            "validate_ig" => post("validate_ig"),
            "description" => post("desc"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
        );
        $add = $this->M_product->insert_table("a__product",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product'
                );
                print json_encode($json_data);
            }
        }
        

    }

    public function ajax_action_delete_product(){
           $delete = $this->M_product->delete_table("a__product","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_get_data_edit(){
        $id = post("id");
        $data = $this->M_product->fetch_table("*","a__product","id='$id'");
        print json_encode($data);

         
    }

    public function ajax_action_edit_product(){
        
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        if(post('is_auto')==1){
            $this->form_validation->set_rules('api', 'Api', 'required');
        }
        $this->form_validation->set_rules('price_hpp', 'Price Hpp', 'required');
        $this->form_validation->set_rules('price_hpp_usd', 'Price Hpp Usd', 'required');
        $this->form_validation->set_rules('markup', 'Markup', 'required');
        $this->form_validation->set_rules('markup_rp', 'Markup Rp', 'required');
        $this->form_validation->set_rules('price_sell', 'Price Sell', 'required');
        $this->form_validation->set_rules('markup_rp', 'Markup Rp', 'required');
        $this->form_validation->set_rules('price_sell', 'Price Sell', 'required');
        $this->form_validation->set_rules('min_order', 'Min Order', 'required');
        $this->form_validation->set_rules('max_order', 'Max Order', 'required');
        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{        
            
        $data = array(
            "name" => post("nama"),
            "id_type" => post("type"),
            "id_api" => post("api"),
            "price_hpp" => str_replace(",","", post("price_hpp")) ,
            "price_hppusd" => str_replace(",","", post("price_hpp_usd")) ,
            "markup" => str_replace(",","", post("markup")) ,
            "markup_rp" => str_replace(",","", post("markup_rp")) ,
            "price_sell" => str_replace(",","", post("price_sell")) ,
            "min_order" => post("min_order"),
            "max_order" => post("max_order"),
            "is_auto" => post("is_auto"),
            "api_service" => post("service_id"),
            "is_private" => post("is_private"),
            "is_active" => post("is_active"),
            "is_validate_ig" => post("is_validate_ig"),
            "validate_ig" => post("validate_ig"),
            "description" => post("desc"),
            "update_at" => date("Y-m-d H:i:s")
        );
        $edit = $this->M_product->update_table("a__product",$data,"id",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product'
                );
                print json_encode($json_data);
            }
         

        }

    }

    
}

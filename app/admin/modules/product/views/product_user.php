<!-- Modal -->
<div class="modal fade" id="modal_product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <form id="form_product">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><center>Product User</center></h4>
      </div>
      <div class="modal-body">
        <select id="member" class="select2 form-control" style="width: 100%;">
          
          <?php foreach ($member as $member) { ?>
              <option value="<?php echo $member->id_member; ?>"><?php echo $member->username; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" onclick="ajax_action_product_user('<?php echo $id_product; ?>')" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<b>Product User</b>
<button data-toggle="modal" data-target="#modal_product" class="btn btn-primary btn-xs" style="float: right;">Tambah Product User</button>
<hr>

<div class="row">
  <div class="col-md-5" style="background: #ccc;">
    <?php foreach ($product as $key) { ?>
    <table >
          <tr>
            <td>
              <label>Nama Product :</label>
              <?php echo $key->name; ?>              
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Sell :</label>
              <?php echo $key->price_sell; ?>              
            </td>
          </tr>
          <tr>
            <td>
              <label>Min Order :</label>
              <?php echo $key->min_order; ?>              
            </td>
          </tr>
          <tr>
            <td>
              <label>Max Order :</label>
              <?php echo $key->max_order; ?>              
            </td>
          </tr>
    </table>
    <?php } ?>
  </div>
  <div class="col-md-6">
    <table class="table">
      <thead>
        <tr>
          <th><center>No</center></th>
          <th><center>Member</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; foreach ($data as $value) { ?>
          <tr>
            <td align="center"><?php echo $no; ?></td>
            <td align="center"><?php echo $value->username; ?></td>
            <td align="center">
              <button onclick="ajax_action_delete_product_user('<?php echo $value->id; ?>')" class="btn btn-danger btn-sm">Delete</button>
            </td>
          </tr>
        <?php $no++; } ?>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
  function ajax_action_product_user(id) {
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_product_user/",
              type:'POST',
              dataType: "json",
              data: {member:$('#member').val(), id:id},
              success: function(data) {
                  if(data.result){
                    alert(data.message.body);
                    setTimeout(function(){window.location = data.redirect},500);
                  }else{
                    alert(data.message.body);
                  }
              }
          });
  }

   function ajax_action_delete_product_user(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_delete_product_user/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              success: function(data) {
                    if(data.result){
                      alert(data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      alert(data.message.body);
                    }
                  
              }
          });
    }
  }
</script>
<div class="card-header bg-success">
        <h4 style="color: white;">Edit Product</h4>
</div>
<?php foreach ($data as $key) { ?>
<form method="POST" onsubmit="return ajax_action_edit_product();">
      <div class="modal-body">
        <input type="hidden" value="<?php echo $key->id; ?>" name="id" id="id">
        <table class="table">
          <tr>
            <td style="width: 50%;">
              <label>Nama :</label>
              <input type="text" name="nama" id="e_nama" value="<?php echo $key->name; ?>" class="form-control">
            </td>
            <td style="width: 50%;">
              <label>Type :</label>
              <select name="type" id="e_type" class="form-control">
                <option value=""></option>
                <?php foreach ($type as $type) { ?>
                  <option <?php if($key->id_type==$type->id){ echo "selected"; } ?> value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Api </label><a href="#" id="lbl-lookup" data-toggle="modal" data-target="#modal_lookup" style="float: right; display: none;"> Lookup</a><br>
              <select onchange="auto_product(this)" name="api" id="e_api" class="form-control">
                <option value=""></option>
                <?php foreach ($api as $api) { ?>
                  <option <?php if($key->id_api==$api->id){ echo "selected"; } ?> value="<?php echo $api->id; ?>"><?php echo $api->api_name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
                <label>Service Id</label>
                <input disabled="" value="<?php echo $key->api_service; ?>" type="text" name="service_id" id="service_id" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <table style="width: 100%">
                <tr>
                  <td>
                    <label>Is Auto </label><br>
                    <input disabled="" type="checkbox" name="is_auto" <?php if($key->is_auto=="1"){ echo "checked"; } ?> value="1" id="e_is_auto">
                  </td>
                  <td>
                    <label>Is Active </label><br>
                    <input type="checkbox" name="is_active" <?php if($key->is_active=="1"){ echo "checked"; } ?> value="1" id="e_is_active">
                  </td>
                  <td>
                    <label>Is private </label><br>
                    <input type="checkbox" name="is_private" <?php if($key->is_private=="1"){ echo "checked"; } ?> value="1" id="e_is_private">
                  </td>
                  <td>
                    <label>Is validate ig </label><br>
                    <input type="checkbox" name="is_validate_ig" <?php if($key->is_validate_ig=="1"){ echo "checked"; } ?> value="1" id="e_is_validate_ig">
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <label>Validate Ig :</label>
                    <input type="text" value="<?php echo $key->validate_ig ?>" class="form-control" id="e_validate_ig" name="">
                  </td>
                </tr>
              </table>
            </td>
            <td>
              <label>Description :</label>
              <textarea rows="5" class="form-control" id="e_desc"><?php echo $key->description ?></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Hpp Usd :</label>
              <input onkeyup="set_price_hpp()" value="<?php echo $key->price_hppusd; ?>" type="text" name="price_hpp_usd" id="e_price_hpp_usd" class="form-control ">
            </td>
            <td>
              <label>Price Hpp :</label>
              <input type="hidden" id="usd_rp" value="<?php echo $config->value; ?>">
              <input value="<?php echo $key->price_hpp; ?>" type="text" name="price_hpp" id="e_price_hpp" class="form-control ">
            </td>
          </tr>
          <tr>
            <td>
              <label>Markup :</label>
              <input value="<?php echo $key->markup; ?>" onkeyup="set_markup_rp()" type="text" name="markup" id="e_markup" class="form-control ">
            </td>
            <td>
              <label>Markup Rp :</label>
              <input value="<?php echo $key->markup_rp; ?>" type="text" name="markup_rp" id="e_markup_rp" class="form-control ">
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Sell :</label>
              <input value="<?php echo $key->price_sell; ?>" type="text" name="price_sell" id="e_price_sell" class="form-control ">
            </td>
            <td>
             <td></td>
                  <!-- <td>
                    <label>Api Info :</label>
                    <textarea class="form-control" name="api_info" id="e_api_info" rows="2"><?php echo $key->api_info; ?></textarea>
                  </td> -->
              
          </tr>
          <tr>
            <td>
              <label>Min Order :</label>
              <input value="<?php echo $key->min_order; ?>" type="number" name="min_order" id="e_min_order" class="form-control">
            </td>
            <td>
              <label>Max Order :</label>
              <input value="<?php echo $key->max_order; ?>" type="number" name="max_order" id="e_max_order" class="form-control">
            </td>
          </tr>
        </table>
        <div id="alert_product"></div>
      </div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/product"><button type="button" class="btn btn-danger" >Back</button></a>
        <button type="submit" id="btn_product" class="btn btn-success">Save</button>
      </div>
      </form>
<?php } ?>


<script type="text/javascript">
  function set_price_hpp(){
    var usd  = $('#e_price_hpp_usd').val();
    if(usd=="" || usd=="0" ){
      $('#e_price_hpp').val("0");
    }else{
      var hasil = parseFloat(usd) * parseFloat($('#usd_rp').val());
      $('#e_price_hpp').val(hasil.toFixed(2));
    }
    set_markup_rp();
  }

  function set_markup_rp(){
    var markup = $('#e_markup').val();
    if(markup=="" || markup=="0" ){
      $('#e_markup_rp').val("0");
      $('#e_price_sell').val("0");
    }else{
      var hasil = parseFloat(markup) /100 * parseFloat($('#e_price_hpp').val());
      $('#e_markup_rp').val(hasil.toFixed(2));
      $('#e_price_sell').val(parseFloat(hasil)+parseFloat($('#e_price_hpp').val()));
    }
  }

  function auto_product(key) {
    var getval = $('#e_api option:selected').text();
    if(getval==""){
      $('#lbl-lookup').hide();
      $('#e_is_auto').prop("checked", false);
    }else{
      $('#lbl-lookup').show();
      $('#e_is_auto').prop("checked", true);
      $('#search_lookup').val("");
      $('#list-lookup').html(""); 
    }
  }

  function ajax_action_lookup(key){
    var search = key.value;
    var api = $('#e_api option:selected').text();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_lookup/",
              type:'POST',
              dataType: "json",
              data: {search:search, api:api},
              beforeSend: function(){
                  $('#list-lookup').html("<tr><td colspan='7'><center>Loading....</center></td></tr>");
                  $('#page-load').show();
              },
              success: function(data) {
                  $('#page-load').hide();
                 var trHTML = '';
                 for(i = 0; i<data.length; i++){
                  var setname = "data[i].name"; 
                    trHTML += '<tr><td>' + data[i].service + '</td><td>' + data[i].name + '</td><td>' + data[i].category + '</td><td>' + data[i].rate + '</td><td>' + data[i].min + '</td><td>' + data[i].max + '</td><td><button class="btn btn-success" onclick="set_lookup('+"'"+data[i].service+"',"+"'"+data[i].name+"',"+"'"+data[i].rate+"',"+"'"+data[i].min+"',"+"'"+data[i].max+"'"+')" id="apply">Apply</button></td></tr>';

                 }
                 if (data.length=="0") {
                  $('#list-lookup').html("<tr><td colspan='7'><center>Data Not Found</center></td></tr>");
                 }else{
                  $('#list-lookup').html(trHTML); 
                 }
                 
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function set_lookup(service,name,rate,min,max){
    $('#service_id').val(service);
    $('#e_nama').val(name);
    $('#e_price_hpp_usd').val(rate);
    $('#e_max_order').val(max);
    $('#e_min_order').val(min);
    $('#modal_lookup').modal('toggle');
    set_price_hpp();
  }

  function ajax_action_edit_product(){
    var id = $("#id").val();
    var nama = $("#e_nama").val();
    var type = $("#e_type").val();
    var api = $("#e_api").val();
    var price_hpp = $("#e_price_hpp").val();
    var price_hpp_usd = $("#e_price_hpp_usd").val();
    var markup = $("#e_markup").val();
    var markup_rp = $("#e_markup_rp").val();
    var price_sell = $("#e_price_sell").val();
    var min_order = $("#e_min_order").val();
    var max_order = $("#e_max_order").val();
    var desc = $("#e_desc").val();
    var validate_ig = $("#e_validate_ig").val();
    //var api_info = $("#e_api_info").val();
    var api_info = "";
    var service_id = $("#service_id").val();
    if ($('#e_is_private').is(':checked')) {
      var is_private = 1;
    }else{
      var is_private = 0;
    }
    if ($('#e_is_active').is(':checked')) {
      var is_active = 1;
    }else{
      var is_active = 0;
    }
    if ($('#e_is_auto').is(':checked')) {
      var is_auto = 1;
    }else{
      var is_auto = 0;
    }
    if ($('#e_is_validate_ig').is(':checked')) {
      var is_validate_ig = 1;
    }else{
      var is_validate_ig = 0;
    }

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_edit_product/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, type:type,api:api, price_hpp:price_hpp,price_hpp_usd:price_hpp_usd, markup:markup,markup_rp:markup_rp, price_sell:price_sell,min_order:min_order, max_order:max_order, is_private:is_private,is_active:is_active, is_auto:is_auto, api_info:api_info, id:id,service_id:service_id,is_validate_ig:is_validate_ig,validate_ig:validate_ig,desc:desc},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
  
</script>
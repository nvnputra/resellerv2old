<div class="card-header bg-primary">
        <h4 style="color: white;">New Product</h4>
</div>
<form method="POST" onsubmit="return ajax_action_add_product();">
      <div class="modal-body">
        <table class="table">
          <tr>
            <td style="width: 50%;">
              <label>Nama :</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </td>
            <td style="width: 50%;">
              <label>Type :</label>
              <select name="type" id="type" class="form-control">
                <option value=""></option>
                <?php foreach ($type as $type) { ?>
                  <option value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Api </label><a href="#" id="lbl-lookup" data-toggle="modal" data-target="#modal_lookup" style="float: right; display: none;"> Lookup</a><br>
              <select onchange="auto_product(this)" name="api" id="api" class="form-control">
                <option value=""></option>
                <?php foreach ($api as $api) { ?>
                  <option value="<?php echo $api->id; ?>"><?php echo $api->api_name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
                    <label>Service Id</label>
                    <input disabled="" type="text" name="service_id" id="service_id" class="form-control">
              </td>
          </tr>
          <tr> 
            <td>
              <table style="width: 100%;">
                <tr>
                  <td>
                    <label>Is Auto </label><br>
                    <input type="checkbox" disabled="" name="is_auto" value="1" id="is_auto">
                  </td>
                  <td>
                    <label>Is Active </label><br>
                    <input type="checkbox" checked="" name="is_active" value="1" id="is_active">
                  </td>
                  <td>
                    <label>Is private </label><br>
                    <input type="checkbox" name="is_private" value="1" id="is_private">
                  </td>
                  <td>
                    <label>Is validate ig </label><br>
                    <input type="checkbox" name="is_validate_ig" value="1" id="is_validate_ig">
                  </td>
                </tr>
                <tr>
                  <td colspan="4">
                    <label>Validate Ig :</label>
                    <span style="font-size: 12px;">ex : url:instagram.com,username:1,available:1</span>
                    <input type="text" class="form-control" id="validate_ig" name="">
                  </td>
                </tr>
              </table>
            </td>
            <td>
              <label>Description :</label>
              <textarea rows="5" class="form-control" id="desc"></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Hpp Usd :</label>
              <input onkeyup="set_price_hpp()" type="text" name="price_hpp_usd" id="price_hpp_usd"  class="form-control">
            </td>
            <td>
              <label>Price Hpp :</label>
              <input type="hidden" id="usd_rp" value="<?php echo $config->value; ?>">
              <input type="text" disabled="" name="price_hpp" id="price_hpp" class="form-control ">
            </td>
          </tr>
          <tr>
            <td>
              <label>Markup :</label>
              <input type="text" onkeyup="set_markup_rp()" name="markup" id="markup" class="form-control ">
            </td>
            <td>
              <label>Markup Rp :</label>
              <input type="text" name="markup_rp" id="markup_rp" class="form-control ">
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Sell :</label>
              <input type="text" name="price_sell" id="price_sell" class="form-control ">
            </td>
            <td>
              <td></td>
                  <!-- <td>
                    <label>Api Info :</label>
                    <textarea class="form-control" name="api_info" id="api_info" rows="2"></textarea>
                  </td> -->
          </tr>
          <tr>
            <td>
              <label>Min Order :</label>
              <input type="number" name="min_order" id="min_order" class="form-control">
            </td>
            <td>
              <label>Max Order :</label>
              <input type="number" name="max_order" id="max_order" class="form-control">
            </td>
          </tr>
        </table>
        <div id="alert_product"></div>
      </div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/product"><button type="button" class="btn btn-danger" >Back</button></a>
        <button type="submit" id="btn_product" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function set_price_hpp(){
    var usd  = $('#price_hpp_usd').val();
    if(usd=="" || usd=="0" ){
      $('#price_hpp').val("0");
    }else{
      var hasil = parseFloat(usd) * parseFloat($('#usd_rp').val());
      $('#price_hpp').val(hasil.toFixed(2));
    }
    set_markup_rp();
  }

  function set_markup_rp(){
    var markup = $('#markup').val();
    if(markup=="" || markup=="0" ){
      $('#markup_rp').val("0");
      $('#price_sell').val("0");
    }else{
      var hasil = parseFloat(markup) /100 * parseFloat($('#price_hpp').val());
      $('#markup_rp').val(hasil.toFixed(2));
      $('#price_sell').val(parseFloat(hasil)+parseFloat($('#price_hpp').val()));
    }
  }

  function auto_product(key) {
    var getval = $('#api option:selected').text();
    if(getval==""){
      $('#lbl-lookup').hide();
      $('#is_auto').prop("checked", false);
    }else{
      $('#lbl-lookup').show();
      $('#is_auto').prop("checked", true);
      $('#search_lookup').val("");
      $('#list-lookup').html(""); 
    }
  }

  function ajax_action_lookup(key){
    var search = key.value;
    var api = $('#api option:selected').text();
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_lookup/",
              type:'POST',
              dataType: "json",
              data: {search:search, api:api},
              beforeSend: function(){
                  $('#list-lookup').html("<tr><td colspan='7'><center>Loading....</center></td></tr>");
                  $('#page-load').show();
              },
              success: function(data) {
                  $('#page-load').hide();
                 var trHTML = '';
                 for(i = 0; i<data.length; i++){
                  var setname = "data[i].name"; 
                    trHTML += '<tr><td>' + data[i].service + '</td><td>' + data[i].name + '</td><td>' + data[i].category + '</td><td>' + data[i].rate + '</td><td>' + data[i].min + '</td><td>' + data[i].max + '</td><td><button class="btn btn-success" onclick="set_lookup('+"'"+data[i].service+"',"+"'"+data[i].name+"',"+"'"+data[i].rate+"',"+"'"+data[i].min+"',"+"'"+data[i].max+"'"+')" id="apply">Apply</button></td></tr>';

                 }
                 if (data.length=="0") {
                  $('#list-lookup').html("<tr><td colspan='7'><center>Data Not Found</center></td></tr>");
                 }else{
                  $('#list-lookup').html(trHTML); 
                 }
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }


  function set_lookup(service,name,rate,min,max){
    $('#service_id').val(service);
    $('#nama').val(name);
    $('#price_hpp_usd').val(rate);
    $('#max_order').val(max);
    $('#min_order').val(min);
    $('#modal_lookup').modal('toggle');
    set_price_hpp();
  }

   function ajax_action_add_product() {

    var nama = $("#nama").val();
    var type = $("#type").val();
    var api = $("#api").val();
    var price_hpp = $("#price_hpp").val();
    var price_hpp_usd = $("#price_hpp_usd").val();
    var markup = $("#markup").val();
    var markup_rp = $("#markup_rp").val();
    var price_sell = $("#price_sell").val();
    var min_order = $("#min_order").val();
    var max_order = $("#max_order").val();
    var desc = $("#desc").val();
    var validate_ig = $("#validate_ig").val();
    //var api_info = $("#api_info").val();
    var api_info = "";
    var service_id = $("#service_id").val();
    if ($('#is_private').is(':checked')) {
      var is_private = 1;
    }else{
      var is_private = 0;
    }
    if ($('#is_active').is(':checked')) {
      var is_active = 1;
    }else{
      var is_active = 0;
    }
    if ($('#is_auto').is(':checked')) {
      var is_auto = 1;
    }else{
      var is_auto = 0;
    }
    if ($('#is_validate_ig').is(':checked')) {
      var is_validate_ig = 1;
    }else{
      var is_validate_ig = 0;
    }

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_add_product/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, type:type,api:api, price_hpp:price_hpp,price_hpp_usd:price_hpp_usd, markup:markup,markup_rp:markup_rp, price_sell:price_sell,min_order:min_order, max_order:max_order, is_private:is_private,is_active:is_active, is_auto:is_auto,service_id:service_id,is_validate_ig:is_validate_ig,validate_ig:validate_ig,desc:desc},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }   
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
</script>
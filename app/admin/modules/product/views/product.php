<?php 
$data_a = $type;
$data_b = $api;

?>
<style type="text/css">
  #list_product td{
    text-align: center;
  }
</style>


<!-- Modal -->
<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form >
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><center>View Product</center></h4>
      </div>
      <div class="modal-body">
        <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" readonly="" name="nama" id="v_nama" class="form-control">
            </td>
            <td>
              <label>Is Auto :</label><br>
              <input type="checkbox" name="is_auto" readonly="" value="1" id="v_is_auto">
            </td>
          </tr>
          <tr>
            <td>
              <label>Is Active :</label><br>
              <input type="checkbox" name="is_active" value="1" readonly="" id="v_is_active">
            </td>
            <td>
              <label>Is private :</label><br>
              <input type="checkbox" name="is_private" value="1" readonly="" id="v_is_private">
            </td>
          </tr>
          <tr>
            <td>
              <label>Type :</label>
              <select name="type" id="v_type" class="form-control" readonly="">
                <option value=""></option>
                <?php foreach ($data_a as $type) { ?>
                  <option value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Api </label><br>
              <select name="api" id="v_api" class="form-control" readonly="">
                <option value=""></option>
                <?php foreach ($data_b as $api) { ?>
                  <option value="<?php echo $api->id; ?>"><?php echo $api->api_name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Hpp :</label>
              <input type="text" name="price_hpp" id="v_price_hpp" readonly="" class="form-control autonumber">
            </td>
            <td>
              <label>Price Hpp Usd :</label>
              <input type="text" name="price_hpp_usd" readonly="" id="v_price_hpp_usd" class="form-control autonumber">
            </td>
          </tr>
          <tr>
            <td>
              <label>Markup :</label>
              <input type="text" name="markup" id="v_markup" readonly="" class="form-control autonumber">
            </td>
            <td>
              <label>Markup Rp :</label>
              <input type="text" name="markup_rp" id="v_markup_rp" readonly="" class="form-control autonumber">
            </td>
          </tr>
          <tr>
            <td>
              <label>Price Sell :</label>
              <input type="text" name="price_sell" id="v_price_sell" readonly="" class="form-control autonumber">
            </td>
            <td>
              <label>Api Info :</label>
              <textarea class="form-control" name="api_info" id="v_api_info" readonly="" rows="2"></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <label>Min Order :</label>
              <input type="number" name="min_order" id="v_min_order" readonly="" class="form-control">
            </td>
            <td>
              <label>Max Order :</label>
              <input type="number" name="max_order" id="v_max_order" readonly="" class="form-control">
            </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!------------ ---------------->



<a href="<?=base_url().$this->config->item('index_page'); ?>/product/new_product/"><button class="btn btn-primary btn-sm">New Product</button></a>
<br><br>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped " id="list_product">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>Nama</center></th>
      <th><center>Type</center></th>
      <th><center>Api</center></th>
      <th><center>Price Sell</center></th>
      <th><center>Min Order</center></th>
      <th><center>Max Order</center></th>
      <th><center>Private</center></th>
      <th><center>Active</center></th>
      <th><center>Auto</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>

  </tbody>
</table>
</div>

<script type="text/javascript">
  

 
  function ajax_action_delete_product(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_action_delete_product/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_product(id){
    $('#id').val(id);
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_get_data_edit/",
              method:'POST',
              dataType: "json",
              data: {id:id},
              success: function(data) {
                var i ;
                for(i = 0; i<data.length; i++){
                     $("#e_nama").val(data[i].name);
                     $("#e_type").val(data[i].id_type);
                     $("#e_api").val(data[i].id_api);
                     $("#e_price_hpp").val(data[i].price_hpp);
                     $("#e_price_hpp_usd").val(data[i].price_hppusd);
                     $("#e_markup").val(data[i].markup);
                     $("#e_markup_rp").val(data[i].markup_rp);
                     $("#e_price_sell").val(data[i].price_sell);
                     $("#e_min_order").val(data[i].min_order);
                     $("#e_max_order").val(data[i].max_order);
                     $("#e_api_info").val(data[i].api_info);

                     if(data[i].is_private=="1"){
                      $('#e_is_private').prop('checked', true);
                    }else{
                      $('#e_is_private').prop('checked', false);
                    }

                    if(data[i].is_active=="1"){
                      $('#e_is_active').prop('checked', true);
                    }else{
                      $('#e_is_active').prop('checked', false);
                    }

                    if(data[i].is_auto=="1"){
                      $('#e_is_auto').prop('checked', true);
                    }else{
                      $('#e_is_auto').prop('checked', false);
                    }


                } 

                $('#modal_edit').modal('show');      
              }
          });
  }


  function ajax_get_view_product(id){
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_get_data_edit/",
              method:'POST',
              dataType: "json",
              data: {id:id},
              success: function(data) {
                var i ;
                for(i = 0; i<data.length; i++){
                     $("#v_nama").val(data[i].name);
                     $("#v_type").val(data[i].id_type);
                     $("#v_api").val(data[i].id_api);
                     $("#v_price_hpp").val(data[i].price_hpp);
                     $("#v_price_hpp_usd").val(data[i].price_hppusd);
                     $("#v_markup").val(data[i].markup);
                     $("#v_markup_rp").val(data[i].markup_rp);
                     $("#v_price_sell").val(data[i].price_sell);
                     $("#v_min_order").val(data[i].min_order);
                     $("#v_max_order").val(data[i].max_order);
                     $("#v_api_info").val(data[i].api_info);

                     if(data[i].is_private=="1"){
                      $('#v_is_private').prop('checked', true);
                    }else{
                      $('#v_is_private').prop('checked', false);
                    }

                    if(data[i].is_active=="1"){
                      $('#v_is_active').prop('checked', true);
                    }else{
                      $('#v_is_active').prop('checked', false);
                    }

                    if(data[i].is_auto=="1"){
                      $('#v_is_auto').prop('checked', true);
                    }else{
                      $('#v_is_auto').prop('checked', false);
                    }


                } 

                $('#modal_view').modal('show');      
              }
          });
  }

  function ajax_get_product_user(id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/product/product_user/"+id;
  }




</script>
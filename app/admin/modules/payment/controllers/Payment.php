<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller  {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_payment");
      $this->load->library('session');
      $this->load->library('form_validation');
    }

    public function index()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
          
            $data['content'] = 'payment';
            $data['pending_order'] = $this->M_payment->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_payment->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }   
    }

    public function ajax_list(){
         $column = 'a.*,b.username';
         $column_order = array(null, 'no_transaction','type','username','nominal_transfer','nominal_topup','status'); //set column field database for datatable orderable
         $column_search = array('no_transaction','type','username','nominal_transfer','nominal_topup','status'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "m__payment a";
         $where = "";
         $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                )
            );

        $list = $this->M_payment->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            if($key->status=="PENDING"){
            $class = "btn-warning";
            }else if($key->status=="PAID"){
              $class = "btn-success";
            }else if($key->status=="FRAUD"){
              $class = "btn-info";
            }else if($key->status=="CANCEL"){
              $class = "btn-danger";
            } else{
              $class = "btn-default";
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->no_transaction;
            $row[] = $key->type;
            $row[] = $key->username;
            $row[] = $key->nominal_transfer;
            $row[] = $key->nominal_topup;
            $row[] = '<button onclick="ajax_get_status('.$key->id.')" class="btn btn-xs '.$class.'">'.$key->status.'</button>';
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_payment->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_payment->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_status(){
        $id = post("id");
        $data = array();
        $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'm__payment_account c',
                    'condition' => 'c.id_payment_account = a.m_id_paymentaccount',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__payment_method d',
                    'condition' => 'd.id  = c.id_payment_method',
                    'jointype' => ''
                ),

            );
        $joins2 = array(
                array(
                    'table' => 'a__payment_method d',
                    'condition' => 'd.id  = a.id_payment_method',
                    'jointype' => ''
                ),

            );
        $data['result'] = $this->M_payment->fetch_joins("m__payment a","a.*,b.username,d.name as method",$joins,"a.id = '$id' ",FALSE);
        $data['admin'] = $this->M_payment->fetch_joins("a__payment_account a","a.*,d.name as method",$joins2,"",FALSE);
        echo json_encode($data);
    }

    public function ajax_action_set_status(){
            /*validasi status tidak berubah*/
            if (post("old_status")==post("status")) {
              $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Tidak Merubah Status'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

            $id_member = post("id_member");
            $data = array(
                "a_id_paymentaccount" => post("admin_account"),
                "status" => post("status"),
                "action_by" => $this->session->userdata('resv2.a_id'),
                "action_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            );
            $data_notif = array(
              "id_user" => $id_member,
              "created_at" => date("Y-m-d H:i:s"),
              "text" => "Payment ".post("no_transaction")." is ".post("status")
            );

            /*validasi data upgrade data premium*/
            if(post('type')=="UPGRADE" and post("status")=="PAID"){
              $data_premium = array(
                "is_premium"=>1,
                "premium_payment_id"=>post("id"),
                "premium_at"=>date("Y-m-d H:i:s")
              );
            }else if(post('type')=="UPGRADE" and post("status")!="PAID"){
              $data_premium = array(
                "is_premium"=>0,
                "premium_at"=>null
              );
            }else{
              $data_premium = null;
            }


            // validasi data balance dan data mutasi
            $get_data = $this->M_payment->get_row("balance","m__member","id_member='$id_member' ","","",FALSE); 
            if((post("status")=="FRAUD" || post("status")=="CANCEL" || post("status")=="PENDING")  && post("old_status")=="PAID"  ){
              $set_balance = array(
                  "balance" => $get_data->balance - post("nominal_topup")
              );
              $set_mutasi = "delete_mutasi";
            }else if(post("status")=="PAID"){
              $set_balance = array(
                  "balance" => $get_data->balance + post("nominal_topup")
              );
              $set_mutasi = array(
                "id_user"=> $id_member,
                "debt" => post("nominal_topup"),
                "balance" => $get_data->balance + post("nominal_topup"),
                "id_payment" => post("id"),
                "created_at" => date("Y-m-d H:i:s"),
                "update_at" => date("Y-m-d H:i:s")
              );
            }else{
              $set_mutasi = null;
              $set_balance = null;
            }

            $edit = $this->M_payment->update_status(post("type"),$data,post("id"),$set_balance,$id_member,$data_notif,$data_premium,$set_mutasi);
            


            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Status'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
              /*validasi kirim email*/
              if (post("status")=="PAID") {
                $this->send_verify($id_member,post("no_transaction"));
              }
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit Status'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment'
                );
                print json_encode($json_data);
            }
          
    }


    public function send_verify($id_member,$no_transaction){
        $this->load->library('ElasticEmail');
        $data_user = $this->M_payment->get_row("*","m__member","id_member='$id_member' ","","",FALSE);

        $Elastic = new ElasticEmail();
        $admin = "admin@resellerindo.com";
        $from = $admin;
        $fromName = "resellerindo";
        $to = $data_user->email;
        $subject = "Payment Paid";
        $bodyText = ""; 
        $bodyHtml = 
        "<b>Hello ".$data_user->username." ,</b><br><br><br>
        Email Address : ".$data_user->email."<br>
        Thanks for add payment in Resellerindo!  <br>
        We're happy you're here. Your payment ".$no_transaction." is paid by admin resellerindo .
        <br><br><br>
        Thanks,<br>
        Admin Resellerindo
        "; 

        $send = $Elastic->send($from,$fromName,$subject,$to,$bodyText,$bodyHtml);
        $send = json_decode($send);
        
        return $send->success;
    }
        
}

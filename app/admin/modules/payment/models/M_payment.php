<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_payment extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }

  public function update_status($type,$data_update,$id_update,$data_balance,$id_member,$data_notif,$data_premium,$set_mutasi){
  	$this->db->trans_begin();

  	    $this->update_table("m__payment",$data_update,"id",$id_update);
     if(($data_balance!="" || $data_balance!= null) and $type == "TOPUP"){
     	  $this->update_table("m__member",$data_balance,"id_member",$id_member);
     }
     if(($data_premium!="" || $data_premium!= null) and $type == "UPGRADE"){
        $this->update_table("m__member",$data_premium,"id_member",$id_member);
     }

     if($set_mutasi=="delete_mutasi"){
        $this->delete_table("m__mutation","id_payment",$id_update);
     }else if(($set_mutasi!= "" || $set_mutasi!= null) AND $type=="TOPUP"){
        $this->db->insert('m__mutation', $set_mutasi);
     }

        $this->db->insert('m__notifications', $data_notif);

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }
  }
  
}
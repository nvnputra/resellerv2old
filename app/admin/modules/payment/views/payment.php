<style type="text/css">
  a{
    cursor: pointer;
  }
  #list_payment td{
    text-align: center;
  }
</style>
<br>

<table class="table" id="list_payment">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>No_transaction</center></th>
      <th><center>Type</center></th>
      <th><center>Member</center></th>
      <th><center>Nominal Transfer</center></th>
      <th><center>Nominal Topup</center></th>
      <th><center>Status</center></th>
    </tr>
  </thead>
  <tbody>
  
  </tbody>
</table>


<script type="text/javascript">

  function ajax_get_status(id) {
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_get_status/",
              type:'POST',
              dataType: "json",
              data: { id:id },
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                var html = "<option value=''>- Select Admin Account -</option>";
                var a;
                for(a=0; a<data.admin.length; a++){
                  html += "<option value='"+data.admin[a].id_payment_account+"'>"+data.admin[a].method+" - "+data.admin[a].account_number+"</option>";
                }
                $('#e_a_account').html(html);

                var i ;
                for(i = 0; i<data.result.length; i++){
                  $("#e_no_transaction").val(data.result[i].no_transaction);
                  $("#e_type").val(data.result[i].type);
                  $("#e_member").val(data.result[i].username);
                  $("#e_nominal_transfer").val(data.result[i].nominal_transfer);
                  $("#e_nominal_topup").val(data.result[i].nominal_topup);
                  $("#e_status").val(data.result[i].status);
                  $("#old_status").val(data.result[i].status);
                  $("#e_id").val(data.result[i].id);
                  $("#e_id_member").val(data.result[i].id_member);
                  $("#e_m_account").val(data.result[i].method+" - "+data.result[i].sender_accountnumber);
                  document.getElementById("e_image").src = "<?php echo base_url(); ?>uploads/file_proof/"+data.result[i].file_proof;
                  $('#e_a_account').val(data.result[i].a_id_paymentaccount);
                }
                
                $('#page-load').hide();
                $('#modal_status').modal('show');
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_set_status(){
    var status =  $('#e_status').val();
    var id =  $('#e_id').val();
    var id_member =  $('#e_id_member').val();
    var nominal_topup =  $('#e_nominal_topup').val();
    var no_transaction =  $('#e_no_transaction').val();
    var old_status =  $('#old_status').val();
    var type =  $('#e_type').val();
    var admin_account = $('#e_a_account').val();
    
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_action_set_status/",
              type:'POST',
              dataType: "json",
              data: { no_transaction:no_transaction,status:status,id:id,id_member:id_member,nominal_topup:nominal_topup,old_status:old_status,type:type,admin_account:admin_account},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

</script>
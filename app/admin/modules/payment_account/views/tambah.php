 <div class="card-header bg-primary">
      <h4 style="color: white;" class="modal-title" id="title-tambah">New Payment Account</h4>
 </div>
 <form method="POST" onsubmit="return ajax_action_add_account();">
        <table class="table">
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="method" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Type :</label><br>
              <select name="type" id="type" class="form-control">
                <option value=""></option>
                <option value="AUTO">Auto</option>
                <option value="MANUAL">Manual</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" name="account_number" class="form-control" id="account_number">
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" name="account_name" class="form-control" id="account_name">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Branch</label><br>
              <input type="text" name="account_branch" class="form-control" id="account_branch">
            </td>
            <td>
              <label>Key :</label>
              <input type="text" name="key" id="key" class="form-control ">
            </td>
          </tr>
          <tr>
            <td>
              <label>Offline time :</label>
              <input type="time" name="offline_time" id="offline_time" class="form-control ">
            </td>
            <td>
              <label>Online time :</label>
              <input type="time" name="online_time" id="online_time" class="form-control ">
            </td>
          </tr>
        </table>
      <div id="alert_payment_acc"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/payment_account"><button type="button" class="btn btn-danger" >Back</button></a>
        <button type="submit" id="btn_payment_acc" class="btn btn-primary">Save</button>
      </div>
</form>
<script type="text/javascript">
  function ajax_action_add_account() {

    var text_method = $("#method option:selected").text();
    var method = $("#method").val();
    var type = $("#type").val();
    var account_number = $("#account_number").val();
    var account_name = $("#account_name").val();
    var account_branch = $("#account_branch").val();
    var key = $("#key").val();
    var offline_time = $("#offline_time").val();
    var online_time = $("#online_time").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_add_account/",
              type:'POST',
              dataType: "json",
              data: { method:method,type:type,account_number:account_number,account_name:account_name,account_branch:account_branch,key:key,offline_time:offline_time,online_time:online_time,text_method:text_method },
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
</script>
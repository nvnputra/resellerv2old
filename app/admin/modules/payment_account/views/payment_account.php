<style type="text/css">
  #list_payment_acc td{
    text-align: center;
  }
</style>
<!-- Modal -->
<?php 
$data_method = $method;

?>



<a href="<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/tambah/""><button class="btn btn-primary btn-sm" >New Payment Account</button></a>
<br>
<table class="table" id="list_payment_acc">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>Paymed Method</center></th>
      <th><center>Type</center></th>
      <th><center>Account Number</center></th>
      <th><center>Account Name</center></th>
      <th><center>Account Branch</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
 

  </tbody>
</table>


<script type="text/javascript">
  

  
  function ajax_action_delete_account(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_delete_account/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_data_account(id){
   window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/edit_payment_account/"+id;

  }

  

</script>
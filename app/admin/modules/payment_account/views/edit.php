 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Payment Account</h4>
 </div>
  <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}

 foreach ($data as $key ) { ?>
<form method="POST" onsubmit="return ajax_action_edit_account();">
        <input type="hidden" value="<?php echo $key->id_payment_account; ?>" id="id" name="id">
        <table class="table">
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="e_method" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option <?php if ($key->id_payment_method==$method->id) { echo "selected"; } ?> value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Type :</label><br>
              <select name="type" id="e_type" class="form-control">
                <option value=""></option>
                <option <?php if ($key->type=="AUTO") { echo "selected"; } ?> value="AUTO">Auto</option>
                <option <?php if ($key->type=="MANUAL") { echo "selected"; } ?> value="MANUAL">Manual</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" value="<?php echo $key->account_number; ?>" name="account_number" class="form-control" id="e_account_number">
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" value="<?php echo $key->account_name; ?>" name="account_name" class="form-control" id="e_account_name">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Branch</label><br>
              <input type="text" value="<?php echo $key->account_branch; ?>" name="account_branch" class="form-control" id="e_account_branch">
            </td>
             <td>
              <label>Key :</label>
              <input type="text" value="<?php echo $key->key; ?>" name="key" id="e_key" class="form-control ">
            </td>   
          </tr>
          <tr>
            <td>
              <label>Offline time :</label>
              <input type="time" value="<?php echo $key->offline_time; ?>" name="offline_time" id="e_offline_time" class="form-control ">
            </td>
            <td>
              <label>Online time :</label>
              <input type="time" value="<?php echo $key->online_time; ?>" name="online_time" id="e_online_time" class="form-control ">
            </td>
          </tr>
        </table>
        <div id="alert_pasyment_acc"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/payment_account"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_payment_acc" class="btn btn-primary">Save</button>
      </div>
</form>
<?php } ?>
<script type="text/javascript">
  function ajax_action_edit_account(){
    var id = $("#id").val();
    var method = $("#e_method").val();
    var type = $("#e_type").val();
    var account_number = $("#e_account_number").val();
    var account_name = $("#e_account_name").val();
    var account_branch = $("#e_account_branch").val();
    var key = $("#e_key").val();
    var offline_time = $("#e_offline_time").val();
    var online_time = $("#e_online_time").val();
    var text_method = $("#e_method option:selected").text();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_edit_account/",
              type:'POST',
              dataType: "json",
              data: { method:method,type:type,account_number:account_number,account_name:account_name,account_branch:account_branch,key:key,offline_time:offline_time,online_time:online_time,id:id,text_method:text_method },
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_account extends MY_Controller  {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_payment_account");
      $this->load->model("payment_method/M_payment_method");
      $this->load->model("member/M_member");
      $this->load->library('session');
      $this->load->library('form_validation');
    }


    public function index()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            
            $data['content'] = 'payment_account';
            $data['pending_order'] = $this->M_payment_account->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment_account->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_payment_account->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['method'] = $this->M_payment_method->fetch_table("*","a__payment_method","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function tambah(){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_payment_account->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment_account->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_payment_account->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data_method'] = $this->M_payment_method->fetch_table("*","a__payment_method","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function edit_payment_account($id){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_payment_account->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment_account->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_payment_account->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_payment_account->fetch_table("*","a__payment_account","id_payment_account='$id'");
            $data['data_method'] = $this->M_payment_method->fetch_table("*","a__payment_method","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }   
    }

    public function ajax_list(){
         $column = 'a.*,b.name as method';
         $column_order = array(null, 'b.name','a.type','account_number','account_name','account_branch',null); //set column field database for datatable orderable
         $column_search = array('b.name','a.type','account_number','account_name','account_branch'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "a__payment_account a";
         $where = "";
         $joins = array(
                array(
                    'table' => 'a__payment_method b',
                    'condition' => 'a.id_payment_method = b.id',
                    'jointype' => ''
                )
            );

        $list = $this->M_payment_account->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->method;
            $row[] = $key->type;
            $row[] = $key->account_number;
            $row[] = $key->account_name;
            $row[] = $key->account_branch;
            $row[] = '<button onclick="ajax_get_data_account('.$key->id_payment_account.')" class="btn btn-success btn-sm">Edit</button>
          <button onclick="ajax_action_delete_account('.$key->id_payment_account.')" class="btn btn-danger btn-sm">Delete</button>';
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_payment_account->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_payment_account->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_account(){
        $text_method = explode("-", post('text_method'));
        if($text_method[0]=="BANK "){
            $this->form_validation->set_rules('type', 'type', 'required');
            $this->form_validation->set_rules('account_name', 'account name', 'required');
            $this->form_validation->set_rules('account_branch', 'account branch', 'required');

            if(post("type")=="AUTO"){
                $this->form_validation->set_rules('key', 'key', 'required');
                $this->form_validation->set_rules('offline_time', 'offline time', 'required');
                $this->form_validation->set_rules('online_time', 'online time', 'required');
            }    
        }
        $this->form_validation->set_rules('method', 'Payment Method', 'required');
        $this->form_validation->set_rules('account_number', 'account number', 'required');
        
        

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
            "id_payment_method" => post("method"),
            "type" => post("type"),
            "account_number" => post("account_number"),
            "account_name" => post("account_name"),
            "account_branch" => post("account_branch"),
            "key" => post("key"),
            "offline_time" => post("offline_time"),
            "online_time" => post("online_time"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
            );

            $add = $this->M_payment_account->insert_table("a__payment_account",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_account'
                );
                print json_encode($json_data);
            }
        }



    }

    public function ajax_action_delete_account(){
           $delete =$this->M_payment_account->delete_table("a__payment_account","id_payment_account",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengahapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses mengahapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_account'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_get_data_edit(){
        $id = post("id");
        $data = $this->M_payment_account->fetch_table("*","a__payment_account","id_payment_account='$id'");
        print json_encode($data);
    }

    public function ajax_action_edit_account(){
       $text_method = explode("-", post('text_method'));
        if($text_method[0]=="BANK "){
            $this->form_validation->set_rules('type', 'type', 'required');
            $this->form_validation->set_rules('account_name', 'account name', 'required');
            $this->form_validation->set_rules('account_branch', 'account branch', 'required');

            if(post("type")=="AUTO"){
                $this->form_validation->set_rules('key', 'key', 'required');
                $this->form_validation->set_rules('offline_time', 'offline time', 'required');
                $this->form_validation->set_rules('online_time', 'online time', 'required');
            }    
        }
        $this->form_validation->set_rules('method', 'Payment Method', 'required');
        $this->form_validation->set_rules('account_number', 'account number', 'required');
        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
             $data = array(
                "id_payment_method" => post("method"),
                "type" => post("type"),
                "account_number" => post("account_number"),
                "account_name" => post("account_name"),
                "account_branch" => post("account_branch"),
                "key" => post("key"),
                "offline_time" => post("offline_time"),
                "online_time" => post("online_time"),
                "update_at" => date("Y-m-d H:i:s")
            );

            $edit = $this->M_payment_account->update_table("a__payment_account",$data,"id_payment_account",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_account'
                );
                print json_encode($json_data);
            }
          
        }
        
    }

    
}

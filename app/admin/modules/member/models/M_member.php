<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_member extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }


  function add_member($data,$detail){
    $this->db->trans_begin();

    $this->db->insert('m__member', $data);
    $get_id = $this->db->insert_id();

    $array_id = array(
      "id_member" => $get_id
    );

    $new_detail = array_merge($array_id,$detail);
    $this->db->insert('m__member_detail', $new_detail);

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }


  }

  function delete_member($id) {
    $this->db->trans_begin();

    $this->db->where('id_member', $id);
    $this->db->delete("m__member");

    $this->db->where('id_member',$id);
    $this->db->delete('m__member_detail');

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }

  
  }

  function edit_member($data,$detail,$id){
    
    $this->db->trans_begin();

    $this->db->where('id_member',$id);
    $this->db->update('m__member', $data);

    $this->db->where('id_member',$id);
    $this->db->update('m__member_detail', $detail);

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }

  }

  function showedit($id){
    $joins = array(
        array(
            'table' => 'm__member_detail b',
            'condition' => 'a.id_member = b.id_member',
            'jointype' => ''
        ),
        array(
            'table' => 'sys__regencies c',
            'condition' => 'b.id_regency = c.id',
            'jointype' => 'left'
        ),
        array(
            'table' => 'sys__districts d',
            'condition' => 'b.id_district = d.id',
            'jointype' => 'left'
        ),
        array(
            'table' => 'sys__villages e',
            'condition' => 'b.id_village = e.id',
            'jointype' => 'left'
        )
    );

    return  $this->fetch_joins("m__member a","a.*,b.*,c.name as regency,d.name as district,e.name as village",$joins,"a.id_member = '$id'",FALSE);
  }

  
  
}
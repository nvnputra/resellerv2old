<style type="text/css">
  #list_member td{
    text-align: center;
  }
</style>
<a href="<?php echo base_url().$this->config->item('index_page'); ?>/member/add"><button class="btn btn-primary btn-sm" ><i class="fas fa-plus"></i> New Member</button></a>
<br><br>
<table class="table" id="list_member">
  <thead style="background: #000; color: #FFF;">
    <tr>
      <th><center>No</center></th>
      <!-- <th><center>Nama</center></th> -->
      <th><center>Username</center></th>
      <th><center>Email</center></th>
      <th><center>Is Active</center></th>
      <th><center>Is Premium</center></th>
      <th><center>Is Banned</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
  
  </tbody>
</table>


<script type="text/javascript">

  

ajax_list_member();
  

  function ajax_action_aktif(key,id){
    if (key.value=="1") {
      var aktif = "0";
    }else{
      var aktif = "1";
    }

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/member/ajax_action_aktif/",
              type:'POST',
              dataType: "json",
              data: {aktif:aktif, id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_premium(key,id){
    if (key.value=="1") {
      var premium = "0";
    }else{
      var premium = "1";
    }

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/member/ajax_action_premium/",
              type:'POST',
              dataType: "json",
              data: {premium:premium, id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_banned(key,id){
    if (key.value=="1") {
      var banned = "0";
    }else{
      var banned = "1";
    }

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/member/ajax_action_banned/",
              type:'POST',
              dataType: "json",
              data: {banned:banned, id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_delete_member(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/member/ajax_action_delete_member/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }


</script>
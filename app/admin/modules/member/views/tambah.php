<div class="card-header bg-primary">
  <h3 style="color: white;">New Member</h3>
</div>
<form method="POST" onsubmit="return ajax_action_add_member();">
      <table class="table">
          <tr>
            <td>
              <label>Email :</label>
              <input type="text" name="email" id="email" class="form-control">
            </td>
            <td>
              <label>Is Active :</label><br>
              <input checked="" type="checkbox" name="isactive" id="isactive" value="1" >
            </td>
          </tr>
          <tr>
            <td>
              <label>Username :</label>
              <input type="text" name="username" id="username" class="form-control">
            </td>
            <td>
              <label>Password :</label>
              <input type="password" name="password" id="password" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Is Premium :</label><br>
              <input type="checkbox" name="ispremium" id="ispremium" value="1" >
            </td>
             <td>
              <label>Trial Expired :</label>
              <input type="text" class="form-control datetime" id="trial_expired" name="trial_expired">
            </td>
          </tr>
        </table>
        <table class="table">
          <tr>
            <td>
              <label>First Name :</label>
              <input type="text" name="first" id="first" class="form-control">
            </td>
            <td>
              <label>Last Name :</label>
              <input type="text" name="last" id="last" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Birth regency :</label>
              <select class="form-control select2" style="width: 100%;" name="birth_regency" id="birth_regency">
                <option value="">Pilihan</option>
                <?php foreach ($regency as $regency) { ?>
                  <option value="<?php echo $regency->id; ?>"><?php echo $regency->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Birth date :</label>
              <input type="date" name="birth_date" id="birth_date" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Phone :</label>
              <input type="number" name="phone" id="phone" class="form-control">
            </td>
            <td>
              <label>Phone 2 :</label>
              <input type="number" name="phone2" id="phone2" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Address :</label>
              <textarea name="address" id="address" rows="2" class="form-control"></textarea>
            </td>
            <td>
              <label>Gender :</label><br>
              <input type="radio" name="jk" id="jk1" value="L"> Male
              <input type="radio" name="jk" id="jk2" value="P"> Female
            </td>
          </tr>
          <tr>
            <td>
              <label>Provinci :</label><br>
              <select onchange="get_regency(this)" class="form-control select2" style="width: 100%;" name="provinci" id="provinci">
                <option value="">Pilihan</option>
                <?php foreach ($prov as $prov) { ?>
                  <option value="<?php echo $prov->id; ?>"><?php echo $prov->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Regency :</label><br>
              <select onchange="get_district(this)" class="form-control select2" style="width: 100%;" name="regency" id="regency">
                
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>District :</label><br>
              <select onchange="get_villages(this)" class="form-control select2" style="width: 100%;" name="district" id="district">
                
              </select>
            </td>
            <td>
              <label>Village :</label><br>
              <select class="form-control select2" style="width: 100%;" name="village" id="village">
                
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Job :</label><br>
              <select class="form-control select2" style="width: 100%;" name="job" id="job">
                <option value="">Pilihan</option>
                <?php foreach ($jobs as $jobs) { ?>
                  <option value="<?php echo $jobs->id; ?>"><?php echo $jobs->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Education :</label><br>
              <select class="form-control select2" style="width: 100%;" name="education" id="education">
                <option value="">Pilihan</option>
                <?php foreach ($educations as $educations) { ?>
                  <option value="<?php echo $educations->id; ?>"><?php echo $educations->name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Fb :</label>
              <input type="text" name="fb" id="fb" class="form-control">
            </td>
            <td>
              <label>Ig :</label><br>
              <input type="text" name="ig" id="ig" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Line :</label><br>
              <input type="text" name="line" id="line" class="form-control">
            </td>
          </tr>
        </table>
        <div id="alert_new_member"></div>
        <button type="submit" id="btn_member" style="float: right; margin-left: 1%;" class="btn btn-primary">Save</button> 
        <a style="float: right;"  href="<?=base_url().$this->config->item('index_page'); ?>/member"><button type="button" class="btn btn-danger">Back</button></a>
</form>
        <br><br><br><br><br><br>

        <script type="text/javascript">
          function get_regency(key) {
            $.ajax({
              url: "<?php echo base_url(); ?>admin.php/s_regencies/ajax_get_regency/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                $('#page-load').show();
              },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select Regency -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#regency').html(html);
                $('#district').html("");
                $('#village').html("");
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
          }
          function get_district(key) {
            $.ajax({
              url: "<?php echo base_url(); ?>admin.php/s_district/ajax_get_district/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                $('#page-load').show();
                },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select District -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#district').html(html);
                $('#village').html("");
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
          }
          function get_villages(key) {
            $.ajax({
              url: "<?php echo base_url(); ?>admin.php/s_villages/ajax_get_villages/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                $('#page-load').show();
              },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select Village -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#village').html(html);
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
          }

          function ajax_action_add_member() {
          
          if ($('#isactive').is(':checked')) {
            var isactive = 1;
          }else{
            var isactive = 0;
          }

          if ($('#ispremium').is(':checked')) {
            var ispremium = 1;
          }else{
            var ispremium = 0;
          }

          if (document.getElementById('jk1').checked) {
            var jk = document.getElementById('jk1').value;
          }else if (document.getElementById('jk2').checked){
            var jk = document.getElementById('jk2').value;
          }else{
            var jk = "";
          }

          var username = $("#username").val();
          var password = $("#password").val();
          var email = $("#email").val();
          var first = $("#first").val();
          var last = $("#last").val();
          var birth_regency = $("#birth_regency").val();
          var birth_date = $("#birth_date").val();
          var phone = $("#phone").val();
          var phone2 = $("#phone2").val();
          var address = $("#address").val();
          var provinci = $("#provinci").val();
          var regency = $("#regency").val();
          var district = $("#district").val();
          var village = $("#village").val();
          var job = $("#job").val();
          var education = $("#education").val();
          var fb = $("#fb").val();
          var ig = $("#ig").val();
          var line = $("#line").val();
          var trial_expired = $("#trial_expired").val();

          $.ajax({
                    url: "<?php echo base_url(); ?>admin.php/member/ajax_action_add_member/",
                    type:'POST',
                    dataType: "json",
                    data: {username:username,password:password, email:email,first:first,last:last, birth_regency:birth_regency, birth_date:birth_date,phone:phone, phone2:phone2, address:address,provinci:provinci,regency:regency, district:district, village:village,job:job, education:education, fb:fb,ig:ig,line:line, isactive:isactive, ispremium:ispremium,jk:jk,trial_expired:trial_expired},
                   beforeSend: function () {
                      $('#page-load').show();
                    },
                    success: function(data) {
                          $('#page-load').hide();
                          if(data.result){
                            toastr["success"](data.message.body);
                            setTimeout(function(){window.location = data.redirect},500);
                          }else{
                            toastr["error"](data.message.body);
                          }
                        
                    },error: function(request, status, error){
                        $('#page-load').hide();
                        toastr["error"]("Error, Please try again later");
                    }
                });

          return false;
          }

        </script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_member");
      $this->load->model("s_regencies/M_s_regencies","M_s_regencies");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'member';
            $data['pending_order'] = $this->M_member->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_member->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_member->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_list(){
         $column = '*';
         $column_order = array(null, 'username','email',null,null,null,null); //set column field database for datatable orderable
         $column_search = array('username','email'); //set column field database for datatable searchable 
         $order = array('id_member' => 'DESC'); // default order 
         $table = "m__member";
         $where = "";
         $joins = "";

        $list = $this->M_member->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            if($key->is_active=="1"){ $is_active =  "checked"; }else{ $is_active = ""; } 
            if($key->is_premium=="1"){ $is_premium =  "checked"; }else{ $is_premium = ""; } 
            if($key->is_banned=="1"){ $is_banned =  "checked"; }else{ $is_banned = ""; } 
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->username;
            $row[] = $key->email;
            $row[] = "<input type='checkbox' onclick='ajax_action_aktif(this,".$key->id_member.")' $is_active name='aktif' value='$key->is_active' >";
            $row[] = "<input type='checkbox' onclick='ajax_action_premium(this,".$key->id_member.")' $is_premium name='premium' value='$key->is_premium' >";
            $row[] = "<input type='checkbox' onclick='ajax_action_banned(this, ".$key->id_member.")' $is_banned name='banned' value='$key->is_banned' >";
            $row[] = '<a href="'.base_url().$this->config->item("index_page").'/member/get_data_edit/'.$key->id_member.'"><button class="btn btn-success btn-sm"><i class="zmdi zmdi-edit"></i></button></a>
          <button onclick="ajax_action_delete_member('.$key->id_member.')" class="btn btn-danger btn-sm"><i class="zmdi zmdi-delete"></i></button> ';
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_member->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_member->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function add()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_member->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_member->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_member->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['jobs'] = $this->M_member->fetch_table("*","sys__jobs","");
            $data['educations'] = $this->M_member->fetch_table("*","sys__educations","");
            $data['prov'] = $this->M_member->fetch_table("*","sys__provinces","");
            $data['regency'] = $this->M_s_regencies->fetch_table("*","sys__regencies","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    
    public function ajax_action_aktif(){

        if (post("aktif")=="1") {
            $aktif_at = date("Y-m-d H:i:s");
        }else{
            $aktif_at = null;
        }

        $data =  array(
            "is_active" => post("aktif"),
            "active_at" => $aktif_at
        );

        $edit = $this->M_member->update_table('m__member',$data,'id_member',post("id"));
            
            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'error', 'body'=> 'Sukses '),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/member'
                );
                print json_encode($json_data);
            }
            
    }

    public function ajax_action_premium(){

        if (post("premium")=="1") {
            $premium_at = date("Y-m-d H:i:s");
        }else{
            $premium_at = null;
        }

        $data =  array(
            "is_premium" => post("premium"),
            "premium_at" => $premium_at
        );

        $edit = $this->M_member->update_table('m__member',$data,'id_member',post("id"));
            
            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'error', 'body'=> 'Sukses '),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/member'
                );
                print json_encode($json_data);
            }
            
    }

    public function ajax_action_banned(){

        if (post("banned")=="1") {
            $banned_at = date("Y-m-d H:i:s");
        }else{
            $banned_at = null;
        }

        $data =  array(
            "is_banned" => post("banned"),
            "banned_at" => $banned_at
        );

        $edit =  $this->M_member->update_table('m__member',$data,'id_member',post("id"));
            
            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'error', 'body'=> 'Sukses '),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/member'
                );
                print json_encode($json_data);
            }
            
    }


    public function ajax_action_add_member(){
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        $this->form_validation->set_rules('first', 'First Name', 'required');
        $this->form_validation->set_rules('last', 'Last Name', 'required');
        /*$this->form_validation->set_rules('birth_regency', 'birth_regency', 'required');*/
        $this->form_validation->set_rules('birth_date', 'Birth date', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');
        $this->form_validation->set_rules('jk', 'gender', 'required');
        /*$this->form_validation->set_rules('provinci', 'provinci', 'required');
        $this->form_validation->set_rules('regency', 'regency', 'required');
        $this->form_validation->set_rules('district', 'district', 'required');
        $this->form_validation->set_rules('village', 'village', 'required');
        $this->form_validation->set_rules('job', 'job', 'required');
        $this->form_validation->set_rules('education', 'education', 'required');*/
        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{     

        $cek_username = count($this->M_member->fetch_table("id_member","m__member","username = '".post('username')."' "));   
        $cek_email = count($this->M_member->fetch_table("id_member","m__member","email = '".post('email')."' "));
       
        if ($cek_email>0) {
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'email sudah ada'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        if ($cek_username>0) {
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'username sudah ada'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

       




        if (post("isactive")=="1") {
            $active_at = date("Y-m-d H:i:s");
        }else{
            $active_at = null;
        }

        if (post("ispremium")=="1") {
            $premium_at = date("Y-m-d H:i:s");
        }else{
            $premium_at = null;
        }

        $data = array(
            "email" => post("email"),
            "username" => post("username"),
            "password" => md5(post("password")),
            "is_active" => post("isactive"),
            "active_at" => $active_at,
            "is_premium" => post("ispremium"),
            "premium_at" => $premium_at,
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s"),
            "trial_expired" => post("trial_expired"),
            "token" => md5(date("Y-m-d H:i:s")) 
        );

        $detail = array(
            "first_name" => post("first"),
            "last_name" => post("last"),
            "birth_regency" => post("birth_regency"),
            "birth_date" => post("birth_date"),
            "phone" => post("phone"),
            "phone2" => post("phone2"),
            "address" => post("address"),
            "gender" => post("jk"),
            "id_provinces" => post("provinci"),
            "id_regency" => post("regency"),
            "id_district" => post("district"),
            "id_village" => post("village"),
            "id_job" => post("job"),
            "id_education" => post("education"),
            "social_fb" => post("fb"),
            "social_ig" => post("ig"),
            "social_line" => post("line"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
        );
        $add_data =$this->M_member->add_member($data,$detail);
    
        if ($add_data ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Gagal Menambah data'),
                "form_error" => '',
                "redirect" => ''
            );
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'error', 'body'=> 'Sukses Menambah data'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/member'
            );
        }
            
            print json_encode($json_data);

        }


    }

    public function ajax_action_delete_member(){
           $delete_data = $this->M_member->delete_member(post("id"));
           if ($delete_data ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Gagal Menghapus data'),
                "form_error" => '',
                "redirect" => ''
            );
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'error', 'body'=> 'Sukses Menghapus data'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/member'
            );
        }
        echo json_encode($json_data);
    }

    public function get_data_edit($id){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_member->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_member->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_member->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['jobs'] = $this->M_member->fetch_table("*","sys__jobs","");
            $data['educations'] = $this->M_member->fetch_table("*","sys__educations","");
            $data['prov'] = $this->M_member->fetch_table("*","sys__provinces","");
            $data['regency'] = $this->M_s_regencies->fetch_table("*","sys__regencies","");
            $data['data'] = $this->M_member->showedit($id);
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function ajax_action_edit_member(){
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        $this->form_validation->set_rules('first', 'First Name', 'required');
        $this->form_validation->set_rules('last', 'Last Name', 'required');
        // $this->form_validation->set_rules('birth_regency', 'birth_regency', 'required');
        $this->form_validation->set_rules('birth_date', 'Birth date', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');
        $this->form_validation->set_rules('jk', 'gender', 'required');
        // $this->form_validation->set_rules('trial_expired', 'trial_expired', 'required');
        /*$this->form_validation->set_rules('provinci', 'provinci', 'required');
        $this->form_validation->set_rules('regency', 'regency', 'required');
        $this->form_validation->set_rules('district', 'district', 'required');
        $this->form_validation->set_rules('village', 'village', 'required');
        $this->form_validation->set_rules('job', 'job', 'required');
        $this->form_validation->set_rules('education', 'education', 'required');*/
        

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{        

            $cek_username = count($this->M_member->fetch_table("id_member","m__member"," id_member != '".post('id')."' and username = '".post('username')."' "));  
            $cek_email = count($this->M_member->fetch_table("id_member","m__member","id_member != '".post('id')."' and email = '".post('email')."' "));
           
            if ($cek_email>0) {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'email sudah ada'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

            if ($cek_username>0) {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'username sudah ada'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

            
        if (post("isactive")=="1") {
            $active_at = date("Y-m-d H:i:s");
        }else{
            $active_at = null;
        }

        if (post("ispremium")=="1") {
            $premium_at = date("Y-m-d H:i:s");
        }else{
            $premium_at = null;
        }


        $data = array(
            "email" => post("email"),
            "username" => post("username"),
            "password" => md5(post("password")),
            "is_active" => post("isactive"),
            "active_at" => $active_at,
            "trial_expired" => post("trial_expired"),
            "premium_at" => $premium_at,
            "update_at" => date("Y-m-d H:i:s")
        );

        $detail = array(
            "first_name" => post("first"),
            "last_name" => post("last"),
            "birth_regency" => post("birth_regency"),
            "birth_date" => post("birth_date"),
            "phone" => post("phone"),
            "phone2" => post("phone2"),
            "address" => post("address"),
            "gender" => post("jk"),
            "id_provinces" => post("provinci"),
            "id_regency" => post("regency"),
            "id_district" => post("district"),
            "id_village" => post("village"),
            "id_job" => post("job"),
            "id_education" => post("education"),
            "social_fb" => post("fb"),
            "social_ig" => post("ig"),
            "social_line" => post("line"),
            "update_at" => date("Y-m-d H:i:s")
        );
        $edit_data =$this->M_member->edit_member($data,$detail,post("id"));
    
        if ($edit_data ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Gagal Mengedit data'),
                "form_error" => '',
                "redirect" => ''
            );
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'error', 'body'=> 'Sukses Mengedit data'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/member'
            );
        }
            
            print json_encode($json_data);

        }


    }

    
}

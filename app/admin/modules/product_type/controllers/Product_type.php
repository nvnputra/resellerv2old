<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_type extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_product_type");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'product_type';
            $data['pending_order'] = $this->M_product_type->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product_type->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product_type->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function tambah()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_product_type->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product_type->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product_type->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function edit_product_type($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_product_type->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product_type->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product_type->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_product_type->fetch_table("*","a__product_type","id='$id'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_list(){
         $column = '*';
         $column_order = array(null, 'name','order_number'); //set column field database for datatable orderable
         $column_search = array('name','order_number'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "a__product_type";
         $where = "";
         $joins = "";

        $list = $this->M_product_type->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            if($key->is_premium=="1"){ $checked =  "checked"; }else{ $checked = ""; }
            if($key->is_validate_ig=="1"){ $checked2 =  "checked"; }else{ $checked2 = ""; } 
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->name;
            $row[] = $key->order_number;
            $row[] = "<input type='checkbox' disabled='' $checked name=''>";
            $row[] = "<input type='checkbox' disabled='' $checked2 name=''>";
            $row[] = "<button onclick='ajax_get_edit_product_type(".$key->id.")' class='btn btn-success btn-sm'>Edit</button>
                     <button onclick='ajax_action_delete_product_type(".$key->id.")' class='btn btn-danger btn-sm'>Delete</button>";
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_product_type->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_product_type->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_product_type(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('order_number', 'Order Number', 'required');

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
                "name" => post("nama"),
                "order_number" => post("order_number"),
                "is_premium" => post("is_premium"),
                "is_validate_ig" => post("is_validate_ig"),
                "validate_ig" => post("validate_ig"),
                "created_at" => date("Y-m-d H:i:s"),
                "update_at" => date("Y-m-d H:i:s")
            );
            $add = $this->M_product_type->insert_table("a__product_type",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product_type'
                );
                print json_encode($json_data);
            }
        }

    }

    public function ajax_action_delete_product_type(){
           $delete = $this->M_product_type->delete_table("a__product_type","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product_type'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_get_data_edit(){
        $id =  post("id");
        $data = $this->M_product_type->fetch_table("*","a__product_type","id='$id'");
        print json_encode($data);
    }

    public function ajax_action_edit_product_type(){
        
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('order_number', 'Order Number', 'required');

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{        
            $data = array(
                "name" => post("nama"),
                "order_number" => post("order_number"),
                "is_premium" => post("is_premium"),
                "is_validate_ig" => post("is_validate_ig"),
                "validate_ig" => post("validate_ig"),
                "update_at" => date("Y-m-d H:i:s")
            );

            $edit = $this->M_product_type->update_table("a__product_type",$data,"id",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product_type'
                );
                print json_encode($json_data);
            }

        }

    }

    
}

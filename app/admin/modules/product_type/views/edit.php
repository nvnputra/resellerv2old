 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Product Type</h4>
 </div>
 <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}

 foreach ($data as $key ) { ?>
<form method="post" onsubmit="return ajax_action_edit_product_type();">
        <input type="hidden" value="<?php echo $key->id; ?>" name="id" id="id" >
        <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" name="nama" id="e_nama" value="<?php echo $key->name; ?>" class="form-control">
            </td>
            <td>
              <label>Order Number :</label>
              <input type="number" name="order_number" value="<?php echo $key->order_number; ?>" id="e_order_number" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
            <table>
              <tr>
                 <td>
                  <label>Is Premium :</label><br>
                  <input type="checkbox" <?php if ($key->is_premium=="1") { echo "checked"; } ?> name="is_premium" value="1" id="e_is_premium">
                </td>
                  <td>
                    <label>Is validate ig :</label><br>
                    <input type="checkbox" <?php if ($key->is_validate_ig=="1") { echo "checked"; } ?> name="is_validate_ig" value="1" id="e_is_validate_ig">
                  </td>
              </tr>
            </table>
            </td>
            <td>
                <label>Validate Ig :</label>
                <input type="text" value="<?php echo $key->validate_ig; ?>" class="form-control" id="e_validate_ig" name="">
            </td>
          </tr>
        </table>
      <div id="alert_product_type"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/product_type/"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_product_type" class="btn btn-success">Save</button>
      </div>
</form>
<?php } ?>
<script type="text/javascript">
  function ajax_action_edit_product_type(){
    var id = $("#id").val();
    var nama = $("#e_nama").val();
    var order_number = $("#e_order_number").val();
    var validate_ig = $("#e_validate_ig").val();
    if ($('#e_is_premium').is(':checked')) {
      var is_premium = 1;
    }else{
      var is_premium = 0;
    }
    if ($('#e_is_validate_ig').is(':checked')) {
      var is_validate_ig = 1;
    }else{
      var is_validate_ig = 0;
    }

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_type/ajax_action_edit_product_type/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, order_number:order_number,is_premium:is_premium,id:id,is_validate_ig:is_validate_ig,validate_ig:validate_ig},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
</script>
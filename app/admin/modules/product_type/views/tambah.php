<div class="card-header bg-primary">
  <h4 style="color: white;">New Product Type</h4>
</div>
<form method="post" onsubmit="return ajax_action_add_product_type();">
        <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </td>
            <td>
              <label>Order Number :</label>
              <input type="number" name="order_number" id="order_number" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
            <table>
              <tr>
                <td>
                  <label>Is Premium :</label><br>
                  <input type="checkbox" name="is_premium" value="1" id="is_premium">
                </td>
                  <td>
                    <label>Is validate ig :</label><br>
                    <input type="checkbox" name="is_validate_ig" value="1" id="is_validate_ig">
                  </td>
              </tr>
            </table>
            </td>
            <td>
                <label>Validate Ig :</label>
                <input type="text" class="form-control" id="validate_ig" name="">
            </td>
          </tr>
        </table>
        <div id="alert_product_type"></div>
      <div class="modal-footer">
        <a href="<?=base_url().$this->config->item('index_page'); ?>/product_type"><button type="button" class="btn btn-danger" >Back</button></a>
        <button type="submit" id="btn_product_type" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function ajax_action_add_product_type() {

    var nama = $("#nama").val();
    var order_number = $("#order_number").val();
    var validate_ig = $("#validate_ig").val();
    
    if ($('#is_premium').is(':checked')) {
      var is_premium = 1;
    }else{
      var is_premium = 0;
    }
    if ($('#is_validate_ig').is(':checked')) {
      var is_validate_ig = 1;
    }else{
      var is_validate_ig = 0;
    }
   

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_type/ajax_action_add_product_type/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, order_number:order_number,is_premium:is_premium,is_validate_ig:is_validate_ig,validate_ig:validate_ig},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
</script>
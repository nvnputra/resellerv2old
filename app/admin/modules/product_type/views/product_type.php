<style type="text/css">
  #list_product_type td{
    text-align: center;
  }
</style>


<a href="<?=base_url().$this->config->item('index_page'); ?>/product_type/tambah"><button class="btn btn-primary btn-sm">New Product Type</button></a>
<br><br>
<table class="table" id="list_product_type">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>Nama</center></th>
      <th><center>Order Number</center></th>
      <th><center>Is Premium</center></th>
      <th><center>Is Validate ig</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>

  </tbody>
</table>


<script type="text/javascript">
  
  
  function ajax_action_delete_product_type(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_type/ajax_action_delete_product_type/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_product_type(id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/product_type/edit_product_type/"+id;
  }

  

</script>
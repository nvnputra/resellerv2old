<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sys_text extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_text");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'text';

            $data['pending_order'] = $this->M_text->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_text->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_text->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function new_text()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_text->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_text->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_text->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data_text'] = $this->M_text->fetch_table("*","sys__text","parent_id = '0' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function edit_text($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_text->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_text->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_text->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_text->fetch_table("*","sys__text","id='$id'");
            $data['data_text'] = $this->M_text->fetch_table("*","sys__text","parent_id = '0' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }



    public function ajax_list(){
         $column = '*';
         $column_order = array(null, 'param','description','parent_id',null); //set column field database for datatable orderable
         $column_search = array('param','description','parent_id',); //set column field database for datatable searchable 
         $order = array('id' => 'DESC'); // default order 
         $table = "sys__text";
         $where = "";
         $joins = "";

        $list = $this->M_text->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->param;
            $row[] = $key->description;
            $row[] = $key->parent_id;
            $row[] = "<button data-toggle='tooltip' data-placement='top' title='Edit' onclick='ajax_get_edit_text(".$key->id.")' class='btn btn-success btn-sm'><i class='zmdi zmdi-edit'></i></button>
                     <button data-toggle='tooltip' data-placement='top' title='Delete' onclick='ajax_action_delete_text(".$key->id.")' class='btn btn-danger btn-sm'><i class='zmdi zmdi-delete'></i></button>";
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_text->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_text->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_text(){
        $this->form_validation->set_rules('param', 'param', 'required');
        $this->form_validation->set_rules('desc', 'desc', 'required');
        $this->form_validation->set_rules('value', 'value', 'required');
       

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
            "param" => post("param"),
            "description" => post("desc"),
            "value" => post("value"),
            "parent_id" => post("parent")
        );
            $add = $this->M_text->insert_table("sys__text",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengisi data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/sys_text/'
                );
                print json_encode($json_data);
            }
            
        }
        
    }

    public function ajax_action_delete_text(){
           $delete = $this->M_text->delete_table("sys__text","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/sys_text/'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_action_edit_text(){
        $this->form_validation->set_rules('param', 'param', 'required');
        $this->form_validation->set_rules('desc', 'desc', 'required');
        $this->form_validation->set_rules('value', 'value', 'required');
        


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
                "param" => post("param"),
            "description" => post("desc"),
            "value" => post("value"),
            "parent_id" => post("parent")
            );

            $edit = $this->M_text->update_table("sys__text",$data,"id",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/sys_text/'
                );
                print json_encode($json_data);
            }
        }

    }

    
}

 <div class="card-header bg-primary">
      <h4 style="color: white;" class="modal-title" id="title-tambah">New Text</h4>
 </div>
<form method="post" onsubmit="return ajax_action_add_text();">
      <table class="table">
          <tr>
            <td>
              <label>Param :</label>
              <input type="text" name="param" id="param" class="form-control">
            </td>
            <td>
              <label>Parent :</label>
              <select name="parent" class="form-control" id="parent">
                <option value=""></option>
                <?php foreach ($data_text as $key) { ?>
                  <option value="<?php echo $key->id; ?>"><?php echo $key->param; ?></option>
                <?php }?>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <label>Description :</label><br>
              <textarea class="form-control" rows="2" id="desc"></textarea>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <label>Value :</label><br>
              <textarea class="form-control" rows="2" id="value"></textarea>
            </td>
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/sys_text"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_admin" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function ajax_action_add_text() {

    var param = $("#param").val();
    var parent = $("#parent").val();
    var desc = $("#desc").val();
    var value = $("#value").val();
   
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/sys_text/ajax_action_add_text/",
              type:'POST',
              dataType: "json",
              data: {param:param, parent:parent,desc:desc,value:value},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
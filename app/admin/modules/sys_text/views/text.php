<style type="text/css">
  #list_admin td{
    text-align: center;
  }
</style>

<a href="<?php echo base_url().$this->config->item('index_page'); ?>/sys_text/new_text/"><button class="btn btn-primary btn-sm" ><i class="fas fa-plus"></i> New Text</button></a>
<br><br>
<table class="table table-borderless table-striped " id="list_text">
  <thead style="background: #000; color: #FFF;">
    <tr>
      <th><center>No</center></th>
      <th><center>Param</center></th>
      <th><center>Description</center></th>
      <th><center>Parent Id</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
  </tbody> 
</table>



<script type="text/javascript">

  
  function ajax_action_delete_text(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/sys_text/ajax_action_delete_text/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_text(id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/sys_text/edit_text/"+id;
  }



</script>
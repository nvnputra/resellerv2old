 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Text</h4>
 </div>
 <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}

 foreach ($data as $key ) { ?>
<form method="post" onsubmit="return ajax_action_edit_text();">
        <input type="hidden" name="id" value="<?php echo $key->id; ?>" id="id">
        <table class="table">
          <tr>
            <td>
              <label>Param :</label>
              <input type="text" name="param" id="param" value="<?php echo $key->param; ?>" class="form-control">
            </td>
            <td>
              <label>Parent :</label>
              <select name="parent" class="form-control" id="parent">
                <option value=""></option>
                <?php foreach ($data_text as $dt) { ?>
                  <option <?php if($dt->id==$key->parent_id){ echo "selected"; } ?> value="<?php echo $dt->id; ?>"><?php echo $dt->param; ?></option>
                <?php }?>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <label>Description :</label><br>
              <textarea class="form-control" rows="2" id="desc"><?php echo $key->description; ?></textarea>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <label>Value :</label><br>
              <textarea class="form-control" rows="2" id="value"><?php echo $key->value; ?></textarea>
            </td>
          </tr>
        </table>
        <div id="alert_admin"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/sys_text"><button type="button" class="btn btn-danger" data-dismiss="modal">Back</button></a>
        <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
      </div>
</form>
<?php } ?>
<script type="text/javascript">
    function ajax_action_edit_text(){
    var id = $("#id").val();
    var param = $("#param").val();
    var parent = $("#parent").val();
    var desc = $("#desc").val();
    var value = $("#value").val();
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/sys_text/ajax_action_edit_text/",
              type:'POST',
              dataType: "json",
              data: {param:param, parent:parent,desc:desc,value:value,id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
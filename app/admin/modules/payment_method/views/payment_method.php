<style type="text/css">
  #list_payment_method td{
    text-align: center;
  }
</style>


<a href="<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/tambah/"><button class="btn btn-primary btn-sm" >New Method Payment</button></a>
<br><br>
<table id="list_payment_method" class="table">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>Nama</center></th>
      <th><center>Method</center></th>
      <th><center>Type</center></th>
      <th><center>Rate</center></th>
      <th><center>Code</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>

  </tbody>
</table>


<script type="text/javascript">
  

  
  function ajax_action_delete_method_p(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_action_delete_method/",
              type:'POST',
              dataType: "json",
              data: {id:id},
             beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_method_p(id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/edit_payment_method/"+id;
   
  }


  function ajax_show_upload(id,apifile){
    if(apifile==""){
      $('#hapus_api').hide();
    }else{
      $('#hapus_api').show();
    }
    $('#id_upload').val(id);
    $('#name_file').html(apifile);
    $('#cek_api').val(apifile);
    $('#userfile').val("");
    $('#modal_upload').modal('show');
  }


  function ajax_action_upload(){
    var file_data = $('#userfile').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('id', $('#id_upload').val());
    form_data.append('apifile', $('#cek_api').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo
$this->security->get_csrf_hash(); ?>');

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_action_upload/",
              dataType: 'json',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_delete_upload(){
  if (confirm('Apakah Anda Yakin Menghapus Api File?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_action_delete_upload/",
              type:'POST',
              dataType: "json",
              data: {id:$('#id_upload').val(), file:$('#cek_api').val()},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }
</script>
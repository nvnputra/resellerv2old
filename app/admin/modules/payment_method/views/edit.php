 <div class="card-header bg-success">
      <h4 style="color: white;" class="modal-title" id="title-tambah">Edit Payment Method</h4>
 </div>
 <?php  
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}

 foreach ($data as $key ) { ?>
<form method="post" onsubmit="return ajax_action_edit_method_p();">
        <input type="hidden" name="id" value="<?php echo $key->id; ?>" id="id">
        <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" name="nama" value="<?php echo $key->name; ?>" id="e_nama" class="form-control">
            </td>
            <td>
              <label>Method :</label>
              <select name="method" class="form-control" id="e_method">
                <option value=""></option>
                <option <?php if($key->method=="BANK"){ echo "selected"; } ?> value="BANK">Bank</option>
                <option <?php if($key->method=="PULSA"){ echo "selected"; } ?> value="PULSA">Pulsa</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Type :</label>
              <select name="type" class="form-control" id="e_type">
                <option value=""></option>
                <option <?php if($key->type=="AUTO"){ echo "selected"; } ?>  value="AUTO">Auto</option>
                <option <?php if($key->type=="MANUAL"){ echo "selected"; } ?>  value="MANUAL">Manual</option>
              </select>
            </td>
            <td>
              <label>Rate :</label>
              <input type="text" value="<?php echo $key->rate; ?>" name="rate" id="e_rate" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Code :</label>
              <input type="text" value="<?php echo $key->code; ?>" name="code" id="e_code" class="form-control">
            </td>
          </tr>
        </table>
        <div id="alert_method"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/payment_method"><button type="button" class="btn btn-danger" >Back</button></a>
        <button type="submit" id="btn_method" class="btn btn-primary">Save</button>
      </div>
</form>
<?php } ?>

<script type="text/javascript">
  function ajax_action_edit_method_p(){
    var id = $("#id").val();
    var nama = $("#e_nama").val();
    var method = $("#e_method").val();
    var type = $("#e_type").val();
    var code = $("#e_code").val();
    var rate = $("#e_rate").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_action_edit_method/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, method:method,type:type, code:code, rate:rate,id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
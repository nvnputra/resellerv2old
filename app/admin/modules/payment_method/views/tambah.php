 <div class="card-header bg-primary">
      <h4 style="color: white;" class="modal-title" id="title-tambah">New Payment Method</h4>
 </div>
<form method="post" onsubmit="return ajax_action_add_method_p();">
        <table class="table">
          <tr>
            <td>
              <label>Nama :</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </td>
            <td>
              <label>Method :</label>
              <select name="method" class="form-control" id="method">
                <option value=""></option>
                <option value="BANK">Bank</option>
                <option value="PULSA">Pulsa</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Type :</label>
              <select name="type" class="form-control" id="type">
                <option value=""></option>
                <option value="AUTO">Auto</option>
                <option value="MANUAL">Manual</option>
              </select>
            </td>
            <td>
              <label>Rate :</label>
              <input type="text" name="rate" id="rate" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Code :</label>
              <input type="text" name="code" id="code" class="form-control">
            </td>
          </tr>
        </table>
      <div id="alert_method"></div>
      <div class="modal-footer">
        <a  href="<?=base_url().$this->config->item('index_page'); ?>/payment_method"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit"  class="btn btn-primary" id="btn_method">Save</button>
      </div>
</form>
<script type="text/javascript">
  function ajax_action_add_method_p() {

    var nama = $("#nama").val();
    var method = $("#method").val();
    var type = $("#type").val();
    var code = $("#code").val();
    var rate = $("#rate").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_action_add_method/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, method:method,type:type, code:code, rate:rate},
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_method extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_payment_method");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'payment_method';
            $data['pending_order'] = $this->M_payment_method->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_payment_method->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment_method->fetch_table("*","a__post_category","id_parent = '0'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function tambah()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_payment_method->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_payment_method->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment_method->fetch_table("*","a__post_category","id_parent = '0'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function edit_payment_method($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_payment_method->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['pending_payment'] = $this->M_payment_method->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['menu_post'] = $this->M_payment_method->fetch_table("*","a__post_category","id_parent = '0'");
            $data['data'] = $this->M_payment_method->fetch_table("*","a__payment_method","id='$id'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_list(){
         $column = '*';
         $column_order = array(null, 'name','method','type','rate','code'); //set column field database for datatable orderable
         $column_search = array('name','method','type','rate','code'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "a__payment_method";
         $where = "";
         $joins = "";

        $list = $this->M_payment_method->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $api_file = "'$key->api_file'";
            $row = array();
            $row[] = $no;
            $row[] = $key->name;
            $row[] = $key->method;
            $row[] = $key->type;
            $row[] = $key->rate;
            $row[] = $key->code;
            $row[] = '<button onclick="ajax_get_edit_method_p('.$key->id.')" class="btn btn-success btn-sm">Edit</button>
          <button onclick="ajax_action_delete_method_p('.$key->id.')" class="btn btn-danger btn-sm">Delete</button>
          <button class="btn btn-warning btn-sm" onclick="ajax_show_upload('.$key->id.','.$api_file.')">Upload Api</button>';
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_payment_method->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_payment_method->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_method(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('method', 'method', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('rate', 'rate', 'required');
        $this->form_validation->set_rules('code', 'code', 'required');

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
            "name" => post("nama"),
            "method" => post("method"),
            "rate" => post("rate"),
            "type" => post("type"),
            "code" => post("code"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
            );

            $add = $this->M_payment_method->insert_table("a__payment_method",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_method'
                );
                print json_encode($json_data);
            }
        }



    }

    public function ajax_action_delete_method(){
           $delete =$this->M_payment_method->delete_table("a__payment_method","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengahapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses mengahapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_method'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_get_data_edit(){
        $id = post("id");
        $data = $this->M_payment_method->fetch_table("*","a__payment_method","id='$id'");
        print json_encode($data);
    }

    public function ajax_action_edit_method(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('method', 'method', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('rate', 'rate', 'required');
        $this->form_validation->set_rules('code', 'code', 'required');


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
            "name" => post("nama"),
            "method" => post("method"),
            "rate" => post("rate"),
            "type" => post("type"),
            "code" => post("code"),
            "update_at" => date("Y-m-d H:i:s")
        );

            $edit = $this->M_payment_method->update_table("a__payment_method",$data,"id",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_method'
                );
                print json_encode($json_data);
            }
          
        }
        
    }

    public function ajax_action_upload(){
        
        $config['upload_path'] = './uploads/payment_method/';
        $config['allowed_types'] = 'gif|jpg|png|doc|txt';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
  
        $this->load->library('upload', $config);

        
  
        if (!$this->upload->do_upload('file'))
        {
            $result = FALSE;
            $message = array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', ''));
            $redirect = "";
        }
        else
        {
            $data = $this->upload->data();
            if(post('apifile')!=""){
                unlink('./uploads/payment_method/' . post('apifile'));  
            }
            $upload_api = $this->M_payment_method->upload_api($data['file_name'], post("id"));
            if($upload_api==TRUE)
            {
                $result = TRUE;
                $message =  array('head'=> 'Success', 'body'=> 'Sukses Mengupload');
                $redirect = ''.base_url().$this->config->item('index_page').'/payment_method';
            }
            else
            {
                $result = FALSE;
                $message =  array('head'=> 'Gagal', 'body'=> 'Gagal Mengupload');
                $redirect = '';
            }
        }
    
            $json_data =  array(
                    "result" => $result ,
                    "message" => $message,
                    "redirect" => $redirect
            );
        print json_encode($json_data);
    }

    public function ajax_action_delete_upload(){
            $upload_api = $this->M_payment_method->upload_api("", post("id"));
            if($upload_api==TRUE)
            {
                unlink('./uploads/payment_method/' . post('file')); 
                $result = TRUE;
                $message =  array('head'=> 'Success', 'body'=> 'Sukses Menghapus Api');
                $redirect = ''.base_url().$this->config->item('index_page').'/payment_method';
            }
            else
            {
                $result = FALSE;
                $message =  array('head'=> 'Gagal', 'body'=> 'Gagal Menghapus Api');
                $redirect = '';
            }

            $json_data =  array(
                    "result" => $result ,
                    "message" => $message,
                    "redirect" => $redirect
            );
        print json_encode($json_data);
    }

    
}

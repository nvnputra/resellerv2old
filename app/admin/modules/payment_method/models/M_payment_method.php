<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_payment_method extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }
  
  function upload_api($name,$id){
    $data = array(
        "api_file" => $name,
        "update_at" => date("Y-m-d H:i:s")
    );

    $this->db->where('id',$id);
    $this->db->update('a__payment_method', $data);
    if ($this->db->affected_rows() > 0 ) {
      return TRUE;
    }else{
      return FALSE;
    }

  }
  
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_login");
	  $this->load->library('session');
	  $this->load->library(array('session','pagination','form_validation'));
      $this->load->library('recaptcha');
    }


	public function index()
	{
		   
        $this->load->view('login');
 
        
    }

    public function ajax_action_login(){
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        
        $username = post('username'); 
        $password = md5(post('password'));

        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Username and password is required'),
                "form_error" => $error,
                "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
            );
            print json_encode($json_data);
            die();
        }else{
            $this->load->library('recaptcha');
            $captcha_answer = $this->input->post('captcha');
            $response = $this->recaptcha->verifyResponse($captcha_answer);

            // Processing ...
            if ($response['success']) {
              $cek = count($this->M_login->fetch_table("id_account","a__admin","username='$username' and password = '$password' and is_active = '1'"));
                $data_user = $this->M_login->get_row("*","a__admin","username='$username'","","",FALSE);
                if(@$cek==0){
                    $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Login Failed'),
                    "form_error" => "",
                    "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
                    );
                    print json_encode($json_data);
                    die();
                }else{
                    $this->session->set_userdata('resv2.a_log_session', TRUE);
                    $this->session->set_userdata('resv2.a_id', $data_user->id_account);
                    
                    $json_data =  array(
                        "result" => TRUE ,
                        "message" => array('head'=> 'Success', 'body'=> 'Login Success'),
                        "form_error" => "",
                        "redirect" => ''.base_url().$this->config->item('index_page').'/login/home'
                    );
                    print json_encode($json_data);                  
                }
            } else {
               $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Check Your Username and Password'),
                    "form_error" => "",
                    "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
                    );
                    print json_encode($json_data);
                    die();
                
            }
            
        }
    }

    public function home(){
    	if(!empty($this->session->userdata('resv2.a_log_session'))){
    		$data['content'] = 'dashboard';
            $data['active_user'] = count($this->M_login->fetch_table("id_member","m__member","is_active = '1' and is_banned !='1' "));
                
            $data['pending_order'] = $this->M_login->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['completed_order'] = count($this->M_login->fetch_table("*","m__orders","status= 'COMPLETED' "));
            $data['menu_post'] = $this->M_login->fetch_table("*","a__post_category","id_parent = '0'");
            
            $data['pending_payment'] = $this->M_login->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['paid_payment'] = count($this->M_login->fetch_table("*","m__payment","status= 'PAID' "));
            
            $this->load->view('login/home',$data);
    	}else{
    		redirect("login/");	
    	}
    }

    public function logout(){
        $session = array("resv2.a_log_session","resv2.a_id");
        $this->session->unset_userdata($session);
        $this->session->sess_destroy();
        redirect("login/");
    }
}
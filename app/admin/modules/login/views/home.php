
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Followersindo</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/datepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/theme.css" rel="stylesheet" media="all">

    <!-- RESV2 CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/resv2-standard.css" rel="stylesheet" media="all">

</head>

<body class="resv2-standard">
<div id="page-load" style="position: fixed; z-index: 999999; opacity: 0.7;  background: white; width: 100%; height: 100%; display: block;  ">
    <div class="page-loader__spin">
        
    </div>
</div>
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="<?=base_url().$this->config->item('index_page'); ?>/login/home">
                            <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="Followersindo" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="<?=base_url().$this->config->item('index_page'); ?>/login/home/">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>

                        <li>
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/payment">
                                <i class="fas fa-dollar"></i>Payment</a>
                        </li>

                        <li>
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/order">
                                <i class="fas fa-shopping-cart"></i>Order</a>
                        </li>
                        
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Product
                                <i class="fas fa-sort-down arrow"></i></a>
                                
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/product">Product</a>
                                </li>
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/product_api">Product Api</a>
                                </li>
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/product_type">Product Type</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/member">
                                <i class="fas fa-users"></i>Member</a>
                        </li>
                                                           
                        <li>
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/admin">
                                <i class="fas fa-user"></i>Admin</a>
                        </li>

                        <li class="has-sub <?php if($this->uri->segment(1)=="post" ){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Post
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/post/category">Category</a>
                                </li>
                                <?php foreach ($menu_post as $menu) { ?>
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/post?data=<?php echo strtolower($menu->name); ?>&id=<?php echo $menu->id_category; ?>"><?php echo $menu->name; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>      
                       
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cog"></i>Setting Payment
                                <i class="fas fa-sort-down arrow"></i>
                                </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment_account">Payment Account</a>
                                </li>
                                <li>
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment_method">Payment Method</a>
                                </li>
                            </ul>
                        </li>
                   </ul>     
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?=base_url().$this->config->item('index_page'); ?>/login/home">
                    <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="Followersindo" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub <?php if($this->uri->segment(1)=="login"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/login/home">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="payment"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/payment">
                                <i class="fas fa-dollar"></i>Payment</a>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="order"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/order">
                                <i class="fas fa-shopping-cart"></i>Order</a>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="product" or $this->uri->segment(1)=="product_api" or $this->uri->segment(1)=="product_type"){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Product 
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="<?php if($this->uri->segment(1)=="product"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/product">Product</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="product_api"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/product_api">Product Api</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="product_type"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/product_type">Product Type</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="member"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/member">
                                <i class="fas fa-users"></i>Member</a>
                        </li>

                        <li class="has-sub <?php if($this->uri->segment(1)=="admin"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/admin">
                                <i class="fas fa-user"></i>Admin</a>
                        </li>
                        
                        <li class="has-sub <?php if($this->uri->segment(1)=="post" ){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Post
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="<?php if($this->uri->segment(2)=="category"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/post/category">Category</a>
                                </li>
                                <?php foreach ($menu_post as $menu_post) { ?>
                                <li class="<?php if($this->input->get('data')==strtolower($menu_post->name)){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/post?data=<?php echo strtolower($menu_post->name); ?>&id=<?php echo $menu_post->id_category; ?>"><?php echo $menu_post->name; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>                     
                       
                       
                        <li class="has-sub <?php if($this->uri->segment(1)=="payment_account" or $this->uri->segment(1)=="payment_method"){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-cog"></i>Setting Payment
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="<?php if($this->uri->segment(1)=="payment_account"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment_account">Payment Account</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="payment_method"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment_method">Payment Method</a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub <?php if($this->uri->segment(1)=="sys_text"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/sys_text">
                                <i class="fas fa-cog"></i>Text</a>
                        </li>
                       
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                <!-- <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button> -->
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-shopping-cart"></i>
                                        <?php if (count($pending_order)>0) { ?><span class="quantity"><?php echo count($pending_order); ?></span><?php } ?>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have <?php echo count($pending_order); ?> Pending Order</p>
                                            </div>
                                            <?php foreach ($pending_order as $key_order) { ?>
                                            <div class="notifi__item" onclick="window.location = '<?=base_url().$this->config->item('index_page'); ?>/order?p_order=<?php echo $key_order->id; ?>' ">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-shopping-cart"></i>
                                                </div>
                                                <div class="content" >
                                                    <p><?php echo $key_order->no_transaction; ?></p>
                                                    <span class="date"><?php echo $key_order->created_at; ?></span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="email__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/order/data_pending/">See all pending order</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-card"></i>
                                        <?php if (count($pending_payment)>0) { ?><span class="quantity"><?php echo count($pending_payment); ?></span><?php } ?>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have <?php echo count($pending_payment); ?> pending payment</p>
                                            </div>
                                            <?php foreach ($pending_payment as $key_payment) { ?>
                                            <div class="notifi__item" onclick="window.location = '<?=base_url().$this->config->item('index_page'); ?>/payment?p_payment=<?php echo $key_payment->id; ?>'">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-card"></i>
                                                </div>
                                                <div class="content">
                                                    <p><?php echo $key_payment->no_transaction; ?></p>
                                                    <span class="date"><?php echo $key_payment->created_at; ?></span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="notifi__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/payment">See All pending payment</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">Admin</a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="account-dropdown__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/login/logout/">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                              <div class="au-card">
                                <?php  $this->load->view($content); ?>
                              </div>
                            </div>        
                        </div>
                      
                        
                    </div>
                </div>
            </div>
            <!-- Modal Tambah -->
            <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="modalTambah" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-primary">
                    <center><h5 style="color: white;" class="modal-title" id="title-tambah">Modal</h5></center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                  </div>
                  <div class="modal-body" id="body-tambah">
                    
                  </div>
                </div>
              </div>
            </div>


            <!-- Modal Lookup -->
            <div class="modal fade" id="modal_lookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" style="width: 100%;" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-warning">
                    <center><h5 style="color: white;" class="modal-title" id="title-tambah">Upload Api Product Api</h5></center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                  </div>
                  <div class="modal-body">
                    <input type="text" id="search_lookup" onchange="return ajax_action_lookup(this);" class="form-control" placeholder="Search By Name/Category/Id Service" name="">
                    <div style="background: #eee;">
                        <table class="table">
                          <thead>
                            <tr>
                              <th><center>Service</center></th>
                              <th><center>Name</center></th>
                              <th><center>Category</center></th>
                              <th><center>Rate</center></th>
                              <th><center>Min</center></th>
                              <th><center>Max</center></th>
                              <th><center>Action</center></th>
                            </tr>
                          </thead>
                          <tbody id="list-lookup">
                            
                          </tbody>
                        </table>
                    </div> 
                  </div> 
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>

            
        </div>

    </div>

    <?php 
          $ajax_list = "";
          
          if($this->uri->segment(2)=="category"){ 
            $ajax_list = "ajax_list_category();";
          }else if($this->uri->segment(1)=="post" and $this->input->get("id")!="" ){ 
            $ajax_list = "ajax_list_post();";
          }else if($this->uri->segment(1)=="sys_text"){ 
            $ajax_list = "ajax_list_text();";
          }else if($this->uri->segment(1)=="admin"){ 
            $ajax_list = "ajax_list_admin();";
          }else if ($this->uri->segment(1)=="member") {
            $ajax_list = "ajax_list_member();";
          }else if ($this->uri->segment(1)=="product_api") {
            $ajax_list = "ajax_list_product_api();";
?>
        <!-- Modal Upload Api Product Api -->
            <div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header bg-warning">
                    <center><h5 style="color: white;" class="modal-title">Upload Api Product Api</h5></center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                  </div>
                  <div class="modal-body">
                    <input type="hidden" id="id_upload" name="">
                    <input type="hidden" id="cek_api" name="">
                    <label>Api File : <span id="name_file"></span> <a onclick="ajax_action_delete_upload()" id="hapus_api" > delete</a></label>
                    <input type="file" name="userfile" class="form-control" id="userfile">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="ajax_action_upload()" class="btn btn-warning">Upload</button>
                  </div>
                </div>
              </div>
            </div>
<?php
          }else if ($this->uri->segment(1)=="product_type") {
            $ajax_list = "ajax_list_product_type();";
          }else if ($this->uri->segment(1)=="product") {
            $ajax_list = "ajax_list_product();";
          }else if ($this->uri->segment(1)=="payment_account") {
            $ajax_list = "ajax_list_payment_acc();";
          }else if ($this->uri->segment(1)=="payment_method") {
            $ajax_list = "ajax_list_payment_method();";
?>
<!-- Modal Upload Api Payment Method -->
<div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning">
            <center><h5 style="color: white;" class="modal-title">Upload Api Payment Method</h5></center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
        </div>
      <div class="modal-body">
        <input type="hidden" id="id_upload" name="">
        <input type="hidden" id="cek_api" name="">
        <label>Api File : <span id="name_file"></span> <a onclick="ajax_action_delete_upload()" id="hapus_api" > delete</a></label>
        <input type="file" name="userfile" class="form-control" id="userfile">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" onclick="ajax_action_upload()" class="btn btn-warning">Upload</button>
      </div>
    </div>
  </div>
</div>

<?php
          }else if ($this->uri->segment(1)=="payment") {
            $ajax_list = "ajax_list_payment();";
            ?>
<!-- Modal Status Payment -->
<div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form id="form_tambah">
      <div class="modal-header bg-primary">
                    <center><h5 style="color: white;" class="modal-title" id="title-tambah">Status</h5></center>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                  </div>
      <div class="modal-body">
        <input type="hidden" id="e_id" name="">
        <input type="hidden" id="e_id_member" name="">
        <table class="table">
          <tr>
            <td colspan="">
              <label>No Transaction :</label>
              <input type="text" class="form-control" disabled="" id="e_no_transaction" name="">      
            </td>
            <td>
              <label>Type :</label>
              <input type="text" class="form-control" disabled="" id="e_type" name="">      
            </td>
          </tr>
          <tr>
            <td>
              <label>Member :</label>
              <input type="text" class="form-control" disabled="" id="e_member" name="">      
            </td>
            <td>
                <label>Member Account</label>
                <input type="text" class="form-control" disabled="" id="e_m_account" name="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Nominal Transfer :</label>
              <input type="text" class="form-control" disabled="" id="e_nominal_transfer" name="">
            </td>
            <td>
              <label>Nominal Topup :</label>
              <input type="text" class="form-control" disabled="" id="e_nominal_topup" name="">      
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <label>File Proof :</label><br>
              <img style="height: 300px;" src="" id="e_image" alt="" class="img-rounded">      
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <input type="hidden" id="old_status" name="">
              <label>Status</label>
              <select class="form-control" id="e_status">
                <option value="PENDING">PENDING</option>
                <option value="WAITING">WAITING</option>
                <option value="PAID">PAID</option>
                <option value="FRAUD">FRAUD</option>
                <option value="CANCEL">CANCEL</option>
              </select>
            </td>
          </tr>
          <tr>
              <td colspan="2">
                  <label>Admin Account</label>
                  <select class="form-control" id="e_a_account"></select>
              </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="ajax_action_set_status()" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
            <?php
          }else if ($this->uri->segment(1)=="order") {
            $ajax_list = "ajax_list_order();";
            ?>
<!-- Modal Status Order -->
<div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
                   <h4 style="color: white;" class="modal-title" id="myModalLabel"><center>Order - <span id="lbl-title">Manual</span></center></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                  </div>
      <div class="modal-body">
        <input type="hidden" id="e_id" name="">
        <input type="hidden" id="e_id_member" name="">
        <table class="table">
          <tr>
            <td colspan="">
              <label>No Transaction :</label>
              <input type="text" class="form-control" disabled="" id="e_no_transaction" name="">
            </td>
            <td>
              <label>Order Id Auto :</label>
              <input type="text" class="form-control" disabled="" id="e_order_id_auto" name="">
            </td>
            <td>
              <label>Price Product :</label>
              <input type="text" class="form-control" disabled="" id="e_price_product">
            </td>
          </tr>
          <tr>
            <td>
             <label>Product Name :</label>
             <input type="text" class="form-control" disabled="" id="e_product" name=""> 
            </td>
            <td>
              <label>Date</label>
              <input type="text" class="form-control" disabled="" id="e_date" name="">
            </td>
            <td>
              <label>Member :</label>
              <input type="text" class="form-control" disabled="" id="e_member" name="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Data :</label>
              <input type="text" class="form-control" disabled="" id="e_data" name="">
            </td>
            <td>
              <label>Qty Order :</label>
              <input type="text" class="form-control" disabled="" id="e_qty" name="">
            </td>
            <td>
              <label>Total :</label>
              <input type="text" class="form-control" disabled="" id="e_price" name="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Api Name :</label>
              <input type="text" disabled="" class="form-control" id="e_api_name" name="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Is Sync :</label><br>
              <label class="switch">
                <input type="checkbox" onclick="ajax_action_issycn()" id="is_auto">
                <span class="slider round"></span>
              </label>
            </td>
            <td>
              <label>Last Sycn :</label>
              <input type="text" id="last_sycn" class="form-control" disabled="" name="">
            </td>
            <td>
              <br>
              <button class="btn btn-success btn-sm" id="btn-sync" onclick="ajax_action_sycn()"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Sync</button>
            </td>
          </tr>
          <tr>
            <td colspan="3">
              <label>Status</label>
              <select class="form-control" onchange="count_validation(this)" id="e_status">
                <option value="PENDING">PENDING</option>
                <option value="INPROGRESS">INPROGRESS</option>
                <option value="PROCESSING">PROCESSING</option>
                <option value="COMPLETED">COMPLETED</option>
                <option value="PARTIAL">PARTIAL</option>
                <option value="CANCELLED">CANCELLED</option>
              </select>
            </td>
          </tr>
          </table>
          <div id="form-qty" style="background: #eee;">
            <table class="table">
                <tr>
                  <td>
                    <label>Qty Start Count:</label>
                    <input type="number" onkeyup="count_qty()" name="start" id="start" class="form-control">
                    <input type="hidden" name="old_start" id="old_start" class="form-control">
                  </td>
                  <td>
                      <label>Qty End Count:</label>
                      <input type="number" name="end" id="end" class="form-control">
                      <input type="hidden" name="old_end" id="old_end" class="form-control">    
                  </td>
                </tr>
              </table>
              <table class="table">
                <tr>
                  <td>
                      <label>Qty Remain:</label>
                      <input type="number" name="remain" id="remain" class="form-control">
                      <input type="hidden" name="old_remain" id="old_remain" class="form-control">
                  </td>
                   <td>
                      <label>Qty Success:</label>
                      <input type="number" name="success" onkeyup="count_failed()" id="success" class="form-control">
                      <input type="hidden" name="old_success" id="old_success" class="form-control">
                    </td>
                   <td>
                      <label>Qty Failed :</label>
                      <input type="number" name="failed" id="failed" class="form-control">
                      <input type="hidden" name="old_failed" id="old_failed" class="form-control">
                    </td>
                </tr>
              </table>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="btn-save-status" onclick="ajax_action_set_status()" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>

            <?php
          }

        if($this->input->get("p_payment")!=""){ $p_payment = $this->input->get("p_payment"); }else{ $p_payment = "''"; }
        if($this->input->get("p_order")!=""){ $p_order = $this->input->get("p_order"); }else{ $p_order = "''"; }
     ?> 


    <!-- Jquery JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/datepicker/bootstrap-datetimepicker.min.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
    <script type="text/javascript">
      $('.select2').select2();
      $(".datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
      var csfrData = {};
      csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
      $.ajaxSetup({
      data: csfrData
      });

        var popup_payment = <?php echo $p_payment; ?>;
        if (popup_payment!="") {
          ajax_get_status(popup_payment);  
        }

        var popup_order = <?php echo $p_order; ?>;
        if (popup_order!="") {
          ajax_get_status(popup_order);  
        }

      function ajax_list_admin(){
            $('#list_admin').DataTable({ 
         
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/admin/ajax_list/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                               
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
      }

      function ajax_list_text(){
            $('#list_text').DataTable({ 
         
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/sys_text/ajax_list/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                               
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
      }

      function ajax_list_category(){
            $('#list_category').DataTable({ 
         
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_list_category/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                               
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
        }

        function ajax_list_post(){
            var id = '<?php echo $this->input->get("id"); ?>';
            $('#list_post').DataTable({ 
         
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/post/ajax_list_post/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',id:id }
                               
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
        }

      function ajax_list_member() {
        $('#list_member').DataTable({ 
       
              "processing": true, //Feature control the processing indicator.
              "serverSide": true, //Feature control DataTables' server-side processing mode.
              "order": [], //Initial no order.
              "pageLength": 10,
              // Load data for the table's content from an Ajax source
              "ajax": {
                  "url": "<?php echo base_url().$this->config->item('index_page'); ?>/member/ajax_list/",
                  "type": "POST",
                  "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
              },
       
              //Set column definition initialisation properties.
              "columnDefs": [
              { 
                  "targets": [ 0 ], //first column / numbering column
                  "orderable": false, //set not orderable
              },
              ],
       
          });

      }

      function ajax_list_product_api() {
          $('#list_product_api').DataTable({ 
         
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/ajax_list/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });

        }

    function ajax_list_product_type() {
      $('#list_product_type').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url().$this->config->item('index_page'); ?>/product_type/ajax_list/",
                "type": "POST",
                "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
     
        });

    }

    function ajax_list_product(){
        $('#list_product').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url().$this->config->item('index_page'); ?>/product/ajax_list/",
                "type": "POST",
                "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
     
        });
      }


    function ajax_list_payment_acc(){
        $('#list_payment_acc').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_list/",
                "type": "POST",
                "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
     
        });
      }


    function ajax_list_payment_method() {
      $('#list_payment_method').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url().$this->config->item('index_page'); ?>/payment_method/ajax_list/",
                "type": "POST",
                "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
     
        });

    }


     function ajax_list_payment(){
        $('#list_payment').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_list/",
                "type": "POST",
                "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
     
        });
      }


      function ajax_list_order(){
        var status = '<?php echo $this->uri->segment(2); ?>';
        $('#list_order').DataTable({ 
     
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_list/",
                "type": "POST",
                "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',status:status }
                           
            },
     
            //Set column definition initialisation properties.
            "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
     
        });
      }

    
    <?php echo $ajax_list; ?>
    </script>
    
</body>

</html>
<script type="text/javascript">
        $(document).ready(function(){
            $('#page-load').hide();
        });
    </script>
<!-- end document-->
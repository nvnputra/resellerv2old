<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_cronjob");
      $this->load->model("order/M_order");
	  $this->load->library('session');
      $this->load->library('curl');   
    }

    public function auto_sycn_order_status ()
    {
        
        $this->benchmark->mark('start');
        //mengambil order yang statusnya auto dan is sync aktif dan status pending/progress 
        $data = $this->M_cronjob->limit_order_auto();
        /*validasi jika data order 0*/
        if (count($data)=="0") {
           $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Order Status Tidak ada '),
                    "form_error" => '',
                    "redirect" => ''
                );
            print json_encode($json_data);
            die();
        }

        /*prosese pengecekan ke API*/ 
        $count_berubah = 0; $count_tidak = 0;
        foreach ($data as $key) {
            $params = array(
                "key" => $key->api_key,
                "request" => 'status',
                "supplier" => $key->api_name,
                "idorder" => $key->orderid_auto
            );
        $get_lookup = $this->curl->simple_post($key->api_url, $params, array(CURLOPT_BUFFERSIZE => 10));

        $arr_data = json_decode($get_lookup);
        $status = strtoupper($arr_data->data->status); 
        //cek status jika kosong / selain yang ada disini maka langsung stop, next loop

        //harusnya menunggu status
        $sukses = $key->qty_order - $arr_data->data->remains;
        $fail  = $key->qty_order - $sukses;
        /*set data update status order*/
        if($status=="INPROGRESS"){
            $is = "is_inprogress";
            $in = "inprogress_at";
        }else if($status=="PROCESSING"){
            $is = "is_processing";
            $in = "processing_at";
        }else if($status=="COMPLETED"){
            $is = "is_completed";
            $in = "completed_at";
        }else if($status=="PARTIAL"){
            $is = "is_partial";
            $in = "partial_at";
        }else if($status=="CANCELLED"){
            $is = "is_cancel";
            $in = "cancel_at";
        }else{ //add by afidz
            $status = "PENDING";
        }
                
                /*set data update*/
                if($status!="PENDING"){ 
                    $data = array(
                        $is => 1 ,
                        $in => date("Y-m-d H:i:s"),
                        "status" => $status,
                        "lastcheck_sync" => date("Y-m-d H:i:s"),
                        "qty_startcount" => $arr_data->data->start_count,
                        "qty_remain" => $arr_data->data->remains,
                        "qty_success" => $sukses,
                        "qty_failed" => $fail,
                        "qty_endcount" => $arr_data->data->start_count+$sukses,
                        "action_by" => $this->session->userdata('resv2.a_id'),
                        "action_at" => date("Y-m-d H:i:s"),
                        "update_at" => date("Y-m-d H:i:s")
                    );
                }else{     
                    $data = array(
                        "status" => $status,
                        "lastcheck_sync" => date("Y-m-d H:i:s"),
                        "qty_startcount" => $arr_data->data->start_count,
                        "qty_remain" => $arr_data->data->remains,
                        "qty_success" => $sukses,
                        "qty_failed" => $fail,
                        "qty_endcount" => $arr_data->data->start_count+$sukses,
                        "action_by" => $this->session->userdata('resv2.a_id'),
                        "action_at" => date("Y-m-d H:i:s"),
                        "update_at" => date("Y-m-d H:i:s")
                    );
                }
               /*----------------------*/

               /*set validasi data update balance ,mutasi, refaund*/
              if ($status=="CANCELLED" || $status=="PARTIAL" ){
                $get_balance = $this->M_order->get_balance_mutation($key->id_member); 
                $total = ($key->price * $fail) /1000;
                $delete_mutation = null;
                $delete_refaund = 1;

                $set_refaund = array(
                    "id_order"=>$key->id,
                    "id_user" =>$key->id_member,
                    "price" => $key->price,
                    "qty" => $fail,
                    "total" => $total,
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );

                $kredit = $key->total-$total;
                $set_mutation = array(
                    "id_user"=> $key->id_member,
                    "debt" => $total,
                    "balance" => $get_balance->balance + $total,
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
                $set_balance = array(
                    "balance" => $get_balance->balance + $total
                ); 
                 

              }else{
                $delete_mutation = null;
                $delete_refaund = null;
                $set_refaund = null;
                $set_balance = null;
                $set_mutation = null;
              }
              /*----------------------*/

            /*validasi data_notif*/
            if ($status!=$key->status) {
                $data_notif = array(
                  "id_user" => $key->id_member,
                  "created_at" => date("Y-m-d H:i:s"),
                  "text" => "Order ".$key->no_transaction." is ".$status
                );
                $this->send_email($key->id_member,$key->no_transaction,$status);
                $count_berubah += 1;
            }else{ 
                $data_notif = null;
                $count_tidak += 1;
             }
            

            $edit = $this->M_order->update_balance($data,$key->id,$set_balance,$key->id_member,$data_notif,$set_mutation,$set_refaund,$delete_mutation,$delete_refaund);

        }
        $this->benchmark->mark('end');
        $json_data =  array(
                    "result" => TRUE ,
                    "message" => array('head'=> 'success', 'body'=> 'Status Berubah :'.$count_berubah.',Status Tidak Berubah :'.$count_tidak.',Time :'.$this->benchmark->elapsed_time('start', 'end')),
                    "redirect" => ''.base_url().$this->config->item('index_page').'/order'
                );
        print json_encode($json_data);
        

    }


    public function send_email($id_member,$no_transaction,$status){
        $this->load->library('ElasticEmail');
        $data_user = $this->M_cronjob->get_row("*","m__member","id_member='$id_member' ","","",FALSE);

        $Elastic = new ElasticEmail();
        $admin = "admin@resellerindo.com";
        $from = $admin;
        $fromName = "resellerindo";
        $to = $data_user->email;
        $subject = "Order ".$no_transaction;
        $bodyText = ""; 
        $bodyHtml = 
        "<b>Hello ".$data_user->username." ,</b><br><br><br>
        Email Address : ".$data_user->email."<br>
        Thanks for order in Resellerindo!  <br>
        We're happy you're here. Your order ".$no_transaction." is ".$status." by admin resellerindo .
        <br><br><br>
        Thanks,<br>
        Admin Resellerindo
        "; 

        $send = $Elastic->send($from,$fromName,$subject,$to,$bodyText,$bodyHtml);
        $send = json_decode($send);
        
        return $send->success;
    }
    
}

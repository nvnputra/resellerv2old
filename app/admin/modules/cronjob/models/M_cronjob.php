<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cronjob extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }
  

  public function limit_order_auto()
  {
    $query = $this->db->query("SELECT
                                c.api_name,c.api_key,c.api_url,a.*
                              FROM
                                m__orders a
                              JOIN
                                a__product b ON a.id_product = b.id
                              JOIN
                                a__product_api c ON c.id = b.id_api
                              WHERE 
                                a.is_auto = '1'
                              AND a.is_sync = '1'
                              AND (
                                a.STATUS = 'PENDING'
                                OR a.STATUS = 'INPROGRESS'
                                OR a.STATUS = 'PROCESSING'
                              )
                              ORDER BY a.lastcheck_sync ASC,
                              a.date ASC limit 50
                              ");
    $result = $query->result();
    return $result;
  }
  
  
  
}
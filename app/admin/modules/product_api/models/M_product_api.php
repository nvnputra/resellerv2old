<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_product_api extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }
  

  function upload_api($name,$id){
    $data = array(
        "api_file" => $name
    );

    $this->db->where('id',$id);
    $this->db->update('a__product_api', $data);
    if ($this->db->affected_rows() > 0 ) {
      return TRUE;
    }else{
      return FALSE;
    }

  }

  function showedit($id){
    return  $this->fetch_table("*","a__product_api","id='$id'","","","","",FALSE);
  }

  
  
}
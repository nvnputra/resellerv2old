<div class="card-header bg-success">
  <h4 style="color: white;">Edit Product Api</h4>
</div>
<?php 
if (count($data)=="0") {
  echo "<br><br><center><h4>Data Not Found</h4></center><br><br>";
}
foreach ($data as $key) { ?>
<form method="post" onsubmit="return ajax_action_edit_api_product();">
        <input type="hidden" value="<?php echo $key->id; ?>" name="id" id="id">
        <table class="table">
          <tr>
            <td>
              <label>Nama Api :</label>
              <input type="text" name="nama" id="e_nama" value="<?php echo $key->api_name; ?>" class="form-control">
            </td>
            <td>
              <label>Url :</label>
              <input type="text" name="url" id="e_url" value="<?php echo $key->api_url; ?>" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Key :</label>
              <input type="text" name="key" id="e_key" value="<?php echo $key->api_key; ?>" class="form-control">
            </td>
            <td>
              <!-- <label>File :</label>
              <input type="text" disabled="" name="file" id="e_file" class="form-control"> -->
            </td>
          </tr>
        </table>
        <div id="alert_product_api"></div>
      <div class="modal-footer">
        <a href="<?=base_url().$this->config->item('index_page'); ?>/product_api"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_product_api" class="btn btn-primary">Save</button>
      </div>
</form>
<?php }?>

<script type="text/javascript">
  function ajax_action_edit_api_product(){
    var id = $("#id").val();
    var nama = $("#e_nama").val();
    var url = $("#e_url").val();
    var key = $("#e_key").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/ajax_action_edit_api/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, url:url,key:key,id:id},
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
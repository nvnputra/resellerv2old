<div class="card-header bg-primary">
  <h4 style="color: white;">New Product Api</h4>
</div>
<form method="post" onsubmit="return ajax_action_add_api_product();">
        <table class="table">
          <tr>
            <td>
              <label>Nama Api :</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </td>
            <td>
              <label>Url :</label>
              <input type="text" name="url" id="url" class="form-control">
            </td>
          </tr>
          <tr>
            <td>
              <label>Key :</label>
              <input type="text" name="key" id="key" class="form-control">
            </td>
            <td>
              <!-- <label>File :</label>
              <input type="text" disabled="" name="file" id="file" class="form-control"> -->
            </td>
          </tr>
        </table>
        <div id="alert_product_api"></div>
      <div class="modal-footer">
        <a href="<?=base_url().$this->config->item('index_page'); ?>/product_api"><button type="button" class="btn btn-danger">Back</button></a>
        <button type="submit" id="btn_product_api" class="btn btn-primary">Save</button>
      </div>
</form>

<script type="text/javascript">
  function ajax_action_add_api_product() {
    //var data = $("#form_tambah").serialize();

    var nama = $("#nama").val();
    var url = $("#url").val();
    var key = $("#key").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/ajax_action_add_api/",
              type:'POST',
              dataType: "json",
              data: {nama:nama, url:url,key:key},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

    return false;
  }
</script>
<style type="text/css">
  #list_product_api td{
    text-align: center;
  }
</style>




<a href="<?=base_url().$this->config->item('index_page'); ?>/product_api/tambah"><button class="btn btn-primary btn-sm" >New Product Api</button></a>
<br><br>
<table class="table" id="list_product_api">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>Nama</center></th>
      <!-- <th><center>Url</center></th> -->
      <!-- <th><center>Key</center></th> -->
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
 
  </tbody>
</table>


<script type="text/javascript">
  



  
  function ajax_action_delete_api_product(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/ajax_action_delete_api/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_edit_api_product(id){
    window.location = "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/edit_product_api/"+id;
  }

  

  function ajax_show_upload(id,apifile){
    if(apifile==""){
      $('#hapus_api').hide();
    }else{
      $('#hapus_api').show();
    }
    $('#id_upload').val(id);
    $('#name_file').html(apifile);
    $('#cek_api').val(apifile);
    $('#userfile').val("");
    $('#modal_upload').modal('show');
  }

  function ajax_action_upload(){
    var file_data = $('#userfile').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('id', $('#id_upload').val());
    form_data.append('apifile', $('#cek_api').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo
$this->security->get_csrf_hash(); ?>');

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/ajax_action_upload/",
              dataType: 'json',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_delete_upload(){
  if (confirm('Apakah Anda Yakin Menghapus Api File?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/product_api/ajax_action_delete_upload/",
              type:'POST',
              dataType: "json",
              data: {id:$('#id_upload').val(), file:$('#cek_api').val()},
              beforeSend: function () {
                     $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }
  

</script>
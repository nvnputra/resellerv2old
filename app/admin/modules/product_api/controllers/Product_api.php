<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_api extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_product_api");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
		if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'product_api';
            $data['pending_order'] = $this->M_product_api->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product_api->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product_api->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function tambah()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'tambah';
            $data['pending_order'] = $this->M_product_api->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product_api->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product_api->fetch_table("*","m__payment","status= 'PENDING' ");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function edit_product_api($id)
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $data['content'] = 'edit';
            $data['pending_order'] = $this->M_product_api->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_product_api->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_product_api->fetch_table("*","m__payment","status= 'PENDING' ");
            $data['data'] = $this->M_product_api->fetch_table("*","a__product_api","id='$id'");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

     public function ajax_list(){
         $column = '*';
         $column_order = array(null, 'api_name','api_url','api_key'); //set column field database for datatable orderable
         $column_search = array('api_name','api_url','api_key'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "a__product_api";
         $where = "";
         $joins = "";

        $list = $this->M_product_api->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $api_file = "'$key->api_file'";
            $row = array();
            $row[] = $no;
            $row[] = $key->api_name;
            /*$row[] = $key->api_url;*/
            /*$row[] = $key->api_key;*/
            $row[] = '<button onclick="ajax_get_edit_api_product('.$key->id.')" class="btn btn-success btn-sm">Edit</button>
          <button onclick="ajax_action_delete_api_product('.$key->id.')" class="btn btn-danger btn-sm">Delete</button>
          <button class="btn btn-warning btn-sm" onclick="ajax_show_upload('.$key->id.','.$api_file.')">Upload Api</button>';
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_product_api->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_product_api->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_api(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('url', 'url', 'required');
        $this->form_validation->set_rules('key', 'key', 'required');

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
                "api_name" => post("nama"),
                "api_url" => post("url"),
                "api_key" => post("key")
            );
            $add = $this->M_product_api->insert_table("a__product_api",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product_api'
                );
                print json_encode($json_data);
            }
        }

    }

    public function ajax_action_delete_api(){
           $delete = $this->M_product_api->delete_table("a__product_api","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menghapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menghapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product_api'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_get_data_edit(){
        $id = post("id"); 
        $data = $this->M_product_api->fetch_table("*","a__product_api","id='$id'");
        print json_encode($data);
    }

    public function ajax_action_edit_api(){
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('url', 'url', 'required');
        $this->form_validation->set_rules('key', 'key', 'required');

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{        
            $data = array(
                "api_name" => post("nama"),
                "api_url" => post("url"),
                "api_key" => post("key")
                );
            $edit = $this->M_product_api->update_table("a__product_api",$data,"id",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/product_api'
                );
                print json_encode($json_data);
            }

        }

    }

    public function ajax_action_upload(){
        
        $config['upload_path'] = './uploads/product_api/';
        $config['allowed_types'] = 'gif|jpg|png|doc|txt';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
  
        $this->load->library('upload', $config);

        
  
        if (!$this->upload->do_upload('file'))
        {
            $result = FALSE;
            $message = array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', ''));
            $redirect = "";
        }
        else
        {
            $data = $this->upload->data();
            if(post('apifile')!=""){
                unlink('./uploads/product_api/' . post('apifile'));  
            }
            $upload_api = $this->M_product_api->upload_api($data['file_name'], post("id"));
            if($upload_api==TRUE)
            {
                $result = TRUE;
                $message =  array('head'=> 'Success', 'body'=> 'Sukses Mengupload');
                $redirect = ''.base_url().$this->config->item('index_page').'/product_api';
            }
            else
            {
                $result = FALSE;
                $message =  array('head'=> 'Gagal', 'body'=> 'Gagal Mengupload');
                $redirect = '';
            }
        }
    
            $json_data =  array(
                    "result" => $result ,
                    "message" => $message,
                    "redirect" => $redirect
            );
        print json_encode($json_data);
    }

    public function ajax_action_delete_upload(){
            $upload_api = $this->M_product_api->upload_api("", post("id"));
            if($upload_api==TRUE)
            {
                unlink('./uploads/product_api/' . post('file')); 
                $result = TRUE;
                $message =  array('head'=> 'Success', 'body'=> 'Sukses Menghapus Api');
                $redirect = ''.base_url().$this->config->item('index_page').'/product_api';
            }
            else
            {
                $result = FALSE;
                $message =  array('head'=> 'Gagal', 'body'=> 'Gagal Menghapus Api');
                $redirect = '';
            }

            $json_data =  array(
                    "result" => $result ,
                    "message" => $message,
                    "redirect" => $redirect
            );
        print json_encode($json_data);
    }

    
}

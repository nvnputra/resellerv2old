<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class S_district extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_s_district");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function ajax_get_district()
	{
		$id = post("id");
		$data = $this->M_s_district->fetch_table("*","sys__districts","regency_id='$id'"); 
        print json_encode($data);
        
        
    }    
}

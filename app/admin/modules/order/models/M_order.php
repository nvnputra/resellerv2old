<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_order extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }

  public function get_balance_mutation($id_user){
    $query = $this->db->query("SELECT balance FROM m__mutation WHERE id_user = '$id_user' ORDER BY  id DESC limit 1");
    $result = $query->row();
    return $result;
  }

  public function get_id_mutation($id){
    $query = $this->db->query("SELECT refund_id as id FROM m__orders WHERE id = '$id' ");
    $result = $query->row();
    return $result;
  }

  public function update_balance($data,$id,$set_balance,$id_member,$data_notif,$set_mutation,$set_refaund,$delete_mutation,$delete_refaund){
  	 $this->db->trans_begin();

    //Update Status Order
    $this->db->where('id',$id);
    $this->db->update('m__orders', $data);

    //Set Balance Member
    if($set_balance!="" || $set_balance!=null){
      $this->db->where('id_member',$id_member);
      $this->db->update('m__member', $set_balance);
    }

    //Delete Mutation
    if($delete_mutation!="" || $delete_mutation!=null ){
      $this->delete_table("m__mutation","id_order_refund",$delete_mutation);
    }

    //Delete Refaund
    if($delete_refaund!="" || $delete_refaund!=null ){
      $this->delete_table("m__orders_refund","id_order",$id);
    }


    //Add Refaund
    if ($set_refaund!="" || $set_refaund!=null) {
      $this->db->insert('m__orders_refund', $set_refaund);
      $get_id = $this->db->insert_id();
      $data_refaund = array(
        "is_refund" => '1',
        "refund_at" => date("Y-m-d H:i:s"),
        "refund_id" => $get_id
      );

      $array_id = array(
        "id_order_refund" => $get_id
      );
      $new_mutation = array_merge($array_id,$set_mutation);
      //Add Mutation
      if ($set_mutation!="" || $set_mutation!=null) {
        $this->db->insert('m__mutation', $new_mutation);
      }
    }else{
      $data_refaund = array(
        "is_refund" => "",
        "refund_at" => "",
        "refund_id" => "",
      );
    }

    //Update is_refaund di Order
    $this->db->where('id',$id);
    $this->db->update('m__orders', $data_refaund);

    //Insert Notif 
    if ($data_notif!="" || $data_notif!=null) {
      $this->db->insert('m__notifications', $data_notif);
    }

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }
  }


  
  
  
}
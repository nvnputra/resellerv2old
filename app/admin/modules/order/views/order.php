<style type="text/css">
  a{
    cursor: pointer;
  }
  #list_order td{
    text-align: center;
  }
  .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

  .switch input {display:none;}

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }

  input:checked + .slider {
    background-color: #2196F3;
  }

  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }

  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }

  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }
</style>
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link <?php if($this->uri->segment(2)==null){ echo "active"; } ?>" href="<?=base_url().$this->config->item('index_page'); ?>/order/">All (<?php echo $count_all; ?>)</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php if($this->uri->segment(2)=="data_pending"){ echo "active"; } ?>" href="<?=base_url().$this->config->item('index_page'); ?>/order/data_pending">Pending (<?php echo $count_pending; ?>)</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php if($this->uri->segment(2)=="data_completed"){ echo "active"; } ?>" href="<?=base_url().$this->config->item('index_page'); ?>/order/data_completed">Completed (<?php echo $count_complete; ?>)</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?php if($this->uri->segment(2)=="data_cancelled"){ echo "active"; } ?>" href="<?=base_url().$this->config->item('index_page'); ?>/order/data_cancelled">Cancelled  (<?php echo $count_cancel; ?>)</a>
  </li>
</ul>
<br>
<button onclick="ajax_action_order_auto_sycn()" class="btn btn-success btn-sm" id="sync-all"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Sync All</button><br><br>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-striped" id="list_order">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>No Transaction</center></th>
      <th><center>Product</center></th>
      <th><center>Member</center></th>
      <th><center>Date</center></th>
      <th><center>Qty Order</center></th>
      <th><center>Total</center></th>
      <th><center>Data</center></th>
      <th><center>Status</center></th>
    </tr>
  </thead>
  <tbody>
   

  </tbody>
</table>
</div>


<script type="text/javascript">
  
   function ajax_get_status(id){
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_get_status/",
              type:'POST',
              dataType: "json",
              data: { id:id },
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                var i ;
                for(i = 0; i<data.length; i++){
                  $("#e_no_transaction").val(data[i].no_transaction);
                  $("#e_product").val(data[i].product_name);
                  $("#e_member").val(data[i].username);
                  $("#e_date").val(data[i].date);
                  $("#e_qty").val(data[i].qty_order);
                  $("#e_price").val(data[i].total);
                  $("#e_price_product").val(data[i].price);
                  $("#e_data").val(data[i].target);
                  $("#e_id").val(data[i].id);
                  $("#e_id_member").val(data[i].id_member);
                  $("#e_status").val(data[i].status);
                  $("#start").val(data[i].qty_startcount);
                  $("#remain").val(data[i].qty_remain);
                  $("#success").val(data[i].qty_success);
                  $("#failed").val(data[i].qty_failed);
                  $("#end").val(data[i].qty_endcount);
                  $("#old_start").val(data[i].qty_startcount);
                  $("#old_remain").val(data[i].qty_remain);
                  $("#old_success").val(data[i].qty_success);
                  $("#old_failed").val(data[i].qty_failed);
                  $("#old_end").val(data[i].qty_endcount);
                  $("#e_order_id_auto").val(data[i].orderid_auto);
                  $("#e_api_name").val(data[i].api_name);
                  $("#last_sycn").val(data[i].lastcheck_sync);
                  if(data[i].is_auto=="1"){
                    $('#lbl-title').html("AUTO");
                  }else{
                    $('#lbl-title').html("MANUAL");
                  }
                  if(data[i].is_sync=="1"){
                    $('#is_auto').prop('checked', true);
                  }else{
                    $('#is_auto').prop('checked', false);
                  }
               }
                cek_issycn();
                $('#modal_status').modal('show');
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function  ajax_action_order_auto_sycn() {
   $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/cronjob/auto_sycn_order_status/",
              type:'POST',
              dataType: "json",
              data: { },
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},1000);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }
  function ajax_action_issycn() {
    var id = $('#e_id').val();
   if ($('#is_auto').is(':checked')) {
    var is_sycn = 1;
   }else{
    var is_sycn = 0;
   }

   $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_action_issycn/",
              type:'POST',
              dataType: "json",
              data: { id:id,is_sycn:is_sycn },
             beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      cek_issycn();
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

  }

  function cek_issycn() {         
        if ($('#is_auto').is(':checked')) {
        var is_sycn = 1;
       }else{
        var is_sycn = 0;
       }
                  if(is_sycn==1){
                      $("#remain").prop('disabled', true);
                      $("#start").prop('disabled', true);
                      $("#success").prop('disabled', true);
                      $("#failed").prop('disabled', true);
                      $("#end").prop('disabled', true);
                      $("#e_status").prop('disabled', true);
                      $("#btn-sync").prop('disabled', false);
                      $("#btn-save-status").prop('disabled', true);
                    }else{
                      $("#remain").prop('disabled', false);
                      $("#start").prop('disabled', false);
                      $("#success").prop('disabled', false);
                      $("#failed").prop('disabled', false);
                      $("#end").prop('disabled', false);
                      $("#e_status").prop('disabled', false);
                      $("#btn-sync").prop('disabled', true);
                      $("#btn-save-status").prop('disabled', false);
                    }
  }

  function ajax_action_sycn(){
    var id_auto = $('#e_order_id_auto').val();
    var api_name = $('#e_api_name').val();
    var id = $('#e_id').val();
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_action_sycn/",
              type:'POST',
              dataType: "json",
              data: { id_auto:id_auto,api_name:api_name,id:id },
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                  $("#remain").val(data.remains);
                  $("#start").val(data.start_count);
                  $("#e_status").val(data.status.toUpperCase());
                  $("#last_sycn").val(data.lastcheck_sync);
                  count_qty();
                  count_failed();
                  $('#btn-save-status').trigger('click');
             },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function count_failed(){
    if(parseInt($('#success').val())>parseInt($('#e_qty').val())){
      alert("Order Only "+parseInt($('#e_qty').val()));
      $('#success').val($('#old_success').val());
    }

    var success = 0;
    if($.trim($('#success').val()) != ''){
      success = parseInt($('#success').val());
    }

    if($('#e_status').val()=="PARTIAL"  ){
      var qty_fail = parseInt($('#e_qty').val())-success;
      var qty_end = parseInt($('#start').val())+success;
      $('#failed').val(qty_fail);
      $('#end').val(qty_end);
    }
  }

  function count_qty(){
    var start = 0;
    if($.trim($('#start').val()) != ''){
      start = parseInt($('#start').val());
    }

    if($('#e_status').val()=="COMPLETED"){
      var hasil = start+parseInt($('#e_qty').val());
      $('#end').val(hasil);
      $('#success').val($('#e_qty').val());
    }else if($('#e_status').val()=="PARTIAL"  ){
      var hasil = start+parseInt($('#success').val());
      $('#end').val(hasil);
    }
  }

  function count_validation(key){
    
    var qty = parseInt($('#e_qty').val());
    var start = 0;
    var success =0;

    if(key.value=="INPROGRESS" || key.value=="PROCESSING" ){
      $("#end").val("");
      $("#remain").val($('#old_remain').val());
      $("#start").val($('#old_start').val());
      $("#success").val($('#old_success').val());
      $("#failed").val($('#old_failed').val());
      $("#end").prop('readonly', true);
      $("#remain").prop('readonly', false);
      $("#start").prop('readonly', false);
      $("#success").prop('readonly', false);
      $("#failed").prop('readonly', false);
    }else if(key.value=="COMPLETED"  ){
      $("#start").val($('#old_start').val());
          if($.trim($('#start').val()) != ''){
            var start = parseInt($('#start').val());
          }
      $("#remain").val("0");
      $("#failed").val("0");
      $("#success").val(qty);
      $("#end").val(start+qty);
      $("#start").prop('readonly', false);
      $("#remain").prop('readonly', true);
      $("#success").prop('readonly', true);
      $("#failed").prop('readonly', true);
      $("#end").prop('readonly', true);
    }else if(key.value=="CANCELLED" || key.value=="PENDING"  ){
      
      $("#remain").val(qty);
      if(key.value=="CANCELLED") {
        $("#failed").val(qty);
        $("#start").val($('#old_start').val());
        $('#end').val($('#old_start').val()); 
      }else{
        $("#start").val("0");
        $("#failed").val("0");
        $("#end").val("0"); 
      }
      $("#success").val("0");
      $("#remain").prop('readonly', true);
      $("#start").prop('readonly', true);
      $("#success").prop('readonly', true);
      $("#failed").prop('readonly', true);
      $("#end").prop('readonly', true);
    }else if(key.value=="PARTIAL"  ){
      $("#start").val($('#old_start').val());
      $("#success").val($('#old_success').val());
      
          if($.trim($('#start').val()) != ''){
            var start = parseInt($('#start').val());
          }

          if($.trim($('#success').val()) != '' ){
            var success = parseInt($('#success').val());
          }
     
      $("#start").prop('readonly', false);
      $("#remain").prop('readonly', true);
      $("#success").prop('readonly', false);
      $("#failed").prop('readonly', true);
      $("#end").prop('readonly', true);
      $("#end").val(start+success);
      $("#remain").val("0");
      $("#failed").val(qty-success);
    }
  }


  
  

  function ajax_action_set_status(){
    var id = $('#e_id').val();
    var id_member = $('#e_id_member').val();
    var status = $('#e_status').val();
    var price = $('#e_price').val();
    var price_product = $('#e_price_product').val();
    var no_transaction = $('#e_no_transaction').val();
    var start =  $("#start").val();
    var remain =  $("#remain").val();
    var success =  $("#success").val();
    var failed =  $("#failed").val();
    var end =  $("#end").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_action_set_status/",
              type:'POST',
              dataType: "json",
              data: { status:status,id:id,id_member:id_member,price:price,no_transaction:no_transaction,start:start,remain:remain,success:success,failed:failed,end:end,price_product:price_product },
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

</script>
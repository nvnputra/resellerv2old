<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller  {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_order");
      $this->load->library('session');
      $this->load->library('form_validation');
    }


    public function index()
    {
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product c',
                    'condition' => 'a.id_product = c.id',
                    'jointype' => ''
                )
            );
            $data['pending_order'] = $this->M_order->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_order->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_order->fetch_table("*","m__payment","status= 'PENDING' ");


            $data['count_complete'] = count($this->M_order->fetch_table("*","m__orders","status= 'COMPLETED' "));
            $data['count_all'] = count($this->M_order->fetch_table("*","m__orders",""));
            $data['count_cancel'] = count($this->M_order->fetch_table("*","m__orders","status= 'CANCELLED' "));
            $data['count_pending'] = count($this->M_order->fetch_table("*","m__orders","status= 'PENDING' or status= 'PROCESSING' or status= 'INPROGRESS' "));
            
            $data['content'] = 'order';
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function data_pending(){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product c',
                    'condition' => 'a.id_product = c.id',
                    'jointype' => ''
                )
            );
            $data['pending_order'] = $this->M_order->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_order->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_order->fetch_table("*","m__payment","status= 'PENDING' ");


            $data['count_complete'] = count($this->M_order->fetch_table("*","m__orders","status= 'COMPLETED' "));
            $data['count_all'] = count($this->M_order->fetch_table("*","m__orders",""));
            $data['count_cancel'] = count($this->M_order->fetch_table("*","m__orders","status= 'CANCELLED' "));
            $data['count_pending'] = count($this->M_order->fetch_table("*","m__orders","status= 'PENDING' or status= 'PROCESSING' or status= 'INPROGRESS' "));
            
            $data['content'] = 'order';
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function data_completed(){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product c',
                    'condition' => 'a.id_product = c.id',
                    'jointype' => ''
                )
            );
            $data['pending_order'] = $this->M_order->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_order->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_order->fetch_table("*","m__payment","status= 'PENDING' ");


            $data['count_complete'] = count($this->M_order->fetch_table("*","m__orders","status= 'COMPLETED' "));
            $data['count_all'] = count($this->M_order->fetch_table("*","m__orders",""));
            $data['count_cancel'] = count($this->M_order->fetch_table("*","m__orders","status= 'CANCELLED' "));
            $data['count_pending'] = count($this->M_order->fetch_table("*","m__orders","status= 'PENDING' or status= 'PROCESSING' or status= 'INPROGRESS' "));
            
            $data['content'] = 'order';
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function data_cancelled(){
        if(!empty($this->session->userdata('resv2.a_log_session'))){
            $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product c',
                    'condition' => 'a.id_product = c.id',
                    'jointype' => ''
                )
            );
            $data['pending_order'] = $this->M_order->fetch_table("*","m__orders","status= 'PENDING' ");
            $data['menu_post'] = $this->M_order->fetch_table("*","a__post_category","id_parent = '0'");
            $data['pending_payment'] = $this->M_order->fetch_table("*","m__payment","status= 'PENDING' ");


            $data['count_complete'] = count($this->M_order->fetch_table("*","m__orders","status= 'COMPLETED' "));
            $data['count_all'] = count($this->M_order->fetch_table("*","m__orders",""));
            $data['count_cancel'] = count($this->M_order->fetch_table("*","m__orders","status= 'CANCELLED' "));
            $data['count_pending'] = count($this->M_order->fetch_table("*","m__orders","status= 'PENDING' or status= 'PROCESSING' or status= 'INPROGRESS' "));
            
            $data['content'] = 'order';
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function ajax_list(){
         $column = 'a.*,b.username,c.name as product';
         $column_order = array(null, 'no_transaction','product_name','username','date','qty_order','total','target','status',null); //set column field database for datatable orderable
         $column_search = array('no_transaction','product_name','username','date','qty_order','total','target','status'); //set column field database for datatable searchable 
         $order = array('a.id' => 'DESC'); // default order 
         $table = "m__orders a";
         if (post("status")=="data_pending") {
             $where = "a.status = 'PENDING' or a.status = 'PROCESSING' or a.status = 'INPROGRESS' ";
         }else if (post("status")=="data_completed") {
             $where = "a.status = 'COMPLETED' ";
         }else if (post("status")=="data_cancelled") {
             $where = "a.status = 'CANCELLED' ";
         }else{
             $where = "";
         }
         $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product c',
                    'condition' => 'a.id_product = c.id',
                    'jointype' => ''
                )
            );

        $list = $this->M_order->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
       
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
           if($key->status=="PENDING"){
            $class = "btn-warning";
            }else if($key->status=="INPROGRESS"){
              $class = "btn-info";
            }else if($key->status=="PROCESSING"){
              $class = "btn-primary";
            }else if($key->status=="COMPLETED"){
              $class = "btn-success";
            }else if($key->status=="PARTIAL"){
              $class = "btn-default";
            }else if($key->status=="CANCELLED"){
              $class = "btn-danger";
            }
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->no_transaction;
            $row[] = $key->product_name;
            $row[] = $key->username;
            $row[] = $key->date;
            $row[] = $key->qty_order;
            $row[] = $key->total;
            $row[] = $key->target;
            $row[] = '<button onclick="ajax_get_status('.$key->id.')" class="btn btn-xs '.$class.'">'.$key->status.'</button>';
 
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_order->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_order->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }



    public function ajax_get_status(){
        $id = post("id");
        $joins = array(
                array(
                    'table' => 'm__member b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product c',
                    'condition' => 'a.id_product = c.id',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__product_api d',
                    'condition' => 'c.id_api = d.id',
                    'jointype' => ''
                )
            );
        $data = $this->M_order->fetch_joins("m__orders a","api_name,a.*,b.username,c.name as product",$joins,"a.id = '$id'",FALSE);
        echo json_encode($data);
    }

    public function ajax_action_set_status(){

            /*validasi Start count required*/

            if (post("status")!="CANCELLED" AND post("status")!="PENDING") {   
             
                $this->form_validation->set_rules('start', 'Qty Start count', 'required');
                

                if($this->form_validation->run()==FALSE){
                    $error = $this->form_validation->error_array();
                    $json_data =  array(
                        "result" => FALSE ,
                        "message" => array('head'=> 'Failed', 'body'=> 'Qty Start count is required'),
                        "form_error" => $error,
                        "redirect" => ''
                    );
                    print json_encode($json_data);
                    die();
                }
            }
            /*----------------------*/
            
            $id_member = post("id_member");
            $id = post("id");
            $get_status = $this->M_order->get_row("status","m__orders","id='$id'","","",FALSE);
            $is = "";
            $in = "";
            $status = post("status");

            /*validasi tidak merubah status*/
            if($status==$get_status->status){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Status Tidak Berubah'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

            /*----------------------*/

            /*set data update status order*/
              if($status=="INPROGRESS"){
                $is = "is_inprogress";
                $in = "inprogress_at";
              }else if($status=="PROCESSING"){
                $is = "is_processing";
                $in = "processing_at";
              }else if($status=="COMPLETED"){
                $is = "is_completed";
                $in = "completed_at";
              }else if($status=="PARTIAL"){
                $is = "is_partial";
                $in = "partial_at";
              }else if($status=="CANCELLED"){
                $is = "is_cancel";
                $in = "cancel_at";
              }

                if($status!="PENDING"){
                    $data = array(
                        $is => 1 ,
                        $in => date("Y-m-d H:i:s"),
                        "status" => $status,
                        "qty_startcount" => post("start"),
                        "qty_remain" => post("remain"),
                        "qty_success" => post("success"),
                        "qty_failed" => post("failed"),
                        "qty_endcount" => post("end"),
                        "action_by" => $this->session->userdata('resv2.a_id'),
                        "action_at" => date("Y-m-d H:i:s"),
                        "update_at" => date("Y-m-d H:i:s")
                    );
                }else{     
                    $data = array(
                        "status" => $status,
                        "qty_startcount" => post("start"),
                        "qty_remain" => post("remain"),
                        "qty_success" => post("success"),
                        "qty_failed" => post("failed"),
                        "qty_endcount" => post("end"),
                        "action_by" => $this->session->userdata('resv2.a_id'),
                        "action_at" => date("Y-m-d H:i:s"),
                        "update_at" => date("Y-m-d H:i:s")
                    );
                }
               /*----------------------*/

               /*set validasi data update balance ,mutasi, refaund*/
              if ((post("status")=="PROCESSING" || post("status")=="COMPLETED" || post("status")=="INPROGRESS" || post("status")=="PENDING") and ($get_status->status == "CANCELLED" or $get_status->status == "PARTIAL")) {
                $getid_mutation = $this->M_order->get_id_mutation($id); 
                $get_balance = $this->M_order->get_balance_mutation($id_member); 
                $delete_refaund = 1;
                $delete_mutation = $getid_mutation->id;
                //echo $delete_mutation; die();
                $set_refaund = null;
                $set_mutation = null;     
                $set_balance = array(
                    "balance" => $get_balance->balance-post('price')
                ); 

              }else if ($status=="CANCELLED" || $status=="PARTIAL" ){
                $get_balance = $this->M_order->get_balance_mutation($id_member); 
                $total = (post("price_product") *post("failed")) /1000;
                $delete_mutation = null;
                $delete_refaund = 1;

                $set_refaund = array(
                    "id_order"=>$id,
                    "id_user" =>$id_member,
                    "price" => post("price_product"),
                    "qty" => post("failed"),
                    "total" => $total,
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );

                $kredit = post('price')-$total;
                $set_mutation = array(
                    "id_user"=> $id_member,
                    "debt" => $total,
                    "balance" => $get_balance->balance + $total,
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
                $set_balance = array(
                    "balance" => $get_balance->balance + $total
                ); 
                 

              }else{
                $delete_mutation = null;
                $delete_refaund = null;
                $set_refaund = null;
                $set_balance = null;
                $set_mutation = null;
              }
              /*----------------------*/

            $data_notif = array(
              "id_user" => $id_member,
              "created_at" => date("Y-m-d H:i:s"),
              "text" => "Order ".post("no_transaction")." is ".post("status")
            );

            $edit = $this->M_order->update_balance($data,$id,$set_balance,$id_member,$data_notif,$set_mutation,$set_refaund,$delete_mutation,$delete_refaund);

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Status'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $this->send_email($id_member,post("no_transaction"),post("status"));
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit Status'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/order'
                );
                print json_encode($json_data);
            }
          
    }


    public function send_email($id_member,$no_transaction,$status){
        $this->load->library('ElasticEmail');
        $data_user = $this->M_order->get_row("*","m__member","id_member='$id_member' ","","",FALSE);

        $Elastic = new ElasticEmail();
        $admin = "admin@resellerindo.com";
        $from = $admin;
        $fromName = "resellerindo";
        $to = $data_user->email;
        $subject = "Order ".$no_transaction;
        $bodyText = ""; 
        $bodyHtml = 
        "<b>Hello ".$data_user->username." ,</b><br><br><br>
        Email Address : ".$data_user->email."<br>
        Thanks for order in Resellerindo!  <br>
        We're happy you're here. Your order ".$no_transaction." is ".$status." by admin resellerindo .
        <br><br><br>
        Thanks,<br>
        Admin Resellerindo
        "; 

        $send = $Elastic->send($from,$fromName,$subject,$to,$bodyText,$bodyHtml);
        $send = json_decode($send);
        
        return $send->success;
    }

    public function ajax_action_sycn(){
        $this->load->library('curl'); 

        /*get api data*/
        $api_name = post('api_name');
        $get_api = $this->M_order->get_row("*","a__product_api","api_name='$api_name' ","","",FALSE);
        $params = array(
            "key" => $get_api->api_key,
            "request" => 'status',
            "supplier" => post("api_name"),
            "idorder" => post("id_auto")
        );

        
        $get_lookup = $this->curl->simple_post($get_api->api_url, $params, array(CURLOPT_BUFFERSIZE => 10));
        $arr_data = json_decode($get_lookup);
        /*validasi data api false*/
        if($arr_data->result==false){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Sycn'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();            
        }else{
            $lastcheck_sync = date("Y-m-d H:i:s");
            $data_update = array(
                "lastcheck_sync" => $lastcheck_sync
            );
            $update = $this->M_order->update_table("m__orders",$data_update,"id",post("id"));
                $set_data = array(
                    "start_count" => $arr_data->data->start_count,
                    "status" => $arr_data->data->status, 
                    "remains" => $arr_data->data->remains,
                    "lastcheck_sync" => $lastcheck_sync
                );
            echo json_encode($set_data);
        }
    }

    public function ajax_action_issycn(){
        $id = post("id");
        $is_sycn = post("is_sycn");

        $data = array(
                "is_sync" => $is_sycn
            );

        $edit = $this->M_order->update_table("m__orders",$data,"id",$id);

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Failed Update'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Success Update'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
            }

    }

    
  
        
}

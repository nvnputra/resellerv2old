<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_mutation extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }


  public function show_mutation($id_member){
    $query = $this->db->query("SELECT
        `a`.*, `b`.`status` AS `status_order`,
        `c`.`status` AS `status_payment`,
        `b`.`no_transaction` AS `no_order`,
        `c`.`no_transaction` AS `no_payment`,
        `e`.no_transaction AS `no_refund`
      FROM
        `m__mutation` `a`
      LEFT JOIN `m__orders` `b` ON `a`.`id_order` = `b`.`id`
      LEFT JOIN `m__payment` `c` ON `a`.`id_payment` = `c`.`id`
      LEFT JOIN `m__orders_refund` `d` ON `a`.`id_order_refund` = `d`.`id`
      LEFT JOIN `m__orders` `e` ON `e`.`id` = `d`.`id_order`
      WHERE
        `a`.`id_user` = '$id_member'
      ORDER BY
        a.created_at ");
    $result = $query->result();
    return $result;
     
  }
  
  
}
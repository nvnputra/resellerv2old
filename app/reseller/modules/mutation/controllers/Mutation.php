<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutation extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_mutation");
      $this->load->model("notif/M_notif");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
         	if(!empty($this->session->userdata('resv2.m_log_session'))){
    	   		$id_member = $this->session->userdata('resv2.m_id');
    	   		$data['data_user'] = $this->M_mutation->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
                if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
                }
                $data['data_profile'] = $this->M_mutation->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
                $data['data_notif'] = $this->M_notif->limit_notif($id_member);
                $data['notif_order'] = $this->M_notif->notif_order($id_member);
                $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
                $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
    	   		$data['content'] = 'mutation';
    	   		$joins = array(
                array(
                    'table' => 'm__orders b',
                    'condition' => 'a.id_order = b.id',
                    'jointype' => 'LEFT'
                ),
                array(
                    'table' => 'm__payment c',
                    'condition' => 'a.id_payment = c.id',
                    'jointype' => 'LEFT'
                )
            );

            $data['mutation'] = $this->M_mutation->show_mutation($id_member);
            	$this->load->view('login/home',$data);
	        }else{
	            redirect("login/"); 
	        }
    }

}

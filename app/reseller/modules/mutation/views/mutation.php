<div class="row">
  <div class="col-md-12">
    <div class="card-header bg-success" style="color: white;">
      <b> Mutation </b> 
      <span style="float: right; cursor: pointer;">
        <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
      </span>
    </div>
    <table class="table">
      <thead>
        <th>Type</th>
        <th>No Transaction</th>
        <th>Date</th>
        <th>Status</th>
        <th>Debet</th>
        <th>Credit</th>
        <th>Balance</th>
      </thead>
      <tbody>
        <?php foreach ($mutation as $key) { ?>
        <tr>
          <td align="centeer"><?php if($key->id_order!=""){ echo "Order"; }else if($key->id_payment!=""){ echo "Payment"; }else{ echo "Refund";} ?></td>
          <td align="centeer"><?php if($key->id_order!=""){ echo $key->no_order; }else if($key->id_payment!=""){ echo $key->no_payment; }else{ echo $key->no_refund;} ?></td>
          <td align="centeer"><?php echo $key->created_at; ?></td>
          <td align="centeer"><b><?php if($key->id_order!=""){ echo $key->status_order; }else if($key->id_payment!=""){ echo $key->status_payment; }else{ echo "REFUND";} ?></b></td>
          <td align="centeer">Rp. <?php echo number_format($key->debt,0,'.','.'); ?></td>
          <td align="centeer">Rp. <?php echo number_format($key->credit,0,'.','.'); ?></td>
          <td align="centeer">Rp. <?php echo number_format($key->balance,0,'.','.'); ?></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div>
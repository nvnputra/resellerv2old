<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_login extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }

  function register($data,$detail){
    $this->db->trans_begin();

    $this->db->insert('m__member', $data);
    $get_id = $this->db->insert_id();

    $array_id = array(
      "id_member" => $get_id
    );

    $new_detail = array_merge($array_id,$detail);
    $this->db->insert('m__member_detail', $new_detail);

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }


  }


  function limit_order($id_member){
    $query = $this->db->query("select * from m__orders where id_member = '$id_member' order by id desc limit 3");
    return $query->result();
  }

  function last_news()
  {
    $query = $this->db->query("select * from a__post where id_category = '1' order by id_post desc limit 1");
    return $query->row();
  }

  function limit_news(){
    $query = $this->db->query("select * from a__post where id_category = '1' order by id_post desc limit 3");
    return $query->result();
  }

  function limit_payment($id_member){
    $query = $this->db->query("SELECT `a`.*, `d`.`name` as `member_account`, `e`.`name` as `admin_account` FROM `m__payment` `a` JOIN `m__payment_account` `b` ON `b`.`id_payment_account` = `a`.`m_id_paymentaccount` JOIN `a__payment_account` `c` ON `c`.`id_payment_account` = `a`.`a_id_paymentaccount` JOIN `a__payment_method` `d` ON `d`.`id`= `b`.`id_payment_method` JOIN `a__payment_method` `e` ON `e`.`id` = `c`.`id_payment_method` WHERE `a`.`id_member` = '$id_member' order by a.id desc LIMIT 3");
    return $query->result();
  }
  
  
}
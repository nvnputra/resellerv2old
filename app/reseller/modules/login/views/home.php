<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Followersindo</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/theme.css" rel="stylesheet" media="all">
    <!-- RESV2 CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/resv2-standard.css" rel="stylesheet" media="all">

    <style type="text/css">
        .hide-menu{
            display: none;
        }
    </style>
</head>

<body class="resv2-standard reseller-page">
<div id="page-load" style="position: fixed; z-index: 999999; opacity: 0.7;  background: white; width: 100%; height: 100%; display: block;  ">    <div class="page-loader__spin">
        
    </div>
</div>
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="<?=base_url().$this->config->item('index_page'); ?>/login/home">
                            <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="Followersindo" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <?php $hidden=""; if($data_user->is_premium!="1"){ 
                    if (date("Y-m-d H:is")>$data_user->trial_expired) {
                        $hidden = "hide-menu";    
                    } 
                }?>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub <?php if($this->uri->segment(1)=="login"){ echo "active"; } echo $hidden; ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/login/home/">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="<?php if($this->uri->segment(1)=="profile" or $this->uri->segment(1)=="payment_account"){ echo "active"; } echo $hidden; ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/profile">
                                <i class="fas fa-user"></i>Profile</a>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="payment"){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-credit-card"></i>Payment
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                               <li class="<?php if($this->uri->segment(1)=="payment" and $this->uri->segment(2)=="topup"){ echo "active"; } echo $hidden; ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment/topup/">Topup</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="payment" and $this->uri->segment(2)=="upgrade"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment/upgrade/">Upgrade</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="order"){ echo "active"; } echo $hidden; ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-shopping-cart"></i>Order
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li class="<?php if($this->uri->segment(1)=="order" and $this->uri->segment(2)==""){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/order">New Order</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="order" and $this->uri->segment(2)=="order_history"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/order/order_history">Order History</a>
                                </li>
                            </ul>
                        </li>
                        <li class="<?php if($this->uri->segment(1)=="mutation"){ echo "active"; } echo $hidden; ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/mutation">
                                <i class="fas fa-dollar"></i>Mutation</a>
                        </li>

                        <li class="has-sub <?php if($this->uri->segment(1)=="post" ){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Post
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <?php foreach ($menu_post as $menu) { ?>
                                <li class="<?php if($this->input->get('data')==strtolower($menu->name)){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/post?data=<?php echo strtolower($menu->name); ?>&id=<?php echo $menu->id_category; ?>"><?php echo $menu->name; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        
                   </ul>     
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?=base_url().$this->config->item('index_page'); ?>/login/home">
                    <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="Followersindo" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub  <?php if($this->uri->segment(1)=="login"){ echo "active"; } ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/login/home">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="<?php if($this->uri->segment(1)=="profile" or $this->uri->segment(1)=="payment_account"){ echo "active"; } echo $hidden; ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/profile">
                                <i class="fas fa-user"></i>Profile</a>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="payment"){ echo "active"; } ?>">
                            <a class="js-arrow " href="#">
                                <i class="fas fa-credit-card"></i>Payment
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="<?php if($this->uri->segment(1)=="payment" and $this->uri->segment(2)=="topup"){ echo "active"; } echo $hidden; ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment/topup/">Topup</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="payment" and $this->uri->segment(2)=="upgrade"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment/upgrade/">Upgrade</a>
                                </li>
                                 <li class="<?php if($this->uri->segment(1)=="payment" and $this->uri->segment(2)=="payment_history"){ echo "active"; } echo $hidden; ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment/payment_history/">Payment History</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="order"){ echo "active"; } echo $hidden; ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-shopping-cart"></i>Order
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="<?php if($this->uri->segment(1)=="order" and $this->uri->segment(2)==""){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/order">New Order</a>
                                </li>
                                <li class="<?php if($this->uri->segment(1)=="order" and $this->uri->segment(2)=="order_history"){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/order/order_history">Order History</a>
                                </li>
                            </ul>
                        </li>
                       <li class="<?php if($this->uri->segment(1)=="mutation"){ echo "active"; } echo $hidden; ?>">
                            <a href="<?=base_url().$this->config->item('index_page'); ?>/mutation">
                                <i class="fas fa-dollar"></i>Mutation</a>
                        </li>
                        <li class="has-sub <?php if($this->uri->segment(1)=="post" ){ echo "active"; } ?>">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Post
                                <i class="fas fa-sort-down arrow"></i>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <?php foreach ($menu_post as $menu_post) { ?>
                                <li class="<?php if($this->input->get('data')==strtolower($menu_post->name)){ echo "active"; } ?>">
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/post?data=<?php echo strtolower($menu_post->name); ?>&id=<?php echo $menu_post->id_category; ?>"><?php echo $menu_post->name; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                <?php if($data_user->is_premium!="1"){
                                    $dStart = new DateTime(date("Y-m-d H:i:s"));
                                    $dEnd  = new DateTime($data_user->trial_expired);
                                    $dDiff = $dStart->diff($dEnd);
                                    if ($dDiff->format('%R')=="-") {
                                        $text = "Your account is expired.";
                                    }else{
                                        $text = "Your account is expired ".$dDiff->days." days left.";
                                    }
                                    ?><a href="<?=base_url().$this->config->item('index_page'); ?>/payment/upgrade/"><span class="badge badge-warning"><h5><?php echo $text; ?></h5></span></a><?php
                                }else{
                                    ?>
                                    <span class="badge badge-success"><h5>Your Account Is Premium</h5></span>
                                    <?php
                                } ?>
                                <!-- <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button> -->
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-shopping-cart"></i>
                                        <?php if(count($notif_order)>0){ ?><span class="quantity"><?php echo count($notif_order); ?></span><?php }?>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have <?php echo count($notif_order) ?> Order Pending</p>
                                            </div>
                                            <?php foreach ($notif_order as $key_order) { ?>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-shopping-cart"></i>
                                                </div>
                                                <div class="content">
                                                    <p><?php echo $key_order->no_transaction; ?></p>
                                                    <span class="date"><?php echo $key_order->created_at; ?></span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="notifi__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/order/order_history/">All order</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-card"></i>
                                        <?php if(count($notif_payment)>0){ ?><span class="quantity"><?php echo count($notif_payment); ?></span><?php } ?>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have <?php echo count($notif_payment) ?> Payment Pending</p>
                                            </div>
                                            <?php foreach ($notif_payment as $key_payment) { ?>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-card"></i>
                                                </div>
                                                <div class="content">
                                                    <p><?php echo $key_payment->no_transaction; ?></p>
                                                    <span class="date"><?php echo $key_payment->created_at; ?></span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="notifi__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/payment/payment_history/">All payment</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div <?php if(count($data_notif)>0){ ?> onclick="ajax_action_read_notif()" <?php } ?> class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <?php if(count($data_notif)>0){ ?><span id="count_notif" class="quantity"><?php echo count($data_notif) ?></span><?php } ?>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have <?php echo count($data_notif) ?> Notifications</p>
                                            </div>
                                            <?php foreach ($data_notif as $key_notif) { ?>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p><?php echo $key_notif->text; ?></p>
                                                    <span class="date"><?php echo $key_notif->created_at; ?></span>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="notifi__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/notif/">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <a onclick="window.location = '<?=base_url().$this->config->item('index_page'); ?>/payment/topup/'"><?php echo "Rp. ".number_format($data_user->balance); ?></a>
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <?php 
                                            $src = "";
                                            if($data_profile===NULL){ $src = base_url().'uploads/profile/profile/profile-default.png'; }else{ $src = base_url().'uploads/profile/profile/'.$data_profile->profile_image; }  ?>
                                            <img class="" src="<?php echo $src; ?>" >
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php echo $data_user->username; ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="account-dropdown__footer">
                                                <a href="<?=base_url().$this->config->item('index_page'); ?>/login/logout/">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                              <div class="au-card">
                                <?php  $this->load->view($content); ?>
                              </div>
                            </div>        
                        </div>
                      
                        
                    </div>
                </div>
            </div>

            
        </div>

    </div>

    <!-- Modal File Proof -->
    <div class="modal fade" id="modal_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 style="color: white;" class="modal-title" id="myModalLabel"><center>File Proof</span></center></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <img width="100%" src="" id="show_img_proof" alt="" class="img-rounded">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Popup -->
    <div class="modal fade" id="modal_news" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 id="news_subject" style="color: white;" class="modal-title" id="myModalLabel"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body" id="news_body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <?php 
          $ajax_list = "";
          if($this->uri->segment(1)=="order" and $this->uri->segment(2)=="order_history" ){ 
            $ajax_list = "ajax_list_history();";
          }else if($this->uri->segment(1)=="payment" and $this->uri->segment(2)=="payment_history" ){ 
            $ajax_list = "ajax_list_payment();";
          }else if ($this->uri->segment(1)=="payment_account") {
            $ajax_list = "ajax_list_payment_acc();";
            $data_method = $method;
            ?>
<div class="modal fade" id="modal_tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="post" onsubmit="return ajax_action_add_account();">
      <div class="modal-header bg-primary">
        <h4 class="modal-title" style="color: white;" id="myModalLabel"><center>New Payment Account</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
      </div>
      <div class="modal-body">
        <table class="table">
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="method" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" name="account_name" class="form-control" id="account_name">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" name="account_number" class="form-control" id="account_number">
            </td>
            <td style="display: none;">
              <label>Account Branch</label><br>
              <input type="hidden" name="account_branch" class="form-control"  id="account_branch">
            </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!------------ ---------------->
<!-- Modal -->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="post" onsubmit="return ajax_action_edit_account();">
      <div class="modal-header bg-success">
        <h4 class="modal-title" style="color: white;" id="myModalLabel"><center>Edit Payment Account</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id" name="id">
        <table class="table">
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="e_method" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" name="account_name" class="form-control" id="e_account_name">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" name="account_number" class="form-control" id="e_account_number">
            </td>
            <td style="display: none;">
              <label>Account Branch</label><br>
              <input type="text" name="account_branch" class="form-control" id="e_account_branch">
            </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!------------ ---------------->

            <?php
          }else if ($this->uri->segment(1)=="profile") {
?>
<div class="modal fade" id="modal_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content ">
      <form onsubmit="return ajax_action_change_password();" method="post" >
      <div class="modal-header btn-warning">
        <h4 style="color:white;" class="modal-title" id="myModalLabel"><center>Change Password</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body ">
        <table class="table">
          <tr>
            <td>
              <label>Old Password</label>
              <input type="password" name="old_password" id="old_password" class="form-control" required="">
            </td>
          </tr>
          <tr>
            <td>
              <label>New Password</label>
              <input type="password" name="new_password" id="new_password" class="form-control" required="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Re Password</label>
              <input type="password" name="re_password" id="re_password" class="form-control" required="">
            </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-warning">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!------------ ---------------->
<div class="modal fade" id="modal_upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content ">
      <form >
      <div class="modal-header btn-primary">
        <h4 style="color:white;" class="modal-title" id="myModalLabel"><center>Upload Image Background</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body ">
        <table class="table">
          <tr>
            <td>
              <label>Image</label>
              <input type="file" name="image1" id="image1" class="form-control">
            </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button"  onclick="ajax_action_upload_bg()" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!------------ ---------------->
<div class="modal fade" id="modal_profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content ">
      <form >
      <div class="modal-header btn-primary">
        <h4 style="color:white;" class="modal-title" id="myModalLabel"><center>Upload Image Profile</center></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body ">
        <table class="table">
          <tr>
            <td>
              <label>Image</label>
              <input type="file" name="image2" id="image2" class="form-control">
            </td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button"  onclick="ajax_action_upload_profile()" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!------------ ---------------->

<?php
          }
if($this->input->get("news")!=""){ $p_news = $this->input->get("news"); }else{ $p_news = "''"; }
?>

    <!-- Jquery JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
    <script type="text/javascript">
      var popup_news = <?php echo $p_news; ?>;
        if (popup_news!="") {
          show_popup_news(popup_news);  
        }

      $('.select2').select2();
      var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
        $.ajaxSetup({
        data: csfrData
      });

        function show_popup_news(id) {
            $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/login/show_popup_news/",
              type:'POST',
              dataType: "json",
              data: { id:id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' },
              beforeSend: function () {
                      $('#page-load').show();
              },
              success: function(data) {
                var i ;
                for(i = 0; i<data.length; i++){
                  $("#news_subject").html(data[i].subject);
                  $("#news_body").html(data[i].body);
                }
                
                $('#page-load').hide();
                $('#modal_news').modal('show');
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
        }

        function show_file_proof(img){
            document.getElementById("show_img_proof").src = "<?php echo base_url(); ?>uploads/file_proof/"+img;
                        
            $('#modal_img').modal('show');
          }

        function ajax_action_read_notif() {
          $.ajax({
                    url: "<?php echo base_url().$this->config->item('index_page'); ?>/notif/ajax_action_read_notif/",
                    method:'POST',
                    dataType: "json",
                    data: {},
                    beforeSend: function () {
                      /*$('#page-load').loading({
                          overlay: $('#page-load')
                        });*/
                    },
                    success: function(data) {
                        $('#count_notif').html('0');
                      //$('#page-load').loading('stop');
                      console.log(data.message.body);
                    },error: function(request, status, error){
                          //$('#page-load').loading('stop');
                          toastr["error"]("Error, Please try again later");
                      }
                });
        }

        function ajax_list_history(){
            $('#list_history').DataTable({ 
                "responsive": true,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_list_history/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                               
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                     //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
        }

        function ajax_list_payment(){
            $('#list_payment').DataTable({ 
                "responsive": true,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_list_payment/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                               
                },

                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
          }

         function ajax_list_payment_acc(){
            $('#list_payment_acc').DataTable({ 
         
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_list_payment_acc/",
                    "type": "POST",
                    "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                               
                },
         
                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0 ], //first column / numbering column
                    "orderable": false, //set not orderable
                },
                ],
         
            });
          }

    
    <?php echo $ajax_list; ?>
    </script>
</body>

</html>
<!-- end document-->

    
</body>

</html>
<script type="text/javascript">
        $(document).ready(function(){
            $('#page-load').hide();
        });
    </script>
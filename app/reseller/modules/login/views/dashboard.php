<div class="row m-t-25">
							<div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-money"></i>
                                            </div>
                                            <div class="text">
                                                <h2>Rp. <?php echo number_format($data_user->balance,0,'.','.'); ?></h2>
                                                <span>Total Saldo</span><br><br>
                                            </div>
                                        </div>
                                        <!-- <div class="overview-chart"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                            <canvas id="widgetChart3" height="115" width="187" class="chartjs-render-monitor" style="display: block; width: 187px; height: 115px;"></canvas>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-shopping-cart"></i>
                                            </div>
                                            <div class="text">
                                                <h2>Rp. <?php echo number_format($total_pesanan->total_pesanan,0,'.','.'); ?></h2>
                                                <span>completed order</span><br><br>
                                            </div>
                                        </div>
                                       <!--  <div class="overview-chart"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                            <canvas id="widgetChart2" height="115" width="187" class="chartjs-render-monitor" style="display: block; width: 187px; height: 115px;"></canvas>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c4">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-card"></i>
                                            </div>
                                            <div class="text">
                                                <h2>Rp. <?php echo number_format($invoice->invoice,0,'.','.'); ?></h2>
                                                <span>total payment paid</span><br><br>
                                            </div>
                                        </div>
                                        <!-- <div class="overview-chart"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                            <canvas id="widgetChart4" height="115" width="187" class="chartjs-render-monitor" style="display: block;"></canvas>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
 
<div class="col-md-12">
		<div class="card-header bg-info" style="color: white;">
			<b> 3 Last News </b> 
		</div>
		<br>
		<div class="table-responsive table--no-card m-b-30">
		<table class="table table-borderless table-striped ">
			<tbody>
			<?php 
			if(count($data_news)<1){
				?><tr><td bgcolor="#EEE" align="center" >Data Kosong</td></tr><?php
			}else{
				foreach ($data_news as $news) { ?>
				<tr>
					<td><li><b><?php echo $news->subject; ?></b></li></td>
				</tr>
				<tr>
					<td><?php echo $news->body; ?></td>
				</tr>
			<?php } }?>
			</tbody>
		</table>
		</div>
	</div>
<div class="col-md-12">
		<div class="card-header bg-success" style="color: white;">
			<b> 3 Last Payment </b> 
		</div>
		<div class="table-responsive table--no-card m-b-30">
		<table class="table table-borderless table-striped ">
			<thead>
				<tr>
					  <th><center>No Transaction</center></th>
			          <th><center>Date</center></th>
			          <th><center>Type</center></th>
			          <th><center>Member Account</center></th>
			          <th><center>Admin Account</center></th>
			          <th><center>Nominal Transfer</center></th>
			          <th><center>Nominal Topup</center></th>
			          <th><center>File Proof</center></th>
			          <th><center>Status</center></th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(count($data_payment)<1){
				?><tr><td bgcolor="#EEE" align="center" colspan="9">Data Kosong</td></tr><?php
			}else{
				foreach ($data_payment as $key) { 
				if($key->status=="PENDING"){
	                $clas = "badge-warning";
	              }else if($key->status=="PAID"){
	                $clas = "badge-success";
	              }else if($key->status=="FRAUD"){
	                $clas = "badge-info";
	              }else if($key->status=="CANCEL"){
	                $clas = "badge-danger";
	              }else if($key->status=="WAITING"){
	              	$clas = "badge-info";
	              }?>
				<tr>
					<td><?php echo $key->no_transaction; ?></td>
		            <td><?php echo $key->date; ?></td>
		            <td><?php echo $key->type; ?></td>
		            <td><?php echo $key->member_account; ?></td>
		            <td><?php echo $key->admin_account; ?></td>
		            <td><?php echo $key->nominal_transfer; ?></td>
		            <td><?php echo $key->nominal_topup; ?></td>
		            <td><a href="#" onclick="show_file_proof('<?php echo $key->file_proof; ?>')">view</a></td>
		            <td><span class="badge <?php echo $clas; ?> "><?php echo $key->status; ?></span></td>
				</tr>
			<?php } }?>
			</tbody>
		</table>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card-header bg-primary" style="color: white;">
			<b> 3 Last Order </b> 
		</div>
		<div class="table-responsive table--no-card m-b-30">
		<table class="table table-borderless table-striped ">
			<thead>
				<tr>
					<th><center>No Transaction</center></th>
					<th><center>Data</center></th>
					<th><center>Qty</center></th>
					<th><center>Status</center></th>
					<th><center>Price</center></th>
					<th><center>Product</center></th>
					<th><center>Date</center></th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(count($data_order)<1){
				?><tr><td bgcolor="#EEE" align="center" colspan="7">Data Kosong</td></tr><?php
			}else{
			foreach ($data_order as $order) {  ?>
				<tr>
					<td align="center"><?php echo $order->no_transaction; ?></td>
					<td align="center"><?php echo $order->target; ?></td>
					<td align="center">
						<?php if($order->status=="PENDING"){
				                $class = "badge-warning";
				              }else if($order->status=="INPROGRESS"){
				                $class = "badge-info";
				              }else if($order->status=="PROCCESSING"){
				                $class = "badge-primary";
				              }else if($order->status=="COMPLETED"){
				                $class = "badge-success";
				              }else if($order->status=="PARTIAL"){
				                $class = "badge-default";
				              }else if($order->status=="CANCELLED"){
				                $class = "badge-danger";
				              }
				        echo '<span class="badge '.$class.'">'.$order->status.'</span>'; ?>		
					</td>
					<td align="center"><?php echo $order->no_transaction; ?></td>
					<td align="center"><?php echo $order->total; ?></td>
					<td align="center"><?php echo $order->product_name; ?></td>
					<td align="center"><?php echo $order->date; ?></td>
				</tr>
			<?php } } ?>
			</tbody>
		</table>
	</div>
	</div>
</div>
 </div>

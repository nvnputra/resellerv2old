<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Register</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.css" rel="stylesheet" media="all">


    <!-- Main CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/theme.css" rel="stylesheet" media="all">

</head>
<body class="animsition" style="background: #eee;">
<div id="page-load" style="position: absolute; z-index: 1500; opacity: 0.7;  background: white; width: 100%; height: 100%; display: none;  ">
    <div class="page-loader__spin">
        
    </div>
</div>
      <div class="container" style="padding-top: 2%;">
                <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="login-content">
                        <div class="login-logo">
                            <h2>
                               <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="CoolAdmin" />
                            </h2>
                        </div>
                        <div class="login-form">
                            <form method="post" onsubmit="return ajax_action_register();">
                                <table class="table" id="form1" >
						          <tr>
						            <td>
						              <label>Email :</label>
						              <input type="text" name="email" id="email" class="form-control" required="">
						            </td>
						            <td>
						              <label>Username :</label>
						              <input type="text" name="username" id="username" class="form-control" required="">
						            </td>
						          </tr>
						          <tr>
						            <td>
						              <label>Password :</label>
						              <input type="password" name="password" id="password" class="form-control" required="">
						            </td>
						            <td>
						              <label>Re Password :</label>
						              <input type="password" name="repassword" id="repassword" class="form-control" required="">
						            </td>
						          </tr>
						          <tr>
						            <td>
						              <label>First Name :</label>
						              <input type="text" name="first" id="first" class="form-control" required="">
						            </td>
						            <td>
						              <label>Last Name :</label>
						              <input type="text" name="last" id="last" class="form-control" required="">
						            </td>
						          </tr>
						          <tr>
						            <td>
						              <label>Birth date :</label>
						              <input type="date" name="birth_date" id="birth_date" class="form-control" required="">
						            </td>
						            <td>
						              <label>Gender :</label><br>
						              <input type="radio" name="jk" id="jk1" value="L"> Male
						              <input type="radio" name="jk" id="jk2" value="P"> Female
						            </td>
						          </tr>
						          <tr>
						            <td>
						              <label>Phone :</label>
						              <input type="number" name="phone" id="phone" class="form-control" required="">
						            </td>
						            <td>
						              <label>Address :</label>
						              <textarea name="address" id="address" rows="2" class="form-control" required=""></textarea>
						            </td>
						          </tr>						          
						        </table>
                                <button  class="au-btn au-btn--block au-btn--blue m-b-20" type="submit">sign up</button>
                                <button onclick="window.location='<?=base_url().$this->config->item('index_page'); ?>/login/'" class="au-btn au-btn--block au-btn--green m-b-20" type="button">sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                </div>
                </div>
            </div>
  

    <!-- Jquery JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.js"></script>
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>

    <script type="text/javascript">
      var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo
        $this->security->get_csrf_hash(); ?>';
        $.ajaxSetup({
        data: csfrData
      });
      toastr.options.timeOut = 1000; // How long the toast will display without user interaction

   function ajax_action_register() {
          
          var username = $("#username").val();
          var password = $("#password").val();
          var email = $("#email").val();  
		      var repassword = $("#repassword").val();
        
          if (document.getElementById('jk1').checked) {
            var jk = document.getElementById('jk1').value;
          }else if (document.getElementById('jk2').checked){
            var jk = document.getElementById('jk2').value;
          }else{
            var jk = "";
          }

          var first = $("#first").val();
          var last = $("#last").val();
          var birth_date = $("#birth_date").val();
          var phone = $("#phone").val();
          var address = $("#address").val();

          $.ajax({
                    url: "<?php echo base_url(); ?>reseller.php/login/ajax_action_register/",
                    type:'POST',
                    dataType: "json",
                    data: {username:username,password:password, email:email,first:first,last:last, birth_date:birth_date,phone:phone, address:address,jk:jk,repassword:repassword,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo
          $this->security->get_csrf_hash(); ?>'},
                    beforeSend: function () {
	                  $('#page-load').show();
	              	},success: function(data) {
	              		$('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }
                    },error: function(request, status, error){
                        $('#page-load').hide();
                        toastr["error"]("Error, Please try again later");
                    }
                }); 
          
          return false;
        }
    
    </script>

</body>

</html>
<!-- end document-->
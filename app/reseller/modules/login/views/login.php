<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.css" rel="stylesheet" media="all">


    <!-- Main CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/theme.css" rel="stylesheet" media="all">

</head>
<body class="animsition">
<div id="page-load" style="position: absolute; z-index: 1500; opacity: 0.7;  background: white; width: 100%; height: 100%; display: none;  ">
    <div class="page-loader__spin">
        
    </div>
</div>
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <h2>
                               <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="CoolAdmin" />
                            </h2>
                        </div>
                        <div class="login-form">
                            <form method="post" onsubmit="return ajax_action_login();">
                                <div class="form-group">
                                    <label>Username or email</label>
                                    <input class="au-input au-input--full" type="text" name="username" id="username"  placeholder="Username or email" required >
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" id="password" name="password" placeholder="Password" required >
                                </div>
                                <div class="login-checkbox">
                                  <label>
                                        <a data-toggle="modal" data-target="#modal_forgot" href="#">Forgotten Password?</a>
                                  </label>
                                </div>
                                <button id="btn-login" class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                                
                            </form>
                            <div id="alert_login">
                            </div>
                            <hr>
                            <div class="register-link">
                                <p>
                                    Don't you have account?
                                    <a href="<?=base_url().$this->config->item('index_page'); ?>/login/register">Sign Up Here</a>
                                    <br><br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Forgot -->
<div class="modal fade" id="modal_forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form onsubmit="return show_forgot_password();">
      <div class="modal-header bg-danger">
          <h4 style="color: white;" class="modal-title" id="myModalLabel"><center>Forgot Password</span></center></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
      </div>
      <div class="modal-body">
        <div id="form-password1">
          <label>Email :</label>
          <input type="text" class="form-control" id="email_forgot" name="">
        </div>
        <div id="form-password2" style="display: none;">
          <p>Send Link reset password to email <b id="span_email"></b> </p><br>
          <button style="width: 49%;" type="button" onclick="back_forgot()" class="btn btn-default">Back</button>
          <button style="width: 50%;" type="button" onclick="ajax_action_mail_password()" class="btn btn-danger">Send</button>
        </div>
      </div>
      <div class="modal-footer">
        <div id="form-btn1">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Next</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.js"></script>
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>

    <script type="text/javascript">
      var csfrData = {};
        csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo
        $this->security->get_csrf_hash(); ?>';
        $.ajaxSetup({
        data: csfrData
      });
      toastr.options.timeOut = 2000; // How long the toast will display without user interaction

      function ajax_action_mail_password(){
      var mail = $('#email_forgot').val();
       $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/profile/ajax_action_mail_password",
              type:'POST',
              dataType: "json",
              data: {email:mail},
              beforeSend: function () {
                  $('#page-load').show();
              },
              success: function(data) {
                       $('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }   
              },error: function(request, status, error){
                 $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
    }

    function show_forgot_password(){
      var mail = $('#email_forgot').val();
      if(mail =="" ){
        toastr["error"]('Email is required');
      }else{
        $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/login/cek_email/",
              type:'POST',
              dataType: "json",
              data: {email:mail},
              beforeSend: function () {
                  $('#page-load').show();
              },
              success: function(data) {
                 $('#page-load').hide();
                if(data>0){
                  $('#span_email').html(mail);
                  $('#form-password1').hide();
                  $('#form-password2').show();
                  $('#form-btn1').hide();
                }else{
                  toastr["error"]("Email Not register");
                }      
              },error: function(request, status, error){
                 $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
      }

      return false;
    }

    function back_forgot(){
        $('#form-password1').show();
        $('#form-password2').hide();
        $('#form-btn1').show();
    }

    function ajax_action_login() {
    var username = $('#username').val();
    var password = $('#password').val();
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/login/ajax_action_login/",
              type:'POST',
              dataType: "json",
              data: {username:username,password:password, 
                  <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>' },
              beforeSend: function () {
                  $('#page-load').show();
              },success: function(data) {
                 $('#page-load').hide();
                  if(data.result){
                    toastr["success"](data.message.body);
                    setTimeout(function(){window.location = data.redirect},1000);
                  }else{
                    toastr["error"](data.message.body);
                  }
              },error: function(request, status, error){
                 $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
      });
    return false;
    
  }
    
    </script>

</body>

</html>
<!-- end document-->
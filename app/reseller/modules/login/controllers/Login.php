<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_login");
      $this->load->model("notif/M_notif");
	  $this->load->library('session');
	  $this->load->library(array('session','pagination','form_validation'));
      
    }


	public function index()
	{
        $data['jobs'] = $this->M_login->fetch_table("*","sys__jobs","");
        $data['educations'] = $this->M_login->fetch_table("*","sys__educations","");
        $data['prov'] = $this->M_login->fetch_table("*","sys__provinces","");
        $data['regency'] = $this->M_login->fetch_table("*","sys__regencies","");
		
        if(!empty($this->session->userdata('resv2.m_log_session'))){
           return $this->home();
        }else{
            $this->load->view('login',$data);
        }
 
        
    }

    public function register(){
        $this->load->view('register');
    }

    public function register_information(){
        $this->load->view('register_information');
    }

    

    public function ajax_action_register(){
        
        $this->form_validation->set_rules('first', 'First Name', 'required');
        $this->form_validation->set_rules('last', 'Last Name', 'required');
        $this->form_validation->set_rules('birth_date', 'Birth date', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');
        $this->form_validation->set_rules('jk', 'gender', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('repassword', 'repassword', 'required');
        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

            $cek_username = count($this->M_login->fetch_table("id_member","m__member","  username = '".post('username')."' ")); 
            $cek_email = count($this->M_login->fetch_table("id_member","m__member"," email = '".post('email')."' "));
           
            if ($cek_email>0) {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Email has been used by another user'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

            if ($cek_username>0) {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Username has been used by another user'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }
            if(post('password')!=post('repassword')){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Password not same'),
                    "form_error" => '',
                    "redirect" => ''
                );

                print json_encode($json_data);
                die();
            }        
            
        $active_at = date("Y-m-d H:i:s");
        $get_trial = $this->M_login->get_row("value","sys__config","param='user-trial-day' ","","",FALSE);
        $plus_trial = $get_trial->value;
        $trial_day = date('Y-m-d H:i:s', strtotime($active_at . '+'.$plus_trial.' day'));

        $data = array(
            "email" => post("email"),
            "username" => post("username"),
            "password" => md5(post("password")),
            "is_active" => 1,
            "active_at" => $active_at,
            "trial_expired" => $trial_day,
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s"),
            "token" => md5(date("Y-m-d H:i:s")) 
        );

        $detail = array(
            "first_name" => post("first"),
            "last_name" => post("last"),
            "birth_date" => post("birth_date"),
            "phone" => post("phone"),
            "address" => post("address"),
            "gender" => post("jk"),
            "created_at" => date("Y-m-d H:i:s"),
            "update_at" => date("Y-m-d H:i:s")
        );
        $add_data =$this->M_login->register($data,$detail);
    
        if ($add_data ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Failed Register'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        $send = $this->send_verify(post('email'));

        if($send!=1){
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Success Register, Failed Send verify Email'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Success Register, Please Check Your Email To verify'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/login/register_information/'
            );
             print json_encode($json_data);
        }

    }

    public function send_verify($mail){
        $this->load->library('ElasticEmail');
        $email = $mail;
        $data_user = $this->M_login->get_row("*","m__member","email='$email' ","","",FALSE);

        $Elastic = new ElasticEmail();
        $admin = "admin@resellerindo.com";
        $from = $admin;
        $fromName = "resellerindo";
        $to = $email;
        $subject = "Verify Email";
        $bodyText = ""; 
        $bodyHtml = 
        "<b>Hello ".$data_user->username." ,</b><br><br><br>
        Email Address : ".$data_user->email."<br>
        Thanks for signing up in Resellerindo!  <br>
        We're happy you're here. Let's get your email address verified , click <a href='".base_url()."reseller.php/verify/cek_verify/?rev2id=".$data_user->id_member."&token=".$data_user->token."'>this link </a> to verify your email.
        <br><br><br>
        Thanks,<br>
        Admin Resellerindo
        "; 

        $send = $Elastic->send($from,$fromName,$subject,$to,$bodyText,$bodyHtml);
        $send = json_decode($send);
        
        return $send->success;
    }

    public function ajax_action_login(){
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        
        $username = post('username'); 
        $password = md5(post('password'));

        /*Cek required*/
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Username and password is required'),
                "form_error" => $error,
                "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
            );
            print json_encode($json_data);
            die();
        }
             $cek = count($this->M_login->fetch_table("id_member","m__member","(username='$username' or email ='$username') and password = '$password' and is_active = '1'  "));
                
        /*Cek Data Ada atau tidak*/
        if(@$cek==0){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Login Failed, Check your username or password '),
                "form_error" => "",
                "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
                );
            print json_encode($json_data);
            die();
        }
        $data_user = $this->M_login->get_row("*","m__member","(username='$username' or email ='$username') and password='$password'","","",FALSE);
        /*Cek Data is_banned*/
        if ($data_user->is_banned=="1") {
            $json_data =  array(
            "result" => FALSE ,
            "message" => array('head'=> 'Failed', 'body'=> 'Login Failed, Your Account Is Banned '),
            "form_error" => "",
            "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
            );
            print json_encode($json_data);
            die();
        }
        if ($data_user->is_premium=="1") {
            $type_user = "Premium";
        }else{
            $type_user = "Free";
        }
        $this->session->set_userdata('resv2.m_log_session', TRUE);
        $this->session->set_userdata('resv2.m_id', $data_user->id_member);
            if($data_user->is_verifyemail=="1"){
                $last_news = $this->M_login->last_news();
                $url = "/login/home?news=".$last_news->id_post;
            }else{
                $url = "/verify/not_verify/";
            }
        $json_data =  array(
            "result" => TRUE ,
            "message" => array('head'=> 'Success', 'body'=> 'Success Login User '.$type_user),
            "form_error" => "",
            "redirect" => ''.base_url().$this->config->item('index_page').$url
        );
        print json_encode($json_data);                  
        
    }

    public function show_popup_news()
    {
        $id = post("id");
        $data = $this->M_login->fetch_table("*","a__post","id_post = '$id'");
        echo json_encode($data);
    }

    public function cek_email(){
        $cek_email = count($this->M_login->fetch_table("id_member","m__member"," email = '".post('email')."' and is_active ='1' and is_banned='0' "));  
        echo $cek_email;      
    }

    public function home(){
    	if(!empty($this->session->userdata('resv2.m_log_session'))){
            $id_member = $this->session->userdata('resv2.m_id');
    		$data['data_user'] = $this->M_login->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
            $data['data_profile'] = $this->M_login->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            $data['jml_pesanan'] = count($this->M_login->fetch_table("id","m__orders","id_member = '$id_member' and status = 'COMPLETED' or status = 'PARTIAL' "));
            $data['jml_deposit'] = count($this->M_login->fetch_table("id","m__payment","id_member = '$id_member' and status = 'PAID'"));
            $data['invoice'] = $this->M_login->get_row("sum(nominal_topup) as invoice","m__payment","id_member = '$id_member' and status = 'PAID'","","",FALSE);
            $data['total_pesanan'] = $this->M_login->get_row(" SUM((qty_success * price)/1000) as total_pesanan","m__orders","id_member = '$id_member' and status = 'COMPLETED' or status = 'PARTIAL'  ","","",FALSE);
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
            $data['data_order'] = $this->M_login->limit_order($id_member);
            $data['data_payment'] = $this->M_login->limit_payment($id_member);
            $data['data_news'] = $this->M_login->limit_news();
            $data['menu_post'] = $this->M_login->fetch_table("*","a__post_category","id_parent = '0'");
            $data['content'] = 'dashboard';
            $this->load->view('login/home',$data);
    	}else{
    		redirect("login/");	
    	}
    }

    public function logout(){
        $session = array("resv2.m_log_session","resv2.m_id");
        $this->session->unset_userdata($session);
        $this->session->sess_destroy();
        redirect("login/");
    }
}

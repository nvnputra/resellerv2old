<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.css" rel="stylesheet" media="all">


    <!-- Main CSS-->
    <link href="<?php echo base_url(); ?>assets/admin/css/theme.css" rel="stylesheet" media="all">

</head>
<body class="animsition">
  <div id="page-load" style="position: absolute; z-index: 1; overflow-x: hidden;
    overflow-y: auto; background: white; width: 100%; height: 100%; display: none;">
  </div>
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <h2>
                               <img src="<?php echo base_url(); ?>uploads/Icon-Logo-Black-Followersindo-1.png" alt="CoolAdmin" />
                            </h2>
                        </div>
                        <?php foreach ($member as $key) { ?>
                            <center><i style="font-size: 100px; color: green;" class="fas fa-check-circle"></i><br><br></center>
                            <center><h3>User telah berhasil terdaftar,silahkan cek email untuk verifikasi email</h3><br>
                              <button class="btn btn-success" id="btn-show" onclick="show_verify()">Kirim ulang email verifikasi</button>
                              <a href="<?=base_url().$this->config->item('index_page'); ?>/login/home?news=<?=(!empty($last_news) ?  $last_news->id_post : 0); ?>"><button class="btn btn-danger btn-lg">Skip</button></a>
                              </center>
                        <div style="display: none;" id="form-verify">
                          <b>Send Verify to your email :</b><br>
                          <input  type="text" class="form-control" id="mail" value="<?php echo $key->email; ?>" name="">
                          
                          <center><br>
                          <button onclick="ajax_action_send_verify('<?php echo $key->email; ?>')" class="btn btn-success btn-lg">Resend verify</button>                          
                        </center>
                        </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url(); ?>assets/admin/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/toast/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/vendor/loading/jquery.loading.min.js"></script>
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>

    <script type="text/javascript">
  var csfrData = {};
    csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
    $.ajaxSetup({
    data: csfrData
  });

    function show_verify(){
      $('#btn-show').hide();
      $('#form-verify').show('low');
    }

  function ajax_action_send_verify(){
    var mail = $('#mail').val();
       $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/verify/ajax_action_send_verify",
              type:'POST',
              dataType: "json",
              data: {email:mail},
              beforeSend: function () {
                  $('#page-load').loading();
              },
              success: function(data) {
                $('#page-load').loading('stop');
                if(data.result){
                  toastr["success"](data.message.body);
                  setTimeout(function(){window.location = data.redirect},500);
                }else{
                  toastr["error"](data.message.body);
                }   
              },error: function(request, status, error){
                  $('#page-load').loading('stop');
                  toastr["error"]("Error, Please try again later");
              }
            });
    }
  </script>

</body>

</html>
<!-- end document-->
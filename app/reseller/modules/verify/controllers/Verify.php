<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_verify");
      $this->load->model("login/M_login");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
        
    }

    public function not_verify(){
    	   	if(!empty($this->session->userdata('resv2.m_log_session'))){
    	   		$id_member = $this->session->userdata('resv2.m_id');
            	$isemail = "";
            	$data['member'] = $this->M_verify->fetch_table("*","m__member","id_member='$id_member' ");
                $data['last_news'] = $this->M_login->last_news();
            	foreach ($data['member'] as $key ) {
            		 $isemail = $key->is_verifyemail;	
            	}
            	if ($isemail=="1") {
            		redirect("login/");
            	}else{
            		$this->load->view('not_verify',$data);
            	}
	        }else{
	            redirect("login/"); 
	        }
    }

    public function ajax_action_send_verify(){
    	$this->load->library('ElasticEmail');
        $email = post('email');

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Email is required'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        $data_user = $this->M_verify->get_row("*","m__member","email='$email' ","","",FALSE);
        $cek_email = count($this->M_verify->fetch_table("id_member","m__member"," id_member != '$data_user->id_member' and email = '".post('email')."' "));
           
            if ($cek_email>0) {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Email has been used by another user'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

        $Elastic = new ElasticEmail();
        $admin = "admin@resellerindo.com";
        $from = $admin;
        $fromName = "resellerindo";
        $to = $email;
        $subject = "Verify Email";
        $bodyText = ""; 
        $bodyHtml = 
        "<b>Hello ".$data_user->username." ,</b><br><br><br>
        Email Address : ".$email."<br>
        Thanks for signing up in Resellerindo!  <br>
        We're happy you're here. Let's get your email address verified , click <a href='".base_url()."reseller.php/verify/cek_verify/?rev2id=".$data_user->id_member."&token=".$data_user->token."'>this link </a> to verify your email.
        <br><br><br>
        Thanks,<br>
        Admin Resellerindo
        "; 

        $send = $Elastic->send($from,$fromName,$subject,$to,$bodyText,$bodyHtml);
        $send = json_decode($send);
        if($send->success!='1' ){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Failed Send Email'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();   
        }else{
            $data_update = array(
                "email" => $email
            );
            $this->M_verify->update_table("m__member",$data_update,"id_member",$data_user->id_member);
            $last_news = $this->M_login->last_news();
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Success Send Email, Please Check Your Email!'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/login/home?news='.$last_news->id_post
            );
             print json_encode($json_data);
        }
    }

    public function cek_verify(){
    	$cek_data = count($this->M_verify->fetch_table("*","m__member","id_member='".get('rev2id')."' and token = '".get('token')."'"));

        if($cek_data=="0"){
            return $this->failed_verify();
            die();   
        }

        $data = array(
            'is_verifyemail' => '1',
            'verifyemail_at' => date("Y-m-d H:i:s")
        );

        $change = $this->M_verify->update_table("m__member",$data,"id_member",get('rev2id'));

        if ($change ===  FALSE) {
           return $this->failed_verify();
            die();   
        }else{
           return $this->success_verify();
            die();   
        }
            
    }

    public function success_verify(){
    	$this->load->view('success_verify');
    }

    public function failed_verify(){
    	$this->load->view('failed_verify');
    }

}

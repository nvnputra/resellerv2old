<div class="row">
  <div class="col-md-12">
    <div class="card-header bg-primary" style="width: 100%; color: white;">
      <b> Notifications </b> 
    </div>
    <ul class="list-group">
      <?php foreach ($notif as $key ){ ?>
      <li class="list-group-item"><b><?php echo $key->text; ?></b> <span style="float: right;"><?php echo $key->created_at; ?></span></li>
      <?php } ?>
    </ul>
  </div>
</div>
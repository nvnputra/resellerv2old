<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_notif extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }

  public function show_notif($id_member){
  	$this->db->select("*");
    $this->db->where("id_user",$id_member);
    $this->db->order_by("created_at","desc");
    $query = $this->db->get("m__notifications");
    $result = $query->result();
    return $result;
     
  }

  public function limit_notif($id_member){
    $query = $this->db->query("SELECT * FROM m__notifications where id_user = '$id_member' and (is_read is null  or is_read != '1') order by id DESC limit 10");
    
    $result = $query->result();
    return $result;
     
  }

  public function notif_order($id_member){
    $query = $this->db->query("SELECT created_at,no_transaction FROM m__orders where id_member = '$id_member' and status = 'PENDING' order by id DESC limit 10");
    
    $result = $query->result();
    return $result;
     
  }

  public function notif_payment($id_member){
    $query = $this->db->query("SELECT created_at,no_transaction FROM m__payment where id_member = '$id_member' and status = 'PENDING' order by id DESC limit 10");
    
    $result = $query->result();
    return $result;
     
  }

  public function read_notif($data,$id_member){
    $this->db->where("id_user",$id_member);
    $this->db->where('is_read != "1" or is_read IS NULL', null, false);
    $this->db->update("m__notifications", $data);
    if ($this->db->affected_rows() > 0 ) {
      return TRUE;
    }else{
      return FALSE;
    }
  }
  
  
  
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller  {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_order");
      $this->load->model("notif/M_notif");
      $this->load->library('session');
      $this->load->library('form_validation');
    }


    public function index()
    {
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            $id_member = $this->session->userdata('resv2.m_id');
            $data['data_user'] = $this->M_order->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
            if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
            }
            $data['data_profile'] = $this->M_order->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
            
            $data['content'] = 'order';
            $data['type'] = $this->M_order->fetch_table("*","a__product_type","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function order_history(){
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            $id_member = $this->session->userdata('resv2.m_id');
            $data['data_user'] = $this->M_order->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
            if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
            }
            $data['data_profile'] = $this->M_order->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['content'] = 'order_history';
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function ajax_list_history(){
        $column = "*";
        $column_order = array('no_transaction', 'target','date','total','product_name','qty_order','qty_startcount','qty_remain','qty_success','qty_failed','qty_endcount','status'); //set column field database for datatable orderable
         $column_search = array('no_transaction','target','date','total','product_name','qty_order','qty_startcount','qty_remain','qty_success','qty_failed','qty_endcount','status'); //set column field database for datatable searchable 
         $order = array('id' => 'DESC'); // default order 
         $table = "m__orders";
         $id_member = $this->session->userdata('resv2.m_id');
         $where = "id_member = '$id_member' ";
         $joins = "";

        $list = $this->M_order->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            if($key->status=="PENDING"){
                $class = "label-warning";
              }else if($key->status=="INPROGRESS"){
                $class = "label-info";
              }else if($key->status=="PROCESSING"){
                $class = "label-primary";
              }else if($key->status=="COMPLETED"){
                $class = "label-success";
              }else if($key->status=="PARTIAL"){
                $class = "label-default";
              }else if($key->status=="CANCELLED"){
                $class = "label-danger";
              }
            $no++;
            $row = array();
            $row[] = $key->no_transaction;
            $row[] = $key->target;
            $row[] = $key->date;
            $row[] = $key->total;
            $row[] = $key->product_name;
            $row[] = $key->qty_order;
            $row[] = $key->qty_startcount;
            $row[] = $key->qty_remain;
            $row[] = $key->qty_success;
            $row[] = $key->qty_failed;
            $row[] = $key->qty_endcount;
            $row[] = '<span class="label '.$class.'">'.$key->status.'</span>';
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_order->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_order->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);

    }

    public function ajax_get_product(){

        $id = post('id');
        $data = $this->M_order->fetch_table("id,name","a__product","id_type='$id' and is_active = '1'");
        print json_encode($data);

    }

    public function ajax_get_detail_product(){

        $id = post('id');
        $row = $this->M_order->get_row("id_api,api_service,is_auto,is_active","a__product","id='$id' ","","",FALSE);
        $data = $this->M_order->fetch_table("*","a__product","id='$id'");
        /*Cek status layanan*/
        if ($row->is_auto=="1") {
            $this->load->library('curl');
            $get_api = $this->M_order->get_row("*","a__product_api","id='$row->id_api' ","","",FALSE);
            $params = array(
                "key" => $get_api->api_key,
                "request" => 'service',
                "supplier" => $get_api->api_name
            );
            $get_lookup = $this->curl->simple_post($get_api->api_url, $params, array(CURLOPT_BUFFERSIZE => 10));
            $arr_data = json_decode($get_lookup);
            if (count($arr_data)=="0") {
                $status = "Tidak Aktif";
            }else{
                $count_status = 0;
                foreach ($arr_data->data as $key) {
                    if (($key->service == $row->api_service)){
                        $count_status += 1;
                    }
                }
                if ($count_status>0) {
                    $status= "Aktif";
                }else{
                    $status = "Tidak Aktif";
                }
            }
        }else{
            if ($row->is_active=="1") {
                $status = "Aktif";
            }else{
                $status = "Tidak Aktif";
            }
        }
        $json_data =  array(
                "result" => TRUE ,
                'data' => $data,
                "status" => $status
        );
        print json_encode($json_data);

    }

    public function tes_curl($url){

        $this->load->library('curl');
        $this->curl->create($url);
        $this->curl->option(CURLOPT_SSL_VERIFYPEER, 0);
        $this->curl->option(CURLOPT_SSL_VERIFYHOST, 0);
        $this->curl->execute();
        return json_encode($this->curl->info['http_code']); // array

    }

    public function ajax_action_add_order(){

        $this->form_validation->set_rules('id_product', 'Product', 'required');
        $this->form_validation->set_rules('target', 'Data', 'required');
        $this->form_validation->set_rules('qty', 'Qty', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('agree', 'agree', 'required');

        /*---------validasi data kosong--------------*/
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }
        /*-------------------------------------------*/
                 
        /*--------- validasi IG ------------------ */
        $id_product = post("id_product");
        $get_validate_product = $this->M_order->get_row("id_type,is_validate_ig,validate_ig","a__product","id='$id_product' ","","",FALSE);
        /*get data validate*/
        if ($get_validate_product->is_validate_ig=="1") {
            $data_validate = $get_validate_product->validate_ig;
        }else{
            $get_validate_type = $this->M_order->get_row("is_validate_ig,validate_ig","a__product_type","id='$get_validate_product->id_type' ","","",FALSE);
            if ($get_validate_type->is_validate_ig=="1") {
                $data_validate = $get_validate_type->validate_ig;
            }else{
                $data_validate = null;
            }
        }

        /*parse validate to array*/
        $finalArray = array();
        $asArr = explode( ',',$data_validate);
        if ($data_validate!=null) {
            foreach( $asArr as $val ){
              $tmp = explode( ':', $val );
              $finalArray[ $tmp[0] ] = $tmp[1];
            }

        }

        /*proses validate*/
            foreach($finalArray as $letter => $number) 
            {
                switch($letter) 
                {
                    case 'url':
                        if(!preg_match('#instagram.com#', post("target"))){
                                $json_data =  array(
                                    "result" => FALSE ,
                                    "message" => array('head'=> 'Failed', 'body'=> 'Invalid data instagram'),
                                    "form_error" =>'',
                                    "redirect" => ''
                                );
                                print json_encode($json_data);
                                die();
                        }
                        break;
                    case 'username':
                        // validasi username
                        break;
                    case 'available':
                         $tes_curl = $this->tes_curl(post("target"));
                         if ($tes_curl!=200) {
                             $json_data =  array(
                                    "result" => FALSE ,
                                    "message" => array('head'=> 'Failed', 'body'=> 'Invalid data '.$tes_curl),
                                    "form_error" =>'',
                                    "redirect" => ''
                                );
                                print json_encode($json_data);
                                die();
                         }
                        break;
                }
            }

        
        /*------------------------------------------------------*/

        /*---------validasi saldo kurang--------------*/
            $id_member = $this->session->userdata('resv2.m_id');
            $get_balance = $this->M_order->get_row("balance","m__member","id_member='$id_member' ","","",FALSE);
            
            if($get_balance->balance<post("harga")){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Saldo Anda Kurang'),
                    "form_error" => '',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();  
            }
        /*-------------------------------------------*/

        /*-------------generate id_trans order baru -------------*/
        $get_prefix = $this->M_order->get_row("value","sys__config","param='order-prefix' ","","",FALSE);
        $get_number = $this->M_order->get_row("value","sys__config","param='order-number' ","","",FALSE);
        $id_trans = $get_prefix->value.$get_number->value;
        /*-------------------------------------------*/

        /*---------------Cek data auto dan Set data Order----------------*/
        
        $data_auto = $this->M_order->get_row("id_api,api_service,is_auto","a__product","id='".post("id_product")."' ","","",FALSE);
        if($data_auto->is_auto=="1"){
            $this->load->library('curl');
            $data_api = $this->M_order->get_row("*","a__product_api","id='".$data_auto->id_api."' ","","",FALSE);
            $params = array(
                "key" => $data_api->api_key,
                "request" => 'order',
                "supplier" => $data_api->api_name,
                "service" => $data_auto->api_service,
                "data" => post('target'),
                "jumlah" => post('qty')
            );
        
            $get_lookup = $this->curl->simple_post($data_api->api_url, $params, array(CURLOPT_BUFFERSIZE => 10));
            $arr_data = json_decode($get_lookup);

                if ($arr_data->result==false) {
                    $json_data =  array(
                        "result" => FALSE ,
                        "message" => array('head'=> 'Failed', 'body'=> $arr_data->message),
                        "form_error" => '',
                        "redirect" => ''
                    );
                    print json_encode($json_data);
                    die();
                }
            $data_insert = array(
                    "no_transaction" => $id_trans,
                    "id_product" => post("id_product"),
                    "date" => date("Y-m-d H:i:s"),
                    "id_member" => $id_member,
                    "target" => post("target"),
                    "qty_order" => post("qty"),
                    "qty_startcount" => '0',
                    "qty_remain" => post("qty"),
                    "is_auto" => '1',
                    "orderid_auto" => $arr_data->order,
                    "is_sync" => '1',
                    "total" => post("harga"),
                    "product_name" => post("product_name"),
                    "price" => post("harga_product"),
                    "status" => "PENDING",
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );


            }else{
                $data_insert = array(
                    "no_transaction" => $id_trans,
                    "id_product" => post("id_product"),
                    "date" => date("Y-m-d H:i:s"),
                    "id_member" => $id_member,
                    "target" => post("target"),
                    "qty_order" => post("qty"),
                    "qty_startcount" => '0',
                    "qty_remain" => post("qty"),
                    "total" => post("harga"),
                    "product_name" => post("product_name"),
                    "price" => post("harga_product"),
                    "status" => "PENDING",
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
            }            

        /*-------------------------------------------*/

        /*----------------Set Data Mutasi-------------*/
            $set_mutasi = array(
                "id_user"=> $id_member,
                "credit" => post("harga"),
                "balance" => $get_balance->balance - post("harga"),
                "created_at" => date("Y-m-d H:i:s"),
                "update_at" => date("Y-m-d H:i:s")
            );
        /*-------------------------------------------*/

        /*------------Set Data Balance--------------*/
            $data_update = array(
                "balance" => $get_balance->balance - post("harga")
            );
        /*-------------------------------------------*/

        /*--------------Generate Id order Setalah Digunakan-----------------*/
            $last_number = $get_number->value;
            $update_id = sprintf('%07s', $last_number + 1);
            $data_updateid = array(
                                "value"=> $update_id
                            );
        /*-------------------------------------------*/



            $trans = $this->M_order->add_order($data_insert,$data_update,$data_updateid,$id_member,$set_mutasi);    

            if($trans==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/order/order_history/'
                );
                print json_encode($json_data);
            }        

        
    }


}

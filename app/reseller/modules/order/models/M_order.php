<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_order extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }

  function add_order($add,$update,$data_updateid,$id_member,$set_mutasi){
    $this->db->trans_begin();
    /*------------------insert Order-------------------------*/
    $this->db->insert('m__orders', $add);
    

    /*------------------Get Id Order-------------------------*/
    $get_id = $this->db->insert_id();
    $array_id = array(
      "id_order" => $get_id
    );
    $new_mutasi = array_merge($array_id,$set_mutasi);
    

    /*-------------------insert Mutasi------------------------*/
    $this->db->insert('m__mutation', $new_mutasi);    

    /*------------------Update Balance-------------------------*/
    $this->db->where("id_member",$id_member);
    $this->db->update("m__member", $update);

    /*------------------Update Id Order Setelah digunakan-------------------------*/
    $this->db->where("id","6");
    $this->db->update("sys__config", $data_updateid);
    

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }


  }
  
  
}
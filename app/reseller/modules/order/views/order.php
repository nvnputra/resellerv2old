<style type="text/css">
  .tb_detail{
    display: none;
  }
</style>
<div class="row">
  <div class="col-md-6">
    <div class="card tb_detail">
      <div class="card-header bg-primary"><b style="color: white;"><span id="title_product"></span></b></div>
      <div class="card-body">
        <table>
        <tr>
          <td>Harga </td>
          <td>:</td>
          <td align="left">Rp. <span id="d_harga"></span></td>
        </tr>
        <tr>
          <td>Min Order</td>
          <td>:</td>
          <td align="left"><p id="d_min"></p></td>
        </tr>
        <tr>
          <td >Max Order </td>
          <td>:</td>
          <td align="left"><p id="d_max"></p></td>
        </tr>
      </table>
      <input type="hidden" name="set_min" id="set_min">
      <input type="hidden" name="set_max" id="set_max">
      <input type="hidden" name="get_harga" id="get_harga">
      </div>
    </div>
  </div>
  <div class="col-md-6">
     <div class="card tb_detail">
      <div class="card-header bg-primary"><b style="color: white;">Description</b></div>
        <div class="card-body">
          <p id="d_desc"></p>
        </div>
      </div>
      <div class="card tb_detail">
      <div class="card-header bg-primary"><b style="color: white;">Status Layanan</b></div>
        <div class="card-body">
          <b><p id="d_status_layanan"></p></b>
        </div>
      </div> 
  </div>
  <div class="col-md-12">
    <form method="POST" onsubmit="return ajax_action_add_order();">
    <table class="table">
      <tr>
        <td>
          <label>Type Product</label>
          <select onchange="ajax_get_product(this)" id="type_product" name="type_product" class="form-control select2" style="width: 100%;" required="">
            <option value="">-Pilihan-</option>
            <?php foreach ($type as $type) { ?>
              <option value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
            <?php } ?>
          </select>
        </td>
        <td>
          <label>Product</label>
          <select onchange="ajax_get_detail_product(this)" id="product" name="product" class="form-control select2" style="width: 100%;" required="">
          </select>
        </td>
      </tr>  
      <tr>
        <td>
          <label>Data</label>
          <input type="text" name="target" class="form-control" id="target" required="">
        </td>
        <td>
          <label>Qty <span id="error_qty" style="color: red;"></span></label>
          <input type="number" onkeyup="ajax_set_max(this)" name="qty" class="form-control" id="qty" required="">
        </td>
      </tr>
      <tr>
        <td>
          <label>Harga</label>
          <input type="number" name="set_harga" id="set_harga" class="form-control" readonly="">
        </td>
        <td>
          <br>
          <div class="form-group">
              <input type="checkbox" value="1" name="agree" id="agree" required=""> Saya menyetujui Syarat dan Ketentuan
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit" class="btn btn-primary btn-sm">Add Order</button>
        </td>
      </tr>
    </table>    
  </form>
  </div>
  
</div>
<script type="text/javascript">
  function ajax_get_product(key) {
    $('.tb_detail').fadeOut();
    $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/order/ajax_get_product/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                      $('#page-load').show();
                    },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select Product -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#product').html(html);
                $('#qty').val("");
                $('#set_harga').val("");
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
  }

  function ajax_get_detail_product(key) {
    $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/order/ajax_get_detail_product/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                   $('#page-load').show();
                },
              success: function(data) {
                $('#page-load').hide();
                
                for(i=0; i<data.data.length; i++){
                  $('#title_product').html(data.data[i].name);
                  $('#d_harga').html(data.data[i].price_sell+"<b> / 1k</b>");
                  $('#d_min').html(data.data[i].min_order);
                  $('#d_max').html(data.data[i].max_order);
                  $('#d_desc').html(data.data[i].description);
                  $('#set_max').val(data.data[i].max_order);
                  $('#set_min').val(data.data[i].min_order);
                  $('#get_harga').val(data.data[i].price_sell);
                }
                $('#d_status_layanan').html(data.status);

                if($('#qty').val()=="" || $('#qty').val()==null ){
                    var jml = 0;
                  }else{
                    var jml = $('#qty').val();
                  }
                var harga =  parseInt($('#get_harga').val()) * parseInt(jml) /1000;
                $('#set_harga').val(harga);
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
    if(key.value==null || key.value==""){
      $('.tb_detail').fadeOut();
      $('#max_order').val("");  
    }else{
      $('.tb_detail').fadeIn();
    }
  }

  function ajax_set_max(key){
    if($('#product').val()=="" || $('#product').val()== null ){
      alert("Silahkan Pilih Product Terlebih Dahulu");
      $('#qty').val("");
    }else{
      var max = $('#set_max').val();
      var min = $('#set_min').val();
      if(parseInt(key.value)>parseInt(max)) {
        $('#error_qty').html('Maximum Order '+max);
        $('#qty').val("");
        $('#set_harga').val("");
      }else if(parseInt(key.value)<parseInt(min)){
        $('#error_qty').html('Minimum Order '+min);
        $('#set_harga').val("");
      }else{
        $('#error_qty').html("");
        if(key.value=="" || key.value==null ){
          var jml = 0;
        }else{
          var jml = key.value;
        }
        var harga =  parseInt($('#get_harga').val()) * parseInt(jml) /1000;
        $('#set_harga').val(harga);
        
      }

    }
  }

  function ajax_action_add_order(){
    var id_product = $('#product').val();
    var product_name = $("#product option:selected").text();
    var target = $('#target').val();
    var qty = $('#qty').val();
    var harga = $('#set_harga').val();
    var harga_product = $('#get_harga').val();
    if (document.getElementById('agree').checked) {
      var agree = $('#agree').val();
    }else{
      var agree = "";
    }
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/order/ajax_action_add_order/",
              type:'POST',
              dataType: "json",
              data: {id_product:id_product,product_name:product_name,target:target,qty:qty,harga:harga,agree:agree,harga_product:harga_product},
              beforeSend: function () {
                $('#page-load').show();
              },
              success: function(data) {
                $('#page-load').hide();
                  if(data.result){
                    toastr["success"](data.message.body);
                    setTimeout(function(){window.location = data.redirect},1000);
                  }else{
                    toastr["error"](data.message.body);
                  }
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }

</script>
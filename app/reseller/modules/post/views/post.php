<style type="text/css">
  #list_admin td{
    text-align: center;
  }
</style>
<div class="card-header bg-primary">
<h4 style="color: white;">- <?php echo $title; //echo $this->input->get("data"); ?> -</h4>
</div><br><br>
<?php if (count($data_post)<1) {
  echo "<center>Data Kosong</center>";
} ?>
<div id="accordion"><?php foreach($data_post as $key_a){ ?>
  <div class="card">
    <div class="card-header" id="heading<?php echo $key_a->id_post; ?>">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $key_a->id_post; ?>" aria-expanded="true" aria-controls="collapse<?php echo $key_a->id_post; ?>">
          <label for="inline-radio<?php echo $key_a->id_post; ?>" class="form-check-label ">
            <?php echo $key_a->subject; ?>
          </label>
        </button>
      </h5>
    </div>

    <div id="collapse<?php echo $key_a->id_post; ?>" class="collapse" aria-labelledby="heading<?php echo $key_a->id_post; ?>" data-parent="#accordion">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <?php echo $key_a->body; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
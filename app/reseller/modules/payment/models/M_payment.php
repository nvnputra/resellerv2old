<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_payment extends MY_Model {
  public function __construct()
  {
    parent::__construct();
    //$this->load->database();
  }

  /*function add_payment($add,$data_updateid,$data_balance,$data_premium,$data_notif,$data_mutasi,$id_member){
    $this->db->trans_begin();

    $this->db->insert('m__payment', $add);
    $get_id = $this->db->insert_id();

    $array_id = array(
      "id_payment" => $get_id
    );

    $new_mutasi = array_merge($array_id,$data_mutasi);

    $this->db->where("id","8");
    $this->db->update("sys__config", $data_updateid);

    $this->db->insert('m__notifications', $data_notif);
    
    if(($data_balance!="" || $data_balance!= null)){
        $this->update_table("m__member",$data_balance,"id_member",$id_member);
     }
     if(($data_premium!="" || $data_premium!= null)){
        $this->update_table("m__member",$data_premium,"id_member",$id_member);
     }

     if(($data_mutasi!= "" || $data_mutasi!= null)){
        $this->db->insert('m__mutation', $new_mutasi);
     }

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }


  }*/

  public function add_payment($add,$data_updateid,$id_member){
        $this->db->trans_begin();

        $this->db->insert('m__payment', $add);
       
        $this->db->where("param","pay-number");
        $this->db->update("sys__config", $data_updateid);

        if($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          return FALSE;
        }else{
          $this->db->trans_commit();
          return TRUE;
        }

  }

  public function konfirmasi_upgrade($add,$update,$member_account,$id){
    $this->db->trans_begin();
    if ($member_account=="New") {
      $this->db->insert('m__payment_account', $add);
      $get_id = $this->db->insert_id();
      $array_id = array(
        "m_id_paymentaccount" => $get_id
      );
      $new_update = array_merge($array_id,$update);
    }else{
      $array_id = array(
        "m_id_paymentaccount" => $member_account
      );
      $new_update = array_merge($array_id,$update);
    }

    $this->db->where("id",$id);
    $this->db->update("m__payment", $new_update);

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }

  }

  public function konfirmasi_auto($add,$update,$member_account,$id,$data_member,$data_notif,$data_mutasi,$id_member){
    $this->db->trans_begin();
    if ($member_account=="New") {
      $this->db->insert('m__payment_account', $add);
      $get_id = $this->db->insert_id();
      $array_id = array(
        "m_id_paymentaccount" => $get_id
      );
      $new_update = array_merge($array_id,$update);
    }else{
      $array_id = array(
        "m_id_paymentaccount" => $member_account
      );
      $new_update = array_merge($array_id,$update);
    }

    $this->db->where("id",$id);
    $this->db->update("m__payment", $new_update);

    $this->update_table("m__member",$data_member,"id_member",$id_member);

    $this->db->insert('m__notifications', $data_notif);

    if(($data_mutasi!= "" || $data_mutasi!= null)){
        $this->db->insert('m__mutation', $data_mutasi);
     }

    if($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return FALSE;
    }else{
      $this->db->trans_commit();
      return TRUE;
    }

  }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_payment");
      $this->load->model("notif/M_notif");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function topup()
	{
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            
            $id_member = $this->session->userdata('resv2.m_id');
            $data['data_profile'] = $this->M_payment->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            $data['data_user'] = $this->M_payment->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
            if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
            }
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
            $data['data_method'] = $this->M_payment->fetch_table("*","a__payment_method","");
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
            $id = $this->session->userdata('resv2.m_id');

            $joins_data = array(
                array(
                    'table' => 'a__payment_account b',
                    'condition' => 'a.a_id_paymentaccount = b.id_payment_account',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__payment_method c',
                    'condition' => 'b.id_payment_method = c.id',
                    'jointype' => ''
                ),
                array(
                    'table' => 'm__payment_account d',
                    'condition' => 'd.id_payment_account = a.m_id_paymentaccount',
                    'jointype' => 'left'
                ),
                array(
                    'table' => 'a__payment_method e',
                    'condition' => 'd.id_payment_method = e.id',
                    'jointype' => 'left'
                )
            );

            $cek_data_upgrade = $this->M_payment->fetch_joins("m__payment a","a.*,c.name as method_admin,c.code as admin_code,b.account_name as admin_name,b.account_number as admin_number,e.name as method_member,e.code as member_code,d.account_name as member_name,d.account_number as member_number,",$joins_data,"a.id_member = '$id_member' and a.type = 'TOPUP' and 
                (status = 'PENDING' OR status = 'WAITING')",FALSE);

            if (count($cek_data_upgrade)>0) {
                $data['content'] = 'after_topup';
                $data['data_upgrade'] = $cek_data_upgrade; 
            }else{
                $data['content'] = 'topup';
            }



            $joins = array(
                array(
                    'table' => 'a__payment_method b',
                    'condition' => 'a.id_payment_method = b.id',
                    'jointype' => ''
                )
            );

            $data['a_payment'] = $this->M_payment->fetch_joins("a__payment_account a","a.*,b.name as method,b.rate",$joins,"",FALSE);
            $data['m_payment'] = $this->M_payment->fetch_joins("m__payment_account a","a.*,b.name as method",$joins,"id_member = '$id'",FALSE);
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function upgrade()
    {
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            
            $id_member = $this->session->userdata('resv2.m_id');
            $data['data_profile'] = $this->M_payment->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            $data['data_user'] = $this->M_payment->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
            
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);

            $data['data_method'] = $this->M_payment->fetch_table("*","a__payment_method","");

            $data['upgrade'] = $this->M_payment->get_row("value","sys__config","param='register-upgrade' ","","",FALSE);
            
            $joins_data = array(
                array(
                    'table' => 'a__payment_account b',
                    'condition' => 'a.a_id_paymentaccount = b.id_payment_account',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__payment_method c',
                    'condition' => 'b.id_payment_method = c.id',
                    'jointype' => ''
                ),
                array(
                    'table' => 'm__payment_account d',
                    'condition' => 'd.id_payment_account = a.m_id_paymentaccount',
                    'jointype' => 'left'
                ),
                array(
                    'table' => 'a__payment_method e',
                    'condition' => 'd.id_payment_method = e.id',
                    'jointype' => 'left'
                )
            );

            $cek_data_upgrade = $this->M_payment->fetch_joins("m__payment a","a.*,c.name as method_admin,c.code as admin_code,b.account_name as admin_name,b.account_number as admin_number,e.name as method_member,e.code as member_code,d.account_name as member_name,d.account_number as member_number,",$joins_data,"a.id_member = '$id_member' and a.type = 'UPGRADE' and 
                (status = 'PENDING' OR status = 'WAITING')",FALSE);
            if ($data['data_user']->is_premium=="1") {
                $data['content'] = 'is_premium';
            }else if (count($cek_data_upgrade)>0) {
                $data['content'] = 'after_upgrade';
                $data['data_upgrade'] = $cek_data_upgrade; 
            }else{
                $data['content'] = 'upgrade';
            }


            $id = $this->session->userdata('resv2.m_id');
            $joins = array(
                array(
                    'table' => 'a__payment_method b',
                    'condition' => 'a.id_payment_method = b.id',
                    'jointype' => ''
                )
            );

            $data['a_payment'] = $this->M_payment->fetch_joins("a__payment_account a","a.*,b.name as method,b.rate",$joins,"b.method = 'BANK'",FALSE);
            $data['m_payment'] = $this->M_payment->fetch_joins("m__payment_account a","a.*,b.name as method",$joins,"id_member = '$id'",FALSE);
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function new_payment_acc(){
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            $data['data_method'] = $this->M_payment->fetch_table("*","a__payment_method","");
            $this->load->view('new_payment_acc',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function payment_history(){
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            $id_member = $this->session->userdata('resv2.m_id');
            $data['data_profile'] = $this->M_payment->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            $data['data_user'] = $this->M_payment->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
           if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
            }
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
            $data['content'] = 'payment_history';
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
    }

    public function ajax_list_payment(){
        $column = 'a.*,d.name as member_account,e.name as admin_account';
         $column_order = array(null, 'date','a.type','d.name','e.name','nominal_transfer','nominal_topup',null,'status'); //set column field database for datatable orderable
         $column_search = array('date','a.type','d.name','e.name','nominal_transfer','nominal_topup','status'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "m__payment a ";
         $id_member = $this->session->userdata('resv2.m_id');
         $where = "a.id_member = '$id_member' ";
        $joins  = array(
                array(
                    'table' => 'm__payment_account b',
                    'condition' => 'b.id_payment_account = a.m_id_paymentaccount',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__payment_account c',
                    'condition' => 'c.id_payment_account = a.a_id_paymentaccount',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__payment_method d',
                    'condition' => 'd.id= b.id_payment_method',
                    'jointype' => ''
                ),
                array(
                    'table' => 'a__payment_method e',
                    'condition' => 'e.id = c.id_payment_method',
                    'jointype' => ''
                )
            );

        $list = $this->M_payment->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
              if($key->status=="PENDING"){
                $class = "badge-warning";
              }else if($key->status=="PAID"){
                $class = "badge-success";
              }else if($key->status=="FRAUD"){
                $class = "badge-info";
              }else if($key->status=="CANCEL"){
                $class = "badge-danger";
              }else{
                $class = "badge-default";
              }
            $no++;
            $row = array();
            $file_proof = "'$key->file_proof'";
            $row[] = $no;
            $row[] = $key->date;
            $row[] = $key->type;
            $row[] = $key->member_account;
            $row[] = $key->admin_account;
            $row[] = $key->nominal_transfer;
            $row[] = $key->nominal_topup;
            $row[] = '<a href="#" onclick="show_file_proof('.$file_proof.')">view</a>';
            $row[] = '<span class="badge '.$class.'">'.$key->status.'</span>';
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_payment->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_payment->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_admin_method(){
            $method = post("method");
            $type = post("type");
            $joins = array(
                array(
                    'table' => 'a__payment_method b',
                    'condition' => 'a.id_payment_method = b.id',
                    'jointype' => ''
                )
            );
            $data = $this->M_payment->fetch_joins("a__payment_account a","a.*,b.name as method",$joins,"b.method = '$method' and a.type='$type' ",FALSE);
            echo json_encode($data);
    }

    public function ajax_get_member_method(){
            $method = post("method");
            $id_member = $this->session->userdata('resv2.m_id');
            $joins = array(
                array(
                    'table' => 'a__payment_method b',
                    'condition' => 'a.id_payment_method = b.id',
                    'jointype' => ''
                )
            );

            $data = $this->M_payment->fetch_joins("m__payment_account a","a.*,b.name as method",$joins,"b.method = '$method' and id_member = '$id_member' ",FALSE);
            echo json_encode($data);
    }

    public function ajax_get_admin_account(){
        $joins = array(
            array(
                'table' => 'a__payment_method b',
                'condition' => 'a.id_payment_method = b.id',
                'jointype' => ''
            )
        );
    $id = post("id");
    $data = $this->M_payment->fetch_joins("a__payment_account a","a.*,b.name as method,b.rate as rate",$joins,"id_payment_account = '$id' ",FALSE);
    echo  json_encode($data);
    }

    public function ajax_get_member_account(){
        $joins = array(
        array(
            'table' => 'a__payment_method b',
            'condition' => 'a.id_payment_method = b.id',
            'jointype' => ''
        )
    );

    $id = post("id");
    $data = $this->M_payment->fetch_joins("m__payment_account a","a.*,b.name as method",$joins,"id_payment_account = '$id' ",FALSE);
    echo  json_encode($data);

    }

    public function ajax_action_add_payment(){
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('m_id_paymentaccount', 'Member account', 'required');
        $this->form_validation->set_rules('a_id_paymentaccount', 'Adminaccount name', 'required');
        $this->form_validation->set_rules('nominal_transfer', 'nominal transfer', 'required');
        $this->form_validation->set_rules('nominal_topup', 'nominal topup', 'required');


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{

            $config['upload_path'] = './uploads/file_proof/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload('file'))
            {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }
            
            $id_member = $this->session->userdata('resv2.m_id');
            $get_prefix = $this->M_payment->get_row("value","sys__config","param='pay-prefix' ","","",FALSE);
            $get_number = $this->M_payment->get_row("value","sys__config","param='pay-number' ","","",FALSE);

            $get_member_account = $this->M_payment->get_row("account_number,account_name,account_branch","m__payment_account","id_member= '$id_member'","","",FALSE); 
            $id_trans = $get_prefix->value.$get_number->value;

            if (post("type")=="TOPUP") {
                $set_topup = post("nominal_topup");
            }else{
                $set_topup = 0;
            }

            
            if (post("metode")=="AUTO") {
            
            $this->load->library('curl');
            $params = array(
                "key" => "Resv2e8300332e447dc4dcae4709ef43d9a80",
                "request" => 'balance',
                "accountid" => post("a_id_paymentaccount"),
                "amount" => post("nominal_transfer"),
            );
        
            $get_lookup = $this->curl->simple_post("http://resellerindo.com/index/api/api.resv2.php", $params, array(CURLOPT_BUFFERSIZE => 10));
            $arr_data = json_decode($get_lookup);
        
                if ($arr_data->result==false) {
                    $json_data =  array(
                        "result" => FALSE ,
                        "message" => array('head'=> 'Failed', 'body'=> "Failed Add Payment Auto"),
                        "form_error" => '',
                        "redirect" => ''
                    );
                    print json_encode($json_data);
                    die();
                }

                $get_data = $this->M_payment->get_row("balance","m__member","id_member='$id_member' ","","",FALSE); 

                $status = "PAID";
                if (post("type")=="TOPUP") {
                    $data_balance = array(
                          "balance" => $get_data->balance + post("nominal_topup")
                      );
                    $data_premium = null;
                }else{
                    $data_balance = null;
                    $data_premium = array(
                        "is_premium"=>1,
                        "premium_at"=>date("Y-m-d H:i:s")
                      );
                }
                $data_notif = array(
                  "id_user" => $id_member,
                  "created_at" => date("Y-m-d H:i:s"),
                  "text" => "Payment ".$id_trans." is ".$status
                );
                 $data_mutasi = array(
                    "id_user"=> $id_member,
                    "debt" => post("nominal_topup"),
                    "balance" => $get_data->balance + post("nominal_topup"),
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                  );

            }else{
                $status = "PENDING";
                $data_balance = null;
                $data_premium = null;
                $data_notif = null;
                $data_mutasi = null;
            }
            
            $data = $this->upload->data();
            $data_insert = array(
                "no_transaction" => $id_trans,
                "date" => date("Y-m-d H:i:s"),
                "id_member" => $id_member,
                "type" => post("type"),
                "m_id_paymentaccount" => post("m_id_paymentaccount"),
                "a_id_paymentaccount" => post("a_id_paymentaccount"),
                "nominal_transfer" => post("nominal_transfer"),
                "nominal_topup" => $set_topup,
                "sender_accountnumber" => $get_member_account->account_number,
                "sender_name" => $get_member_account->account_name,
                "sender_bank" => $get_member_account->account_branch,
                "file_proof" => $data['file_name'],
                "status" => $status,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            );

            $last_number = $get_number->value;
            $update_id = sprintf('%07s', $last_number + 1);
            $data_update = array(
                                "value"=> $update_id
                            );

            $add = $this->M_payment->add_payment($data_insert,$data_update,$data_balance,$data_premium,$data_notif,$data_mutasi,$id_member);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment'
                );
                print json_encode($json_data);
            }
        }
    }

    

    public function ajax_action_add_upgrade(){
        $this->form_validation->set_rules('admin_account', 'admin_account', 'required');
        $this->form_validation->set_rules('nominal', 'nominal', 'required');


        /*cek validasi data kosong*/
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }
            
        $id_member = $this->session->userdata('resv2.m_id');
        $cek_data =  $this->M_payment->fetch_table("*","m__payment","id_member = '$id_member' and type = 'UPGRADE' and (status = 'PENDING' or status = 'WAITING')");
        /*cek data payment pending*/
        if (count($cek_data)>0) {
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Masih Terdapat Transaksi Yang Belum selesai'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        /*generate id payment dari config*/ 
            $get_prefix = $this->M_payment->get_row("value","sys__config","param='pay-prefix' ","","",FALSE);
            $get_number = $this->M_payment->get_row("value","sys__config","param='pay-number' ","","",FALSE);

            //$get_member_account = $this->M_payment->get_row("account_number,account_name,account_branch","m__payment_account","id_member= '$id_member'","","",FALSE); 
            $id_trans = $get_prefix->value.$get_number->value;
            $status = "WAITING";
             
        /*set data insert payment*/
            $data_insert = array(
                "no_transaction" => $id_trans,
                "date" => date("Y-m-d H:i:s"),
                "id_member" => $id_member,
                "type" => "UPGRADE",
                "a_id_paymentaccount" => post("admin_account"),
                "nominal_transfer" => post("nominal"),
                "nominal_topup" => 0,
                "sender_accountnumber" => '',
                "sender_name" => '',
                "sender_bank" => '',
                "file_proof" => '',
                "status" => $status,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            );

            /*set update data id config*/
            $last_number = $get_number->value;
            $update_id = sprintf('%07s', $last_number + 1);
            $data_update = array(
                                "value"=> $update_id
                            );

            $add = $this->M_payment->add_payment($data_insert,$data_update,$id_member);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Failed Upgrade'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Success Upgrade'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/upgrade'
                );
                print json_encode($json_data);
            }
    }

    function ajax_action_add_topup(){
        $this->form_validation->set_rules('admin_account', 'admin_account', 'required');
        $this->form_validation->set_rules('nominal', 'nominal', 'required');

        /*validasi data kosong*/
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        /*validasi nominal minimal*/ 
        if (post('nominal')<50000) {
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Minimal Nominal 50,000'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        $id_member = $this->session->userdata('resv2.m_id');
        $cek_data =  $this->M_payment->fetch_table("*","m__payment","id_member = '$id_member' and type = 'TOPUP' and (status = 'PENDING' or status = 'WAITING')");
        /*validasi transaksi pending*/
        if (count($cek_data)>0) {
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Masih Terdapat Transaksi Yang Belum selesai'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

            /*generate id payment dari config*/
            $get_prefix = $this->M_payment->get_row("value","sys__config","param='pay-prefix' ","","",FALSE);
            $get_number = $this->M_payment->get_row("value","sys__config","param='pay-number' ","","",FALSE);

            //$get_member_account = $this->M_payment->get_row("account_number,account_name,account_branch","m__payment_account","id_member= '$id_member'","","",FALSE); 
            $id_trans = $get_prefix->value.$get_number->value;

            $status = "WAITING";
             

            /*set data insert payment*/
            $data_insert = array(
                "no_transaction" => $id_trans,
                "date" => date("Y-m-d H:i:s"),
                "id_member" => $id_member,
                "type" => "TOPUP",
                "a_id_paymentaccount" => post("admin_account"),
                "nominal_transfer" => post("nominal"),
                "nominal_topup" => post("nominal"),
                "sender_accountnumber" => '',
                "sender_name" => '',
                "sender_bank" => '',
                "file_proof" => '',
                "status" => $status,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            );

            /*set data update id payment di config*/
            $last_number = $get_number->value;
            $update_id = sprintf('%07s', $last_number + 1);
            $data_update = array(
                                "value"=> $update_id
                            );

            $add = $this->M_payment->add_payment($data_insert,$data_update,$id_member);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Failed TOPUP'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Success TOPUP'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/topup'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_action_konfirmasi_pembayaran(){
            $this->form_validation->set_rules('member_account', 'member_account', 'required');


            /*validasi data kosong*/
            if($this->form_validation->run()==FALSE){
                $error = $this->form_validation->error_array();
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }

            $config['upload_path'] = './uploads/file_proof/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
      
            $this->load->library('upload', $config);
            
            /*validasi upload*/
            if (!$this->upload->do_upload('file'))
            {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }
            $data = $this->upload->data();
            $member_account = post('member_account');
            
            /*validasi new member account*/
            if ($member_account=="New") {
                $data_insert = array(
                    "id_payment_method" => post("method"),
                    "id_member" => $this->session->userdata('resv2.m_id'),
                    "type" => "AUTO",
                    "account_number" => post("account_number"),
                    "account_name" => post("account_name"),
                    "account_branch" => post("account_branch"),
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
                $sender_accountnumber = post("account_number");
                $sender_name = post("account_name");
            }else{
                $get_member_account = $this->M_payment->get_row("account_number,account_name,account_branch","m__payment_account","id_payment_account= '$member_account'","","",FALSE);
                $sender_accountnumber = $get_member_account->account_number;
                $sender_name = $get_member_account->account_name;
                $data_insert = null;
            }

            /*set data update payment*/
            $data_update = array(
                "sender_accountnumber" => $sender_accountnumber,
                "sender_name" => $sender_name,
                "status" => "PENDING",
                "file_proof" =>$data['file_name']
            );
            $edit = $this->M_payment->konfirmasi_upgrade($data_insert,$data_update,$member_account,post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Failed Confrim'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Success Confrim'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/upgrade/'
                );
                print json_encode($json_data);
            }

    }

    public function ajax_action_konfirmasi_topup(){

            $this->form_validation->set_rules('member_account', 'member_account', 'required');

            /*validasi data kosong*/
            if($this->form_validation->run()==FALSE){
                $error = $this->form_validation->error_array();
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }
            $config['upload_path'] = './uploads/file_proof/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
      
            $this->load->library('upload', $config);
            
            /*validasi upload*/
            if (!$this->upload->do_upload('file'))
            {
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', '')),
                    "form_error" => 'file proof',
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }
            $data = $this->upload->data();
            $member_account = post('member_account');
            /*validasi new member_account*/
            if ($member_account=="New") {
                $data_insert = array(
                    "id_payment_method" => post("method"),
                    "id_member" => $this->session->userdata('resv2.m_id'),
                    "type" => "AUTO",
                    "account_number" => post("account_number"),
                    "account_name" => post("account_name"),
                    "account_branch" => post("account_branch"),
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
            $sender_accountnumber = post("account_number");
            $sender_name = post("account_name");
            }else{
                $get_member_account = $this->M_payment->get_row("account_number,account_name,account_branch","m__payment_account","id_payment_account= '$member_account'","","",FALSE);
                $sender_accountnumber = $get_member_account->account_number;
                $sender_name = $get_member_account->account_name;
                $data_insert = null;
            }

            /*set data update payment*/
            $data_update = array(
                "sender_accountnumber" => $sender_accountnumber,
                "sender_name" => $sender_name,
                "status" => "PENDING",
                "file_proof" =>$data['file_name']
            );
            $edit = $this->M_payment->konfirmasi_upgrade($data_insert,$data_update,$member_account,post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Failed Confrim Topup'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Success Confrim Topup'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/topup/'
                );
                print json_encode($json_data);
            }

    }


    public function ajax_action_cancel_topup(){
         $delete = $this->M_payment->delete_table("m__payment","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Membatalkan Topup'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Membatalkan Topup'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/topup/'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_action_cancel_upgrade(){
         $delete = $this->M_payment->delete_table("m__payment","id",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Membatalkan Upgrade'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Membatalkan Upgrade'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/upgrade/'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_action_konfirmasi_auto(){
        $this->form_validation->set_rules('id_account', 'id_account', 'required');
        $this->form_validation->set_rules('nominal', 'nominal', 'required');
        $this->form_validation->set_rules('member_account', 'member_account', 'required');
        $id_member = $this->session->userdata('resv2.m_id'); 


        /*validasi data kosong*/
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }


        /*cek data ke API*/
        $id_payment_account = post("id_account");
        $get_api = $this->M_payment->get_row("*","a__payment_account","id_payment_account='$id_payment_account' ","","",FALSE);
            $this->load->library('curl');
            $params = array(
                "key" => $get_api->key,
                "request" => 'balance',
                "accountid" => $id_payment_account,
                "amount" => post("nominal"),
            );
        
            $get_lookup = $this->curl->simple_post("http://resellerindo.com/index/api/api.resv2.php", $params, array(CURLOPT_BUFFERSIZE => 10));
            $arr_data = json_decode($get_lookup);
                
                /*validasi API tidak ditemukan*/
                if ($arr_data->result==false) {
                    $json_data =  array(
                        "result" => FALSE ,
                        "message" => array('head'=> 'Failed', 'body'=> "konfirmasi otomatis GAGAL"),
                        "form_error" => '',
                        "redirect" => ''
                    );
                    print json_encode($json_data);
                    die();
                }

            
            $id_payment = post("id");
            $get_data = $this->M_payment->get_row("balance","m__member","id_member='$id_member' ","","",FALSE); 
            $get_payment = $this->M_payment->get_row("no_transaction","m__payment","id='$id_payment' ","","",FALSE);
            $status = "PAID";
                
            if (post("type")=="TOPUP") {
                $data_member = array(
                      "balance" => $get_data->balance + post("nominal")
                  );
            }else{
                $data_member = array(
                    "is_premium"=>1,
                    "premium_at"=>date("Y-m-d H:i:s")
                  );
            }
            $data_notif = array(
              "id_user" => $id_member,
              "created_at" => date("Y-m-d H:i:s"),
              "text" => "Payment ".$get_payment->no_transaction." is ".$status
            );
            $data_mutasi = array(
                "id_user"=> $id_member,
                "debt" => post("nominal"),
                "balance" => $get_data->balance + post("nominal"),
                "created_at" => date("Y-m-d H:i:s"),
                "update_at" => date("Y-m-d H:i:s")
              );

            
            
            $member_account = post('member_account');
            if ($member_account=="New") {
                $data_insert = array(
                    "id_payment_method" => post("method"),
                    "id_member" => $this->session->userdata('resv2.m_id'),
                    "type" => "AUTO",
                    "account_number" => post("account_number"),
                    "account_name" => post("account_name"),
                    "account_branch" => post("account_branch"),
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
            $sender_accountnumber = post("account_number");
            $sender_name = post("account_name");
            }else{
                $get_member_account = $this->M_payment->get_row("account_number,account_name,account_branch","m__payment_account","id_payment_account= '$member_account'","","",FALSE);
                $sender_accountnumber = $get_member_account->account_number;
                $sender_name = $get_member_account->account_name;
                $data_insert = null;
            }
            $data_update = array(
                "sender_accountnumber" => $sender_accountnumber,
                "sender_name" => $sender_name,
                "status" => $status
            );

            $add = $this->M_payment->konfirmasi_auto($data_insert,$data_update,$member_account,$id_payment,$data_member,$data_notif,$data_mutasi,$id_member);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal konfirmasi auto'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses konfirmasi auto'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment/payment_history'
                );
                print json_encode($json_data);
            }    
    }

    
}

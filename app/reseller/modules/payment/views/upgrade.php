<style type="text/css">
	.card{
		margin-bottom: 0;
	}
</style>
<label>Admin Account :</label>
<div id="accordion">
  <?php $rand = rand(50,499); foreach($a_payment as $key_a){ ?>
  <div class="card">
    <div class="card-header" id="heading<?php echo $key_a->id_payment_account; ?>">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $key_a->id_payment_account; ?>" aria-expanded="true" aria-controls="collapse<?php echo $key_a->id_payment_account; ?>">
          <h4><?php echo $key_a->method; ?></h4>
        </button>
      </h5>
    </div>

    <div id="collapse<?php echo $key_a->id_payment_account; ?>" class="collapse" aria-labelledby="heading<?php echo $key_a->id_payment_account; ?>" data-parent="#accordion">
      <div class="card-body">
      	<div class="row">
      		<div class="col-md-6">
      			<p><b>Account Name : </b> <?php echo $key_a->account_name; ?></p><br>
		        <p><b>Account Number : </b> <?php echo $key_a->account_number; ?></p><br>
		        <p><b>Account Branch : </b> <?php echo $key_a->account_branch; ?></p><br>	
      		</div>
      		<div class="col-md-6">
      			<fieldset>
      				<center>Nominal Upgrde :</center>
      				<center><h1><?php  $nominal = ($upgrade->value*$key_a->rate) + $rand; echo number_format($nominal); ?></h1></center>
      				<center><button onclick="ajax_action_upgrade('<?php echo $key_a->id_payment_account; ?>','<?php echo $nominal; ?>')" class="btn btn-success">Next</button></center>
      			</fieldset>
      		</div>
      	</div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>


<script type="text/javascript">
	
	function ajax_action_upgrade(admin_account,nominal){
       $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/payment/ajax_action_add_upgrade",
              type:'POST',
              dataType: "json",
              data: {admin_account:admin_account,nominal:nominal},
              beforeSend: function () {
                  $('#page-load').show();
              },
              success: function(data) {
                        $('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }   
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
	}
</script>
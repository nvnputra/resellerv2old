<style type="text/css">
  .card{
    margin-bottom: 0;
  }
</style>
<?php 
foreach ($data_upgrade as $key ) {  ?>
<input type="hidden" id="id" value="<?php echo $key->id; ?>" name="">
<br>
<?php if ($key->status=="WAITING") { ?>
<!-- Petunjuk Pembayaran -->
  <div class="card">
    <div class="card-header" id="heading-petunjuk">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-petunjuk" aria-expanded="true" aria-controls="collapse-petunjuk">
           Petunjuk Pembayaran
         </button>
      </h5>
    </div>
      <div class="card-body">
        <center>
          <h4>Segera Lakukan Pembayaran Sebelum</h4>
          <b><?php echo date('d-m-Y , H:i A', strtotime($key->created_at .' +1 days')); ?></b>

          <br><br>
          <b>Transfer Pembayaran Ke</b>
          <br>
          <div class="card bg-info" style="color: white; width: 40%; padding: 10px;">
            <h4 style="color: white;"><?php echo $key->method_admin; ?>(<?php echo $key->admin_code; ?>)</h4>
            <?php echo $key->admin_number; ?><br>
            a/n <?php echo $key->admin_name; ?>
          </div>
          
          <h4>Jumlah Yang Harus Di Bayar :</h4>
          <h3 style="color: #ff4600;">Rp. <?php echo number_format($key->nominal_transfer); ?></h3>
          <br>
        </center>
        <div style="width: 100%; padding: 30px; background: #4c4c4c; ">
          <center>
            <div>
              <h4 style="color: white;">Transfer tepat sampai 3 digit terakhir</h4><br>
              <h5 style="color: white;">Perbedaan Nominal menghambat proses verifikasi</h5>
            </div>
          </center>
        </div>
      </div>
  </div>
  <!-- --------------------------------------------------- -->
  <!-- Status Pembayaran -->
  <div class="card">
    <div class="card-header" id="heading-status">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-status" aria-expanded="true" aria-controls="collapse-status">
            Status Pembayaran
          </button>
      </h5>
    </div>

      <div class="card-body">
        <h4><?php if ($key->status=="WAITING") { echo "Belum terbayar";  }else{ echo " menunggu persetujuan "; } ?></h4>
        <br><button class="btn btn-danger" onclick="ajax_action_cancel_payment()" type="button">Batalkan Pembayaran</button>
      </div>
  </div>
  <!-- --------------------------------------------------- -->
<div id="accordion">
   <!-- Konfirmasi auto -->
  <div class="card">
    <div class="card-header" id="heading-auto">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-auto" aria-expanded="true" aria-controls="collapse-auto">
            Konfirmasi Pembayaran (Auto)
          </button>
      </h5>
    </div>

    <div id="collapse-auto" class="collapse" aria-labelledby="heading-auto" data-parent="#accordion">
      <div class="card-body">
        <div class="alert alert-warning" role="alert">
             Jika anda sudah melakukan pembayaran sesuai jumlah diatas, anda bisa langsung cek status pembayaran secara otomatis tanpa menunggu admin
        </div>
        <label>Member Payment Account :</label>
      <select id="member_account_auto" class="select2" style="width: 100%;" onchange="show_detail_member_acc(this)">
        <option value="">- Select Payment Account -</option>
        <optgroup label="+ New Payment Account" >
            <option value="New">New</option>
         </optgroup>
         <optgroup label="Other" >
          <?php foreach ($m_payment as $key_m) { ?>
            <option value="<?php echo $key_m->id_payment_account; ?>"><?php echo $key_m->method." - ".$key_m->account_number; ?></option>
          <?php } ?>
        </optgroup>
      </select>
      <div id="form_new_acc" style="display: none;">
        <table class="table" style="background: #eee;">
          <tr>
            <td colspan="2"><center>New Payment Account</center></td>
          </tr>
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="method_auto" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" name="account_name" class="form-control" id="account_name_auto">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" name="account_number" class="form-control" id="account_number_auto">
            </td>
            <td style="display: none;">
              <label>Account Branch</label><br>
              <input type="hidden" name="account_branch" class="form-control" id="account_branch_auto">
            </td>
          </tr>
        </table>
      </div>
        <table id="tb_m_acc" class="table" style="display: none; background: #eee;">
            <tr>
              <td colspan="3" align="center"><b>Member Account</b></td>
            </tr>
            <tr>
              <td>Method</td>
              <td>:</td>
              <td><p id="m_ket_method_auto"></p></td>
            </tr>
            <tr>
              <td>Account Number</td>
              <td>:</td>
              <td><p id="m_ket_number_auto"></p></td>
            </tr>
            <tr>
              <td>Account Name</td>
              <td>:</td>
              <td><p id="m_ket_name_auto"></p></td>
            </tr>
           <!--  <tr>
              <td>Account Branch</td>
              <td>:</td>
              <td><p id="m_ket_branch"></p></td>
            </tr> -->
            <tr>
              <td>Type</td>
              <td>:</td>
              <td><p id="m_ket_type_auto"></p></td>
            </tr>
          </table><br>
          <button onclick="ajax_action_payment_auto('<?php echo $key->a_id_paymentaccount; ?>','<?php echo $key->nominal_transfer; ?>')" type="button" class="btn btn-success">Konfrim Auto</button>
          <br><br>
          <div id="alert_gagal_auto" style="display: none;" class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
            Pembayaran anda belum masuk,<br> jika anda sudah benar-benar membayar silahkan konfirmasi secara manual 
            <a href="#" onclick="showform()" class="alert-link">disini</a> 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
      </div>
    </div>
  </div>
  <!-- --------------------------------------------------- -->
   <!-- Konfirm Manual -->
  <div class="card">
    <div class="card-header" id="heading-manual">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-manual" aria-expanded="true" aria-controls="collapse-manual">
            Konfirmasi pembayaran (manual)
          </button>
      </h5>
    </div>

    <div id="collapse-manual" class="collapse" aria-labelledby="heading-manual" data-parent="#accordion">
  <div class="card-body">
    <div class="alert alert-warning" role="alert">
      Konfirmasi secara manual dengan menyertakan bukti foto, proses konfirmasi paling lama 1x24 jam
    </div>
    <label>Member Payment Account :</label>
      <select id="member_account" class="select2" style="width: 100%;" onchange="show_detail_member_acc2(this)">
        <option value="">- Select Payment Account -</option>
        <optgroup label="+ New Payment Account" >
            <option value="New">New</option>
         </optgroup>
         <optgroup label="Other" >
          <?php foreach ($m_payment as $key_m) { ?>
            <option value="<?php echo $key_m->id_payment_account; ?>"><?php echo $key_m->method." - ".$key_m->account_number; ?></option>
          <?php } ?>
        </optgroup>
      </select>
      <div id="form_new_acc2" style="display: none;">
        <table class="table" style="background: #eee;">
          <tr>
            <td colspan="2"><center>New Payment Account</center></td>
          </tr>
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="method" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" name="account_name" class="form-control" id="account_name">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" name="account_number" class="form-control" id="account_number">
            </td>
            <td style="display: none;">
              <label>Account Branch</label><br>
              <input type="hidden" name="account_branch" class="form-control" id="account_branch">
            </td>
          </tr>
        </table>
      </div>
        <table id="tb_m_acc2" class="table" style="display: none; background: #eee;">
            <tr>
              <td colspan="3" align="center"><b>Member Account</b></td>
            </tr>
            <tr>
              <td>Method</td>
              <td>:</td>
              <td><p id="m_ket_method"></p></td>
            </tr>
            <tr>
              <td>Account Number</td>
              <td>:</td>
              <td><p id="m_ket_number"></p></td>
            </tr>
            <tr>
              <td>Account Name</td>
              <td>:</td>
              <td><p id="m_ket_name"></p></td>
            </tr>
           <!--  <tr>
              <td>Account Branch</td>
              <td>:</td>
              <td><p id="m_ket_branch"></p></td>
            </tr> -->
            <tr>
              <td>Type</td>
              <td>:</td>
              <td><p id="m_ket_type"></p></td>
            </tr>
          </table>
  <br>
  <input type="file" name="pic" id="file_proof" class="form-control" accept="image/*"><br>
  <button class="btn btn-success" id="btn-konfrim" onclick="ajax_action_konfirmasi_pembayaran()">Submit</button>
  </div>  
    </div>
  </div>
  <!-- --------------------------------------------------- -->
</div>

 <?php }else{ ?>
  <center><h3>Terimakasih telah melakukan konfirmasi</h3><br>
          <h4>silahkan menunggu persetujuan dari admin</h4><br>
          <h4>Nominal Transfer :</h4>
          <h3 style="color: #ff4600;">Rp. <?php echo number_format($key->nominal_transfer); ?></h3><br>
          <h4>Status :</h4>
          <span class="badge badge-warning"><h5>menunggu persetujuan</h5></span>
  </center><br><br>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">Member Account</div>
        <div class="card-body">
          <table class="table">
            <tr><td><b>Method :</b> <?php echo $key->method_member; ?></td></tr>
            <tr><td><b>Account Number :</b> <?php echo $key->member_number; ?></td></tr>
            <tr><td><b>Account Name :</b> <?php echo $key->member_name; ?></td></tr>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">Admin Account</div>
        <div class="card-body">
          <table class="table">
            <tr><td><b>Method :</b> <?php echo $key->method_admin; ?></td></tr>
            <tr><td><b>Account Number :</b> <?php echo $key->admin_number; ?></td></tr>
            <tr><td><b>Account Name :</b> <?php echo $key->admin_name; ?></td></tr>
          </table>
        </div>
      </div>
    </div>
  </div>
<label>Bukti Pembayaran :</label>
  <div class="col-md-7"><img style="height: 300px;" src="<?php echo base_url(); ?>uploads/file_proof/<?php echo $key->file_proof; ?>"></div>
<?php }
} ?>
<script type="text/javascript">
   function showform(){
    //$("#collapse-auto").collapse('hide');
    $("#collapse-manual").collapse('show');
    //$('#form-konfirmasi').slideDown();
    //$('html, body').animate({ scrollTop: $('#btn-konfrim').offset().top }, 'slow');
  }
  function show_detail_member_acc(key) {
    if (key.value==="New") {
      $('#tb_m_acc').hide();
      $('#form_new_acc').show();
    }else{
      $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/payment/ajax_get_member_account",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                  $('#form_new_acc').hide();
                  $('#page-load').show();
              },
              success: function(data) {
                  var i ;
                  for(i = 0; i<data.length; i++){
                      $("#m_ket_name_auto").html(data[i].account_name);
                      $("#m_ket_number_auto").html(data[i].account_number);
                      $("#m_ket_branch").html(data[i].account_branch);
                      $("#m_ket_type_auto").html(data[i].type);
                      $("#m_ket_method_auto").html(data[i].method);
                 }
                if(key.value!=""){ $('#tb_m_acc').show(); }else{ $('#tb_m_acc').hide(); }
                  $('#page-load').hide();
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
    }
  }

  function show_detail_member_acc2(key) {
    if (key.value==="New") {
      $('#tb_m_acc2').hide();
      $('#form_new_acc2').show();
    }else{
      $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/payment/ajax_get_member_account",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                  $('#form_new_acc2').hide();
                  $('#page-load').show();
              },
              success: function(data) {
                  var i ;
                  for(i = 0; i<data.length; i++){
                      $("#m_ket_name").html(data[i].account_name);
                      $("#m_ket_number").html(data[i].account_number);
                      $("#m_ket_branch").html(data[i].account_branch);
                      $("#m_ket_type").html(data[i].type);
                      $("#m_ket_method").html(data[i].method);
                 }
                if(key.value!=""){ $('#tb_m_acc2').show(); }else{ $('#tb_m_acc2').hide(); }
                  $('#page-load').hide();
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
    }
  }


  function ajax_action_cancel_payment(){
    if (confirm('Apakah Anda Yakin Membatalkan Upgrade Ini?')) {
      var id = $('#id').val();
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_action_cancel_upgrade/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                    $('#page-load').show();
              },
              success: function(data) {
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                    }
                     $('#page-load').hide();
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

	function ajax_action_konfirmasi_pembayaran() {
	var file_data = $('#file_proof').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('id', $('#id').val());
    form_data.append('member_account', $('#member_account').val());
    form_data.append('method', $("#method").val());
    form_data.append('text_method', $("#method option:selected").text());
    form_data.append('account_number', $("#account_number").val());
    form_data.append('account_name', $("#account_name").val());
    form_data.append('account_branch', $("#account_branch").val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo
$this->security->get_csrf_hash(); ?>');
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_action_konfirmasi_pembayaran/",
              type:'POST',
              dataType: "json",
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              beforeSend: function () {
                  $('#page-load').show();
              },
              success: function(data) {
                        $('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }   
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
	}

  function ajax_action_payment_auto(id_account,nominal){
    var type = "UPGRADE";
    var id = $('#id').val();
    var member_account = $('#member_account_auto').val();
    var method = $("#method_auto").val();
    var text_method = $("#method_auto option:selected").text();
    var account_number = $("#account_number_auto").val();
    var account_name = $("#account_name_auto").val();
    var account_branch = $("#account_branch_auto").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_action_konfirmasi_auto/",
              type:'POST',
              dataType: "json",
              data: {id:id,id_account:id_account,nominal:nominal,type:type,member_account:member_account,method:method,text_method:text_method,account_number:account_number,account_name:account_name,account_branch:account_branch},
              beforeSend: function () {
                   $('#page-load').show();
              },
              success: function(data) {
                    $('#page-load').hide();
                    if(data.result){
                      toastr["success"](data.message.body);
                      setTimeout(function(){window.location = data.redirect},500);
                    }else{
                      toastr["error"](data.message.body);
                      $('#alert_gagal_auto').show();
                      $('html, body').animate({ scrollTop: $('#alert_gagal_auto').offset().top }, 'slow');
                    }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
      });
  }

</script>
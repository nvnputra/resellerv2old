<table class="table" style="background: #eee;">
          <tr>
            <td colspan="2"><center>New Payment Account</center></td>
          </tr>
          <tr>
            <td>
              <label>Payment Method :</label>
              <select name="method" id="method" class="form-control">
                <option value=""></option>
                <?php foreach ($data_method as $method) { ?>
                  <option value="<?php echo $method->id; ?>"><?php echo $method->method." - ".$method->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Account Name</label><br>
              <input type="text" name="account_name" class="form-control" id="account_name">
            </td>
          </tr>
          <tr>
            <td>
              <label>Account Number</label><br>
              <input type="number" name="account_number" class="form-control" id="account_number">
            </td>
            <td style="display: none;">
              <label>Account Branch</label><br>
              <input type="hidden" name="account_branch" class="form-control" id="account_branch">
            </td>
          </tr>
</table>
<style type="text/css">
	.card{
		margin-bottom: 0;
	}
</style>
<table class="table">
	<tr>
		<td>
			<label>Nominal :</label>
			<input onkeyup="set_nominal1()" type="number" id="nominal" class="form-control" name="">
			<label id="alert-nominal" style="color: red; font-weight: bold;"></label>
		</td>
	</tr>
	<tr>
		<td>
			<label>Admin Account :</label>
<div id="accordion">
	<input type="hidden" value="<?php echo rand(50,499); ?>" id="rand" name="">
	<input type="hidden" id="val-topup" value="0" name="">
	<input type="hidden" id="val-rate" value="0" name="">	
	<input type="hidden" id="val-id" value="0" name="">	
  <?php foreach($a_payment as $key_a){ ?>
  <div class="card">
    <div class="card-header" id="heading<?php echo $key_a->id_payment_account; ?>">
      <h5 class="mb-0">
        <button onclick="set_nominal2('<?php echo $key_a->rate; ?>','<?php echo $key_a->id_payment_account; ?>')" class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $key_a->id_payment_account; ?>" aria-expanded="true" aria-controls="collapse<?php echo $key_a->id_payment_account; ?>">
          <label for="inline-radio<?php echo $key_a->id_payment_account; ?>" class="form-check-label ">
          	<?php echo $key_a->method; ?>
          </label>
        </button>
      </h5>
    </div>

    <div id="collapse<?php echo $key_a->id_payment_account; ?>" class="collapse" aria-labelledby="heading<?php echo $key_a->id_payment_account; ?>" data-parent="#accordion">
      <div class="card-body">
      	<div class="row">
      		<div class="col-md-6">
      			<p><b>Account Name : </b> <?php echo $key_a->account_name; ?></p><br>
		        <p><b>Account Number : </b> <?php echo $key_a->account_number; ?></p><br>
		        <p><b>Account Branch : </b> <?php echo $key_a->account_branch; ?></p><br>
      		</div>
      		<div class="col-md-6">
      			<fieldset>
      				<center>Nominal Topup :</center>
      				<center><h1 id="lbl-<?php echo $key_a->id_payment_account; ?>"></h1></center>
      				<center><button onclick="ajax_action_topup('<?php echo $key_a->id_payment_account; ?>')" class="btn btn-success">Next</button></center>
      			</fieldset>
      		</div>
      	</div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
	</td>
	</tr>
</table>
<script type="text/javascript">
	function set_nominal1(){
		var admin_acc =0;
		var nominal =0;
		if ($("#val-id").val()!="") {
			admin_acc = $("#val-id").val();
		}

		

		if ($('#nominal').val()!="") {
			nominal = parseFloat($('#nominal').val()); 
			var rate = parseFloat($('#val-rate').val());
			var topup = (nominal * parseFloat(rate))+parseFloat($('#rand').val())
			$('#lbl-'+admin_acc).html(topup);
			$('#val-topup').val(topup);
		}else{
			$('#lbl-'+admin_acc).html('0');
			$('#val-topup').val('0');
		}

		if (parseInt(nominal)<50000) {
			$('#alert-nominal').html("Minimal Nominal 50,000");
		}else{
			$('#alert-nominal').html("");
		}


		

	}
	function set_nominal2(rate,id) {
		var nominal = 0;
		if ($('#nominal').val()!="") {
			var nominal = parseFloat($('#nominal').val());
			var topup = (nominal * parseFloat(rate))+parseFloat($('#rand').val())
			$('#lbl-'+id).html(topup);
			$('#val-topup').val(topup);
			$('#val-rate').val(rate);
		}else{
			$('#lbl-'+id).html(0);
			$('#val-topup').val(0);
			$('#val-rate').val(rate);
		}
		$('#val-id').val(id);
	}
	function ajax_action_topup(admin_account){
		var nominal = $('#val-topup').val();
		$.ajax({
              url: "<?php echo base_url(); ?>reseller.php/payment/ajax_action_add_topup",
              type:'POST',
              dataType: "json",
              data: {admin_account:admin_account,nominal:nominal},
              beforeSend: function () {
                  $('#page-load').show();
              },
              success: function(data) {
                        $('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }   
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
	}
</script>
<style type="text/css">
  #list_payment td{
    text-align: center;
  }
</style>
<div class="row">
  <div class="col-md-12" style="background: #eee;">
    <table>
      <tr>
        <td><h4>List Payment </h4></td>
        <td><h5><?php if($resv2_premium=="1"){ ?> (This Account Is Premium) <?php } ?></h5></td>
      </tr>
    </table>
    <br>
    <table class="display responsive nowrap" style="width:100%" id="list_payment">
      <thead>
        <tr>
          <th><center>No</center></th>
          <th><center>Date</center></th>
          <th><center>Type</center></th>
          <th><center>Member Account</center></th>
          <th><center>Admin Account</center></th>
          <th><center>Nominal Transfer</center></th>
          <th><center>Nominal Topup</center></th>
          <th><center>File Proof</center></th>
          <th><center>Status</center></th>
        </tr>
      </thead>
      <tbody>
        
      </tbody>
    </table>
  </div>
  <div class="col-md-8" style="margin-top:5%;">
    <h4>Add Payment</h4>
    <form method="post" onsubmit="return ajax_action_add_payment();">
    <table class="table">
      <tr>
        <td>
          <label>Metode Pembayaran</label>
          <select onchange="ajax_get_method()" class="form-control" id="metode" name="metode" required="">
            <option value=""></option>
            <option value="AUTO">AUTO</option>
            <option value="MANUAL">MANUAL</option>
          </select>
        </td>
        <td>
          <label>Method</label>
          <select onchange="ajax_get_method()" class="form-control" id="method" required="">
            <option value="">-Select Method-</option>
            <option value="BANK">BANK</option>
            <option value="PULSA">PULSA</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <label>Member Account</label>
          <select onchange="ajax_get_member_account(this)" id="m_id_paymentaccount" name="m_id_paymentaccount" class="form-control select2" style="width: 100%;" required="">
          </select>
        </td>
        <td>
          <label>Admin Account</label>
          <select onchange="ajax_get_admin_account(this)" id="a_id_paymentaccount" name="a_id_paymentaccount" class="form-control select2" style="width: 100%;" required="">
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <label>Type</label>
          <select class="form-control" id="type" name="type" required="">
            <option value=""></option>
            <option value="TOPUP">TOPUP</option>
            <?php if($resv2_premium!="1"){ ?><option value="UPGRADE">UPGRADE</option><?php } ?>
          </select>
        </td>
        <td>
          <label>Nominal</label>
          <input type="text" name="nominal" id="nominal" onkeyup="ajax_set_nominal(this)" class="form-control" value="0">
        </td>
      </tr>
    </table>
    <table class="table">
      <tr>
        <td>
          <label>Nominal Transfer</label>
          <input disabled="" type="text" value="0" class="form-control" name="nominal_transfer" id="nominal_transfer" required="">
          <input type="hidden" name="" id="generic_number" value="<?php echo rand(50,499); ?>">
        </td>
        <td>
          <label>Nominal Topup</label>
          <input type="text" class="form-control" readonly="" name="nominal_topup" id="nominal_topup">
        </td>
      </tr>
      <tr>
        <td>
          <label>File Proof</label>
          <input type="file" class="form-control" name="file_proof" id="file_proof" required="">
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td>
          <button type="submit" class="btn btn-primary btn-sm">Add Payment</button>
        </td>
        <td></td>
      </tr>
    </table>
    </form>    
  </div>
  <div class="col-md-4" style="margin-top:5%;">
      <table id="tb_a_acc" class="table" style="display: none;">
        <tr>
          <td colspan="3" align="center"><b>Admin Account</b></td>
        </tr>
        <tr>
          <td>Method</td>
          <td>:</td>
          <td><p id="ket_method"></p></td>
        </tr>
        <tr>
          <td>Account Number</td>
          <td>:</td>
          <td><p id="ket_number"></p></td>
        </tr>
        <tr>
          <td>Account Name</td>
          <td>:</td>
          <td><p id="ket_name"></p></td>
        </tr>
        <tr>
          <td>Account Branch</td>
          <td>:</td>
          <td><p id="ket_branch"></p></td>
        </tr>
        <tr>
          <td>Type</td>
          <td>:</td>
          <td><p id="ket_type"></p></td>
          <input type="hidden" id="rate" name="rate">
        </tr>
      </table>
      <br>
      <table id="tb_m_acc" class="table" style="display: none;">
        <tr>
          <td colspan="3" align="center"><b>Member Account</b></td>
        </tr>
        <tr>
          <td>Method</td>
          <td>:</td>
          <td><p id="m_ket_method"></p></td>
        </tr>
        <tr>
          <td>Account Number</td>
          <td>:</td>
          <td><p id="m_ket_number"></p></td>
        </tr>
        <tr>
          <td>Account Name</td>
          <td>:</td>
          <td><p id="m_ket_name"></p></td>
        </tr>
        <tr>
          <td>Account Branch</td>
          <td>:</td>
          <td><p id="m_ket_branch"></p></td>
        </tr>
        <tr>
          <td>Type</td>
          <td>:</td>
          <td><p id="m_ket_type"></p></td>
        </tr>
      </table>    
  </div>
</div>


<div class="modal fade" id="modal_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form id="form_tambah">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><center>File Proof</center></h4>
      </div>
      <div class="modal-body">
        <img width="100%" src="" id="e_image" alt="" class="img-rounded">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  
  function ajax_list_payment(){
    $('#list_payment').DataTable({ 
        "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 10,
        "ajax": {
            "url": "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_list_payment/",
            "type": "POST",
            "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                       
        },

        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
  }

  ajax_list_payment();

  function show_file_proof(img){
    document.getElementById("e_image").src = "<?php echo base_url(); ?>uploads/file_proof/"+img;              
    $('#modal_img').modal('show');
  }

  function ajax_get_method(key){
    var method = $('#method').val();
    var type = $('#metode').val();
    method_admin(method,type);
    method_member(method);
    $('#nominal_transfer').val("");
    $('#nominal_topup').val("");
    $("#rate").val("");
  }

  function method_admin(method,type){
    $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/payment/ajax_get_admin_method/",
              type:'POST',
              dataType: "json",
              data: {method:method,type:type},
              success: function(data) {
                var html = "<option value=''>- Select Account Admin -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id_payment_account+"'>"+data[i].method+"</option>";
                }
                $('#a_id_paymentaccount').html(html);
              }
            });
  }

  function method_member(method){
   $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/payment/ajax_get_member_method/",
              type:'POST',
              dataType: "json",
              data: {method:method},
              success: function(data) {
                var html = "<option value=''>- Select Account Member -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id_payment_account+"'>"+data[i].method+"</option>";
                }
                $('#m_id_paymentaccount').html(html);
              }
            }); 
  }

  function ajax_get_member_account(key) {
    if(key.value==""){
      $('#tb_m_acc').slideUp();
    }
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_get_member_account/",
              method:'POST',
              dataType: "json",
              data: {id:key.value},
              success: function(data) {
                var i ;
                for(i = 0; i<data.length; i++){
                  $("#m_ket_name").html(data[i].account_name);
                  $("#m_ket_number").html(data[i].account_number);
                  $("#m_ket_branch").html(data[i].account_branch);
                  $("#m_ket_type").html(data[i].type);
                  $("#m_ket_method").html(data[i].method);
                } 
                if(key.value!=""){
                  $('#tb_m_acc').slideDown();
                }
              }
          });
    }

  function ajax_get_admin_account(key) {
    if(key.value==""){
      $('#tb_a_acc').slideUp();
      $("#rate").val("");
      $('#nominal_transfer').val("");
      $('#nominal').val("0");
      $('#nominal_topup').val("");
    }else{
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_get_admin_account/",
              method:'POST',
              dataType: "json",
              data: {id:key.value},
              success: function(data) {
                var i ;
                for(i = 0; i<data.length; i++){
                  $("#ket_name").html(data[i].account_name);
                  $("#ket_number").html(data[i].account_number);
                  $("#ket_branch").html(data[i].account_branch);
                  $("#ket_type").html(data[i].type);
                  $("#ket_method").html(data[i].method);
                  $("#rate").val(data[i].rate);
                  
                } 
                $('#tb_a_acc').slideDown();
              }
          });
    }
  }
  function ajax_set_nominal(key){
    if($('#rate').val()==""){
      alert('Silahkan Pilih Admin Account terlebih dahulu ');
      $('#nominal').val("0");
    }else{
      if(key.value==""){
        var get_nominal = 0;
        $('#nominal_topup').val("0");
        $('#nominal_transfer').val("0");
      }else{
        var get_nominal = key.value;
        var nominal = parseFloat(get_nominal);
        var rate = parseFloat($('#rate').val());
        var hasil = (nominal * rate)+parseFloat($('#generic_number').val());
        $('#nominal_topup').val(parseFloat(hasil));
        $('#nominal_transfer').val(parseFloat(hasil));
      }
    }
  }

  function ajax_action_add_payment(){    
    var file_data = $('#file_proof').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('m_id_paymentaccount', $('#m_id_paymentaccount').val());
    form_data.append('a_id_paymentaccount', $('#a_id_paymentaccount').val());
    form_data.append('nominal_transfer', $('#nominal_transfer').val());
    form_data.append('nominal_topup', $('#nominal_topup').val());
    form_data.append('type', $('#type').val());
    form_data.append('metode', $('#metode').val());
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo
$this->security->get_csrf_hash(); ?>');

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment/ajax_action_add_payment/",
              type:'POST',
              dataType: "json",
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              success: function(data) {
                  if(data.result){
                    alert(data.message.body);
                    setTimeout(function(){window.location = data.redirect},500);
                  }else{
                    alert(data.message.body);
                  }
              }
          });
    return false;
  }
</script>
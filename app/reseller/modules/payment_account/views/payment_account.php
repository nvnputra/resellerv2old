
<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_tambah">New Payment Account</button>
<br><br>
<table class="table" id="list_payment_acc">
  <thead>
    <tr>
      <th><center>No</center></th>
      <th><center>Account Number</center></th>
      <th><center>Account Name</center></th>
      <th><center>Account Branch</center></th>
      <th><center>Action</center></th>
    </tr>
  </thead>
  <tbody>
 
  </tbody>
</table>


<script type="text/javascript">
 

  function ajax_action_add_account() {

    var method = $("#method").val();
    var text_method = $("#method option:selected").text();
    var account_number = $("#account_number").val();
    var account_name = $("#account_name").val();
    var account_branch = $("#account_branch").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_add_account/",
              type:'POST',
              dataType: "json",
              data: { method:method,account_number:account_number,account_name:account_name,account_branch:account_branch,text_method:text_method },
              beforeSend: function () {
                $('#page-load').show();
                    },
              success: function(data) {
                $('#page-load').hide();
                  if(data.result){
                    toastr["success"](data.message.body);
                    setTimeout(function(){window.location = data.redirect},500);
                  }else{
                    toastr["error"](data.message.body);
                  }
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }
  function ajax_action_delete_account(id){
    if (confirm('Apakah Anda Yakin Menghapus Data Ini?')) {
      $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_delete_account/",
              type:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                $('#page-load').show();
                    },
              success: function(data) {
                $('#page-load').hide();
                    if(data.result){
                    toastr["success"](data.message.body);
                  }else{
                    toastr["error"](data.message.body);
                  }
                  
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    }
  }

  function ajax_get_data_account(id){
    $('#id').val(id);
    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_get_data_edit/",
              method:'POST',
              dataType: "json",
              data: {id:id},
              beforeSend: function () {
                $('#page-load').show();
                    },
              success: function(data) {
                $('#page-load').hide();
                var i ;
                for(i = 0; i<data.length; i++){
                      $("#e_method").val(data[i].id_payment_method);
                      $("#e_type").val(data[i].type);
                      $("#e_account_number").val(data[i].account_number);
                      $("#e_account_name").val(data[i].account_name);
                      $("#e_account_branch").val(data[i].account_branch);

                } 

                $('#modal_edit').modal('show');      
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
  }

  function ajax_action_edit_account(){
    var id = $("#id").val();
    var method = $("#e_method").val();
    var text_method = $("#e_method option:selected").text();
    var account_number = $("#e_account_number").val();
    var account_name = $("#e_account_name").val();
    var account_branch = $("#e_account_branch").val();

    $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/payment_account/ajax_action_edit_account/",
              type:'POST',
              dataType: "json",
              data: { method:method,account_number:account_number,account_name:account_name,account_branch:account_branch,id:id,text_method:text_method },
              beforeSend: function () {
                $('#page-load').show();
                    },
              success: function(data) {
                $('#page-load').hide();
                  if(data.result){
                    toastr["success"](data.message.body);
                  }else{
                    toastr["error"](data.message.body);
                  }
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });
    return false;
  }

</script>
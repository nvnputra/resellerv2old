<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_account extends MY_Controller  {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_payment_account");
      $this->load->model("notif/M_notif");
      $this->load->library('session');
      $this->load->library('form_validation');
    }


    public function index()
    {
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            $id_member = $this->session->userdata('resv2.m_id');
           $data['data_profile'] = $this->M_payment_account->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
           $data['data_user'] = $this->M_payment_account->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
           if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
            }
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");

            $data['content'] = 'payment_account';
            
            $data['method'] = $this->M_payment_account->fetch_table("*","a__payment_method","");
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_list_payment_acc(){
        $column = 'a.*,b.name as method';
         $column_order = array(null, 'method','a.type','account_number','account_name','account_branch',null); //set column field database for datatable orderable
         $column_search = array('method','a.type','account_number','account_name','account_branch'); //set column field database for datatable searchable 
         $order = array('id' => 'asc'); // default order 
         $table = "m__payment_account a ";
         $id_member = $this->session->userdata('resv2.m_id');
         $where = "a.id_member = '$id_member' ";
        $joins = array(
                array(
                    'table' => 'a__payment_method b',
                    'condition' => 'a.id_payment_method = b.id',
                    'jointype' => ''
                )
            );

        $list = $this->M_payment_account->get_datatables($column,$table,$column_order,$column_search,$order,$where,$joins);
        
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $key->method." - ".$key->account_number;
            $row[] = $key->account_name;
            $row[] = $key->account_branch;
            $row[] = ' <button onclick="ajax_get_data_account('.$key->id_payment_account.')" class="btn btn-success btn-sm">Edit</button>
          <button onclick="ajax_action_delete_account('.$key->id_payment_account.')" class="btn btn-danger btn-sm">Delete</button>';
            $data[] = $row;
        }
 
        $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->M_payment_account->count_all($table,$where,$joins),
                    "recordsFiltered" => $this->M_payment_account->count_filtered($column,$table,$column_order,$column_search,$order,$where,$joins),
                    "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_action_add_account(){
        $text_method = explode("-", post('text_method'));
        if($text_method[0]=="BANK "){
            $this->form_validation->set_rules('account_name', 'account name', 'required');
            /*$this->form_validation->set_rules('account_branch', 'account branch', 'required');*/

        }
        $this->form_validation->set_rules('method', 'Payment Method', 'required');
        $this->form_validation->set_rules('account_number', 'account number', 'required');
        


        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
            $data = array(
                "id_payment_method" => post("method"),
                "id_member" => $this->session->userdata('resv2.m_id'),
                "type" => "AUTO",
                "account_number" => post("account_number"),
                "account_name" => post("account_name"),
                "account_branch" => post("account_branch"),
                "created_at" => date("Y-m-d H:i:s"),
                "update_at" => date("Y-m-d H:i:s")
            );

            $add = $this->M_payment_account->insert_table("m__payment_account",$data);
            if($add==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Menambah Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Menambah data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_account'
                );
                print json_encode($json_data);
            }
        }



    }

    public function ajax_action_delete_account(){
           $delete =$this->M_payment_account->delete_table("m__payment_account","id_payment_account",post("id"));
           if($delete==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal mengahapus Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses mengahapus data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_account'
                );
                print json_encode($json_data);
            }
    }

    public function ajax_get_data_edit(){
        $id = post("id");
        $data = $this->M_payment_account->fetch_table("*","m__payment_account","id_payment_account='$id'");
        print json_encode($data);
    }

    public function ajax_action_edit_account(){
        $text_method = explode("-", post('text_method'));
        if($text_method[0]=="BANK "){
            $this->form_validation->set_rules('account_name', 'account name', 'required');
            /*$this->form_validation->set_rules('account_branch', 'account branch', 'required');*/

        }
        $this->form_validation->set_rules('method', 'Payment Method', 'required');
        $this->form_validation->set_rules('account_number', 'account number', 'required');

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{
             $data = array(
                "id_payment_method" => post("method"),
                "type" => "AUTO",
                "account_number" => post("account_number"),
                "account_name" => post("account_name"),
                "account_branch" => post("account_branch"),
                "update_at" => date("Y-m-d H:i:s")
            );

            $edit = $this->M_payment_account->update_table("m__payment_account",$data,"id_payment_account",post("id"));

            if($edit==FALSE){
                $json_data =  array(
                    "result" => FALSE ,
                    "message" => array('head'=> 'Failed', 'body'=> 'Gagal Mengedit Data'),
                    "form_error" => $error,
                    "redirect" => ''
                );
                print json_encode($json_data);
                die();
            }else{
                $json_data =  array(
                    "result" => TRUE,
                    "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                    "form_error" => '',
                    "redirect" => ''.base_url().$this->config->item('index_page').'/payment_account'
                );
                print json_encode($json_data);
            }
          
        }
        
    }

    
}

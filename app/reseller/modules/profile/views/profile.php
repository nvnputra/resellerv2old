<?php 
$data_prov = $prov ;
$data_jobs = $jobs ;
$data_education = $educations ;
foreach ($data as $key) {    ?>
<div class="row">
  <div class="col-md-3">
    <center>

    <div style="height: 220px; width: 100%; background: url(<?php if($key->bg_image==''){ echo base_url().'uploads/profile/bg/bg-default.jpg'; }else{ echo base_url().'uploads/profile/bg/'.$key->bg_image; }; ?>)"><br><br>
        <img style="" alt="140x140"  class="image img-cir img-120"  width="150"
    src="<?php if($key->profile_image==""){ echo base_url().'uploads/profile/profile/profile-default.png'; }else{
      echo base_url().'uploads/profile/profile/'.$key->profile_image; } ?>">
      <br>
    </div>
    <br>
      <b><?php echo $key->username; ?></b>
    
    <br><br>
    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_upload">Upload Background</button><br><br>
    <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_profile">Upload Profile</button><br><br>
    <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal_password" >Change Password</button><br><br>
    <a href="<?=base_url().$this->config->item('index_page'); ?>/payment_account/"><button class="btn btn-success btn-xs"  >Payment Account</button></a>
    <input type="hidden" id="val_profile_img" value="<?php echo $key->profile_image; ?>" name="">
    <input type="hidden" id="val_bg_img" value="<?php echo $key->bg_image; ?>" name="">
    <input type="hidden" id="val_id_img" value="<?php echo $key->id_images; ?>" name="">
    
    </center>
  </div>
  <div class="col-md-9">
    <table class="table" id="from1">
          <tr>
            <td>
              <label>First Name :</label>
              <?php echo $key->first_name; ?>
            </td>
            <td>
              <label>Last Name :</label>
              <?php echo $key->last_name; ?>
            </td>
          </tr>
          <tr>
            <td>
              <label>Birth regency :</label>
              <?php echo $key->regency; ?>
            </td>
            <td>
              <label>Birth date :</label>
              <?php echo $key->birth_date; ?>
            </td>
          </tr>
          <tr>
            <td>
              <label>Phone :</label>
              <?php echo $key->phone; ?>
            </td>
            <td>
              <label>Phone 2 :</label>
              <?php echo $key->phone2; ?>
            </td>
          </tr>
          <tr>
            <td>
              <label>Address :</label>
              <?php echo $key->address; ?>
            </td>
            <td>
              <label>Gender :</label>
              <?php if($key->gender=="L"){ echo "Male";}else if($key->gender=="P"){ echo "Female";} ?> 
          </tr>
          <tr>
            <td>
              <label>Provinci :</label>
                <?php foreach ($data_prov as $prov) { ?>
                  <?php if($key->id_provinces==$prov->id){ echo $prov->name; } ?>
                <?php } ?>
            </td>
            <td>
              <label>Regency :</label>
              <?php echo $key->regency ; ?>
            </td>
          </tr>
          <tr>
            <td>
              <label>District :</label>
              <?php echo $key->district; ?>
            </td>
            <td>
              <label>Village :</label>
              <?php echo $key->village; ?>
            </td>
          </tr>
          <tr>
            <td>
              <label>Job :</label>
                <?php foreach ($data_jobs as $jobs) { ?>
                <?php if($key->id_job==$jobs->id){ echo $jobs->name; ; } ?> 
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Education :</label>
                <?php foreach ($data_education as $educations) { ?>
                  <?php if($key->id_education==$educations->id){ echo $educations->name; } ?>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Fb :</label>
              <?php echo $key->social_fb; ?>
            </td>
            <td>
              <label>Ig :</label>
              <?php echo $key->social_ig; ?>
            </td>
          </tr>
          <tr>
            <td>
              <label>Line :</label>
              <?php echo $key->social_line; ?>
            </td>
            <td align="">
              <button class="btn btn-success btn-sm " onclick="ajax_show_edit()">Edit Profile</button>
            </td>
          </tr>
    </table>
    <form method="post" onsubmit="return ajax_action_edit_profile();">
    <table class="table" style="display: none;" id="from2">
          <tr>
            <td>
              <label>First Name :</label>
              <input type="text" name="first" id="first" class="form-control" value="<?php echo $key->first_name; ?>" required="">
            </td>
            <td>
              <label>Last Name :</label>
              <input type="text" name="last" id="last" class="form-control" value="<?php echo $key->last_name; ?>" required="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Birth regency :</label>
              <select class="form-control select2" style="width: 100%;" name="birth_regency" id="birth_regency" required="">
                <option value="">Pilihan</option>
                <?php foreach ($regency as $regency) { ?>
                  <option <?php if($key->birth_regency==$regency->id){ echo "selected"; } ?> value="<?php echo $regency->id; ?>"><?php echo $regency->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Birth date :</label>
              <input type="date" name="birth_date" id="birth_date" class="form-control"  value="<?php echo $key->birth_date; ?>" required="">
            </td>
          </tr>
          <tr>
            <td>
              <label>Phone :</label>
              <input type="number" name="phone" id="phone" class="form-control" value="<?php echo $key->phone; ?>" required="">
            </td>
            <td>
              <label>Phone 2 :</label>
              <input type="number" name="phone2" id="phone2" class="form-control" value="<?php echo $key->phone2; ?>">
            </td>
          </tr>
          <tr>
            <td>
              <label>Address :</label>
              <textarea name="address" id="address" rows="2" class="form-control" required=""><?php echo $key->address; ?></textarea>
            </td>
            <td>
              <label>Gender :</label><br>
              <input type="radio" name="jk" <?php if($key->gender=="L"){ echo "checked";} ?> id="jk1" value="L"> Male
              <input type="radio" name="jk" <?php if($key->gender=="P"){ echo "checked";} ?> id="jk2" value="P"> Female
            </td>
          </tr>
          <tr>
            <td>
              <label>Provinci :</label><br>
              <select onchange="get_regency(this)" class="form-control select2" style="width: 100%;" name="provinci" id="provinci" required="">
                <option value="">Pilihan</option>
                <?php foreach ($data_prov as $prov) { ?>
                  <option <?php if($key->id_provinces==$prov->id){ echo "selected"; } ?> value="<?php echo $prov->id; ?>"><?php echo $prov->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Regency :</label><br>
              <select onchange="get_district(this)" class="form-control select2" style="width: 100%;" name="regency" id="regency" required="">
                <option value="<?php echo $key->id_regency; ?>"><?php echo $key->regency ; ?></option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>District :</label><br>
              <select onchange="get_villages(this)" class="form-control select2" style="width: 100%;" name="district" id="district" required="">
                <option value="<?php echo $key->id_district; ?>"><?php echo $key->district ; ?></option>
              </select>
            </td>
            <td>
              <label>Village :</label><br>
              <select class="form-control select2" style="width: 100%;" name="village" id="village" required="">
                <option value="<?php echo $key->id_village; ?>"><?php echo $key->village ; ?></option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Job :</label><br>
              <select class="form-control select2" style="width: 100%;" name="job" id="job" required="">
                <option value="">Pilihan</option>
                <?php foreach ($data_jobs as $jobs) { ?>
                  <option <?php if($key->id_job==$jobs->id){ echo "selected"; } ?> value="<?php echo $jobs->id; ?>"><?php echo $jobs->name; ?></option>
                <?php } ?>
              </select>
            </td>
            <td>
              <label>Education :</label><br>
              <select class="form-control select2" style="width: 100%;" name="education" id="education" required="">
                <option value="">Pilihan</option>
                <?php foreach ($data_education as $educations) { ?>
                  <option <?php if($key->id_education==$educations->id){ echo "selected"; } ?> value="<?php echo $educations->id; ?>"><?php echo $educations->name; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td>
              <label>Fb :</label>
              <input type="text" name="fb" id="fb" class="form-control" value="<?php echo $key->social_fb; ?>">
            </td>
            <td>
              <label>Ig :</label><br>
              <input type="text" name="ig" id="ig" class="form-control" value="<?php echo $key->social_ig; ?>">
            </td>
          </tr>
          <tr>
            <td>
              <label>Line :</label><br>
              <input type="text" name="line" id="line" class="form-control" value="<?php echo $key->social_line; ?>">
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <button type="submit" class="btn btn-success btn-sm btn-block" >Edit</button>
            </td>
          </tr>
        </table> 
        </form>
  </div>
</div>
<?php } ?>
<script type="text/javascript">
function ajax_show_edit(){
  $('#from1').hide();
  $('#from2').fadeIn();
}

function get_regency(key) {
            $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/s_regencies/ajax_get_regency/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                $('#page-load').show();
                },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select Regency -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#regency').html(html);
                $('#district').html("");
                $('#village').html("");
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
          }
          function get_district(key) {
            $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/s_district/ajax_get_district/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                $('#page-load').show();
                },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select District -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#district').html(html);
                $('#village').html("");
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
          }
          function get_villages(key) {
            $.ajax({
              url: "<?php echo base_url(); ?>reseller.php/s_villages/ajax_get_villages/",
              type:'POST',
              dataType: "json",
              data: {id:key.value},
              beforeSend: function () {
                $('#page-load').show();
                },
              success: function(data) {
                $('#page-load').hide();
                var html = "<option value=''>- Select Village -</option>";
                var i;
                for(i=0; i<data.length; i++){
                  html += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                }
                $('#village').html(html);
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
            });
          }

function ajax_action_change_password(){
          var old_password = $("#old_password").val();
          var new_password = $("#new_password").val();
          var re_password = $("#re_password").val();

          $.ajax({
                    url: "<?php echo base_url(); ?>reseller.php/profile/ajax_action_change_password/",
                    type:'POST',
                    dataType: "json",
                    data: {old_password:old_password,new_password:new_password, re_password:re_password},
                    beforeSend: function () {
                      $('#page-load').show();
                     },
                    success: function(data) {
                      $('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }
                    },error: function(request, status, error){
                        $('#page-load').hide();
                        toastr["error"]("Error, Please try again later");
                    }
                });
    return false;
}

function ajax_action_edit_profile() {
         

          if (document.getElementById('jk1').checked) {
            var jk = document.getElementById('jk1').value;
          }else if (document.getElementById('jk2').checked){
            var jk = document.getElementById('jk2').value;
          }else{
            var jk = "";
          }

          var first = $("#first").val();
          var last = $("#last").val();
          var birth_regency = $("#birth_regency").val();
          var birth_date = $("#birth_date").val();
          var phone = $("#phone").val();
          var phone2 = $("#phone2").val();
          var address = $("#address").val();
          var provinci = $("#provinci").val();
          var regency = $("#regency").val();
          var district = $("#district").val();
          var village = $("#village").val();
          var job = $("#job").val();
          var education = $("#education").val();
          var fb = $("#fb").val();
          var ig = $("#ig").val();
          var line = $("#line").val();

          $.ajax({
                    url: "<?php echo base_url(); ?>reseller.php/profile/ajax_action_edit_profile/",
                    type:'POST',
                    dataType: "json",
                    data: {first:first,last:last, birth_regency:birth_regency, birth_date:birth_date,phone:phone, phone2:phone2, address:address,provinci:provinci,regency:regency, district:district, village:village,job:job, education:education, fb:fb,ig:ig,line:line, jk:jk},
                    beforeSend: function () {
                      $('#page-load').show();
                    },
                    success: function(data) {
                      $('#page-load').hide();
                        if(data.result){
                          toastr["success"](data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          toastr["error"](data.message.body);
                        }
                    },error: function(request, status, error){
                        $('#page-load').hide();
                        toastr["error"]("Error, Please try again later");
                    }
                });

          return false;
          }

function ajax_action_upload_bg(){
  var file_data = $('#image1').prop('files')[0];
  var form_data = new FormData();
  form_data.append('file', file_data);
  form_data.append('bg', $('#val_bg_img').val());
  form_data.append('id_images', $('#val_id_img').val());
  form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo
$this->security->get_csrf_hash(); ?>');

  $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/profile/ajax_action_upload_bg/",
              dataType: 'json',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              beforeSend: function () {
                $('#page-load').show();
              },
              success: function(data) {
                $('#page-load').hide();
                  if(data.result){
                    toastr["success"](data.message.body);
                    setTimeout(function(){window.location = data.redirect},500);
                  }else{
                    toastr["error"](data.message.body);
                  }
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

}

function ajax_action_upload_profile(){
  var file_data = $('#image2').prop('files')[0];
  var form_data = new FormData();
  form_data.append('file', file_data);
  form_data.append('profile_image', $('#val_profile_img').val());
  form_data.append('id_images', $('#val_id_img').val());
  form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo
$this->security->get_csrf_hash(); ?>');

  $.ajax({
              url: "<?php echo base_url().$this->config->item('index_page'); ?>/profile/ajax_action_upload_profile/",
              dataType: 'json',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              beforeSend: function () {
                $('#page-load').show();
              },
              success: function(data) {
                $('#page-load').hide();
                  if(data.result){
                    toastr["success"](data.message.body);
                    setTimeout(function(){window.location = data.redirect},500);
                  }else{
                    toastr["error"](data.message.body);
                  }
              },error: function(request, status, error){
                  $('#page-load').hide();
                  toastr["error"]("Error, Please try again later");
              }
          });

}
</script>
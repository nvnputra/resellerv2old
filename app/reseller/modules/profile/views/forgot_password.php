<!DOCTYPE html>
<html>
<head>
	<title>ResellerV2 | Forgot Password</title>
 	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery.min.js"></script>
  
<style type="text/css">
</style>
</head>
<body style="background: rgba(0, 0, 0, 0.72);">
	
<div class="container" >
  <div class="row" >
    <div class="col-md-4" ></div>
    <div class="col-md-4" style="background-color: #fff; margin-top: 10%;">
      <div class="btn-warning" style="width: 100%; left: 0; padding: 1%;">
        <center><h3> Forgot Password </h3></center>
      </div>
      <br><br><br>
      <form>
        <label>New Password :</label>
          <input type="password" class="form-control" name="new_password" id="new_password">
        <label>Re Password :</label>
          <input type="password" class="form-control" name="re_password" id="re_password"><br>
        <button type="submit" class="btn btn-warning">Submit</button>
      </form>
      <br><br><br>
    </div>
    <div class="col-md-4" ></div>
  </div>
</div>
</body>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<div id="get_csrf_code">

<script type="text/javascript">

   $('form').submit(function(event){
          event.preventDefault();
          var new_password = $("#new_password").val();
          var re_password = $("#re_password").val();
          $.ajax({
                    url: "<?php echo base_url(); ?>reseller.php/profile/ajax_action_forgot_password/",
                    type:'POST',
                    dataType: "json",
                    data: {new_password:new_password, re_password:re_password,rev2id:'<?php echo get('rev2id'); ?>',token:'<?php echo get('token'); ?>'},
                    success: function(data) {
                        if(data.result){
                          alert(data.message.body);
                          setTimeout(function(){window.location = data.redirect},500);
                        }else{
                          alert(data.message.body);
                        }
                    }
                });
    });

  var csfrData = {};
  csfrData['<?php echo $this->security->get_csrf_token_name(); ?>'] = '<?php echo $this->security->get_csrf_hash(); ?>';
  $.ajaxSetup({
  data: csfrData
  });

    
</script>
</div>
</html>
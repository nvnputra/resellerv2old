<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller  {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
            parent::__construct();
      $this->load->helper('url');
      $this->load->model("M_profile");
      $this->load->model("notif/M_notif");
	  $this->load->library('session');
	  $this->load->library('form_validation');
    }


	public function index()
	{
        if(!empty($this->session->userdata('resv2.m_log_session'))){
            
            $data['content'] = 'profile';
            $id_member = $this->session->userdata('resv2.m_id');
            $data['data_user'] = $this->M_profile->get_row("username,balance,is_premium,trial_expired","m__member","id_member='$id_member' ","","",FALSE);
            $data['data_profile']= $this->M_profile->get_row("profile_image","m__images","id_member='$id_member' ","","",FALSE);
            if (date("Y-m-d H:i:s")>$data['data_user']->trial_expired and $data['data_user']->is_premium!="1") {
                    redirect('payment/upgrade/');
            }
            $data['data_notif'] = $this->M_notif->limit_notif($id_member);
            $data['menu_post'] = $this->M_notif->fetch_table("*","a__post_category","id_parent = '0'");
            $data['notif_order'] = $this->M_notif->notif_order($id_member);
            $data['notif_payment'] = $this->M_notif->notif_payment($id_member);

            $id = $this->session->userdata('resv2.m_id');
            $data['jobs'] = $this->M_profile->fetch_table("*","sys__jobs","");
            $data['educations'] = $this->M_profile->fetch_table("*","sys__educations","");
            $data['prov'] = $this->M_profile->fetch_table("*","sys__provinces","");
            $data['regency'] = $this->M_profile->fetch_table("*","sys__regencies","");

            $joins = array(
                array(
                    'table' => 'm__member_detail b',
                    'condition' => 'a.id_member = b.id_member',
                    'jointype' => ''
                ),
                array(
                    'table' => 'sys__regencies c',
                    'condition' => 'b.id_regency = c.id',
                    'jointype' => 'left'
                ),
                array(
                    'table' => 'sys__districts d',
                    'condition' => 'b.id_district = d.id',
                    'jointype' => 'left'
                ),
                array(
                    'table' => 'sys__villages e',
                    'condition' => 'b.id_village = e.id',
                    'jointype' => 'left'
                ),
                array(
                    'table' => 'm__images f',
                    'condition' => 'a.id_member = f.id_member',
                    'jointype' => 'left'
                )
            );

            $data['data'] = $this->M_profile->fetch_joins("m__member a","a.*,b.*,c.name as regency,d.name as district,e.name as village,profile_image,bg_image,id_images",$joins,"a.id_member = '$id'",FALSE);
            $this->load->view('login/home',$data);
        }else{
            redirect("login/"); 
        }
        
    }

    public function ajax_action_change_password(){
        $this->form_validation->set_rules('old_password', 'Old password', 'required');
        $this->form_validation->set_rules('new_password', 'New password', 'required');
        $this->form_validation->set_rules('re_password', 'Re password', 'required');
        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        $id = $this->session->userdata('resv2.m_id');
        $data_user = $this->M_profile->get_row("*","m__member","id_member = '$id' ","","",FALSE);

        if(md5(post('old_password'))!= $data_user->password ){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Old Password not Valid'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();   
        }

        if(post('new_password')!= post('re_password') ){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Re Password not same'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();   
        }

        $data = array(
            'password' => md5(post('new_password'))
        );
        $change = $this->M_profile->update_table("m__member",$data,"id_member",$id);

        if ($change ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Failed Edit password'),
                "form_error" => '',
                "redirect" => ''
            );
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Success Edit password'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/profile'
            );
        }
            
            print json_encode($json_data);

    }

    public function ajax_action_edit_profile(){
        
        $this->form_validation->set_rules('first', 'First Name', 'required');
        $this->form_validation->set_rules('last', 'Last Name', 'required');
        $this->form_validation->set_rules('birth_regency', 'birth_regency', 'required');
        $this->form_validation->set_rules('birth_date', 'Birth date', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('phone2', 'phone2', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');
        $this->form_validation->set_rules('jk', 'gender', 'required');
        $this->form_validation->set_rules('provinci', 'provinci', 'required');
        $this->form_validation->set_rules('regency', 'regency', 'required');
        $this->form_validation->set_rules('district', 'district', 'required');
        $this->form_validation->set_rules('village', 'village', 'required');
        $this->form_validation->set_rules('job', 'job', 'required');
        $this->form_validation->set_rules('education', 'education', 'required');
        

        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }else{        


        $detail = array(
            "first_name" => post("first"),
            "last_name" => post("last"),
            "birth_regency" => post("birth_regency"),
            "birth_date" => post("birth_date"),
            "phone" => post("phone"),
            "phone2" => post("phone2"),
            "address" => post("address"),
            "gender" => post("jk"),
            "id_provinces" => post("provinci"),
            "id_regency" => post("regency"),
            "id_district" => post("district"),
            "id_village" => post("village"),
            "id_job" => post("job"),
            "id_education" => post("education"),
            "social_fb" => post("fb"),
            "social_ig" => post("ig"),
            "social_line" => post("line"),
            "update_at" => date("Y-m-d H:i:s")
        );

        $id = $this->session->userdata('resv2.m_id');
        $edit_data =$this->M_profile->update_table("m__member_detail",$detail,"id_member",$id);
    
        if ($edit_data ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'error', 'body'=> 'Gagal Mengedit data'),
                "form_error" => '',
                "redirect" => ''
            );
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Sukses Mengedit data'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/profile'
            );
        }
            
            print json_encode($json_data);

        }

    }

    public function ajax_action_upload_bg(){
        
        $config['upload_path'] = './uploads/profile/bg/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
  
        $this->load->library('upload', $config);

        
  
        if (!$this->upload->do_upload('file'))
        {
            $result = FALSE;
            $message = array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', ''));
            $redirect = "";
        }
        else
        {
            $data = $this->upload->data();
            if(post('id_images')==""){
                 $data_upload = array(
                    "bg_image" => $data['file_name'],
                    "id_member" => $this->session->userdata('resv2.m_id'),
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
                $upload_api = $this->M_profile->insert_table("m__images",$data_upload);  
            }else{
               $data_upload = array(
                    "bg_image" => $data['file_name'],
                    "update_at" => date("Y-m-d H:i:s")
                );
                $upload_api = $this->M_profile->update_table("m__images",$data_upload,"id_images",post("id_images"));
                if(post('bg')!=""){
                    unlink('./uploads/profile/bg/' . post('bg'));
                }
            }
            
            if($upload_api==TRUE)
            {
                $result = TRUE;
                $message =  array('head'=> 'Success', 'body'=> 'Sukses Mengupload');
                $redirect = ''.base_url().$this->config->item('index_page').'/profile';
            }
            else
            {
                $result = FALSE;
                $message =  array('head'=> 'Gagal', 'body'=> 'Gagal Mengupload');
                $redirect = '';
            }
        }
    
            $json_data =  array(
                    "result" => $result ,
                    "message" => $message,
                    "redirect" => $redirect
            );
        print json_encode($json_data);
    }

    public function ajax_action_upload_profile(){
        
        $config['upload_path'] = './uploads/profile/profile/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;
  
        $this->load->library('upload', $config);

        
  
        if (!$this->upload->do_upload('file'))
        {
            $result = FALSE;
            $message = array('head'=> 'Failed', 'body'=> $this->upload->display_errors('', ''));
            $redirect = "";
        }
        else
        {
            $data = $this->upload->data();
            if(post('id_images')==""){
                 $data_upload = array(
                    "profile_image" => $data['file_name'],
                    "id_member" => $this->session->userdata('resv2.m_id'),
                    "created_at" => date("Y-m-d H:i:s"),
                    "update_at" => date("Y-m-d H:i:s")
                );
                $upload_api = $this->M_profile->insert_table("m__images",$data_upload);  
            }else{
               $data_upload = array(
                    "profile_image" => $data['file_name'],
                    "update_at" => date("Y-m-d H:i:s")
                );
                $upload_api = $this->M_profile->update_table("m__images",$data_upload,"id_images",post("id_images"));
                if(post('profile_image')!=""){
                    unlink('./uploads/profile/profile/' . post('profile_image'));
                }
            }
            
            if($upload_api==TRUE)
            {
                $result = TRUE;
                $message =  array('head'=> 'Success', 'body'=> 'Sukses Mengupload');
                $redirect = ''.base_url().$this->config->item('index_page').'/profile';
            }
            else
            {
                $result = FALSE;
                $message =  array('head'=> 'Gagal', 'body'=> 'Gagal Mengupload');
                $redirect = '';
            }
        }
    
            $json_data =  array(
                    "result" => $result ,
                    "message" => $message,
                    "redirect" => $redirect
            );
        print json_encode($json_data);
    }

    public function ajax_action_mail_password(){
        $this->load->library('ElasticEmail');
        $email = post('email');
        $data_user = $this->M_profile->get_row("*","m__member","email='$email' ","","",FALSE);

        $Elastic = new ElasticEmail();
        $admin = "admin@resellerindo.com";
        $from = $admin;
        $fromName = "resellerindo";
        $to = $email;
        $subject = "Reset Password Resellerindo";
        $bodyText = ""; 
        $bodyHtml = 
        "<b>Hello ".$data_user->username." ,</b><br><br><br>
        Email Address : ".$data_user->email."<br>
        We heard that you lost your Resellerindo password. Sorry about that , <br>
        But don’t worry! You can click <a href='http://localhost/resellerv2/reseller.php/profile/forgot_password/?rev2id=".$data_user->id_member."&token=".$data_user->token."'>this link </a> to reset your password.
        <br><br><br>
        Thanks,<br>
        Admin Resellerindo
        "; 

        $send = $Elastic->send($from,$fromName,$subject,$to,$bodyText,$bodyHtml);
        $send = json_decode($send);
        if($send->success!='1' ){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Failed Send Email'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();   
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Success Send Email, Please Check Your Email!'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/login/'
            );
             print json_encode($json_data);
        }



    }

    public function forgot_password(){
        $this->load->view('forgot_password');
    }

    public function ajax_action_forgot_password(){
        $this->form_validation->set_rules('new_password', 'New password', 'required');
        $this->form_validation->set_rules('re_password', 'Re password', 'required');
        
        if($this->form_validation->run()==FALSE){
            $error = $this->form_validation->error_array();
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Pastikan Data terisi Semua'),
                "form_error" => $error,
                "redirect" => ''
            );
            print json_encode($json_data);
            die();
        }

        if(post('new_password')!= post('re_password') ){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Re Password Tidak Cocok'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();   
        }

        $cek_data = count($this->M_profile->fetch_table("*","m__member","id_member='".post('rev2id')."' and token = '".post('token')."'"));

        if($cek_data=="0"){
            $json_data =  array(
                "result" => FALSE ,
                "message" => array('head'=> 'Failed', 'body'=> 'Invalid Url'),
                "form_error" => '',
                "redirect" => ''
            );
            print json_encode($json_data);
            die();   
        }

        $data = array(
            'password' => md5(post('new_password'))
        );
        $change = $this->M_profile->update_table("m__member",$data,"id_member",post('rev2id'));

        if ($change ===  FALSE) {
            $json_data =  array(
                "result" => FALSE,
                "message" => array('head'=> 'Failed', 'body'=> 'Failed change password'),
                "form_error" => '',
                "redirect" => ''
            );
        }else{
            $json_data =  array(
                "result" => TRUE,
                "message" => array('head'=> 'Success', 'body'=> 'Success change password'),
                "form_error" => '',
                "redirect" => ''.base_url().$this->config->item('index_page').'/login'
            );
        }
            
            print json_encode($json_data);

    }
    
}
